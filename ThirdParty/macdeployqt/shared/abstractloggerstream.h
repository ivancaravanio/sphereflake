/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the tools applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/
#ifndef MACDEPLOYQT_SHARED_ABSTRACTLOGGERSTREAM_H
#define MACDEPLOYQT_SHARED_ABSTRACTLOGGERSTREAM_H

#include <QtGlobal>

#include <QChar>
#include <QString>
#include <QByteArray>
#include <QLatin1String>
#include <QTextStreamFunction>
#include <QTextStreamManipulator>

namespace MacDeployQt {
namespace Shared {

class AbstractLoggerStream
{
public:
    virtual ~AbstractLoggerStream();

    virtual AbstractLoggerStream &operator<<(QChar ch) = 0;
    virtual AbstractLoggerStream &operator<<(char ch) = 0;
    virtual AbstractLoggerStream &operator<<(signed short i) = 0;
    virtual AbstractLoggerStream &operator<<(unsigned short i) = 0;
    virtual AbstractLoggerStream &operator<<(signed int i) = 0;
    virtual AbstractLoggerStream &operator<<(unsigned int i) = 0;
    virtual AbstractLoggerStream &operator<<(signed long i) = 0;
    virtual AbstractLoggerStream &operator<<(unsigned long i) = 0;
    virtual AbstractLoggerStream &operator<<(qlonglong i) = 0;
    virtual AbstractLoggerStream &operator<<(qulonglong i) = 0;
    virtual AbstractLoggerStream &operator<<(float f) = 0;
    virtual AbstractLoggerStream &operator<<(double f) = 0;
    virtual AbstractLoggerStream &operator<<(const QString &s) = 0;
    virtual AbstractLoggerStream &operator<<(QLatin1String s) = 0;
    virtual AbstractLoggerStream &operator<<(const QByteArray &array) = 0;
    virtual AbstractLoggerStream &operator<<(const char *c) = 0;
    virtual AbstractLoggerStream &operator<<(const void *ptr) = 0;
    virtual AbstractLoggerStream &operator<<(QTextStreamFunction f) = 0;
    virtual AbstractLoggerStream &operator<<(QTextStreamManipulator m) = 0;
};

}
}

#endif // MACDEPLOYQT_SHARED_ABSTRACTLOGGERSTREAM_H
