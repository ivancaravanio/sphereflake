#include "EnvironmentSettingsEditor.h"

#include "CameraEditor.h"
#include "ColorPicker.h"
#include "LightEditor.h"

#include <QGridLayout>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>

using namespace Sphereflake;

EnvironmentSettingsEditor::EnvironmentSettingsEditor( QWidget* parent, Qt::WindowFlags f )
    : QWidget( parent, f )
    , m_lightEditorGroupBox( nullptr )
    , m_lightEditor( nullptr )
    , m_cameraEditorGroupBox( nullptr )
    , m_cameraEditor( nullptr )
    , m_ambientColorLabel( nullptr )
    , m_ambientColorEditor( nullptr )
{
    m_lightEditorGroupBox = new QGroupBox;
    m_lightEditor = new LightEditor;

    QHBoxLayout* const lightEditorGroupBoxLayout = new QHBoxLayout( m_lightEditorGroupBox );
    lightEditorGroupBoxLayout->addWidget( m_lightEditor );

    m_cameraEditorGroupBox = new QGroupBox;
    m_cameraEditor = new CameraEditor;

    QHBoxLayout* const cameraEditorGroupBoxLayout = new QHBoxLayout( m_cameraEditorGroupBox );
    cameraEditorGroupBoxLayout->addWidget( m_cameraEditor );

    m_ambientColorLabel = new QLabel;
    m_ambientColorEditor = new ColorPicker;

    QGridLayout* const mainLayout = new QGridLayout( this );
    mainLayout->setContentsMargins( QMargins() );
    mainLayout->addWidget( m_lightEditorGroupBox, 0, 0, 1, 2 );
    mainLayout->addWidget( m_cameraEditorGroupBox, 1, 0, 1, 2 );
    mainLayout->addWidget( m_ambientColorLabel, 2, 0 );
    mainLayout->addWidget( m_ambientColorEditor, 2, 1 );

    this->setEnvironmentSettings( EnvironmentSettings::defaultEnvironmentSettings() );
    this->retranslateSelfOnly();
}

void EnvironmentSettingsEditor::retranslate()
{
    this->retranslateSelfOnly();
}

void EnvironmentSettingsEditor::setEnvironmentSettings( const EnvironmentSettings& aEnvironmentSettings )
{
    if ( ! aEnvironmentSettings.isValid() )
    {
        return;
    }

    m_lightEditor->setLight( aEnvironmentSettings.light() );
    m_cameraEditor->setCamera( aEnvironmentSettings.camera() );
    m_ambientColorEditor->setColor( aEnvironmentSettings.ambientColor() );
}

EnvironmentSettings Sphereflake::EnvironmentSettingsEditor::environmentSettings() const
{
    return EnvironmentSettings(
                m_lightEditor->light(),
                m_cameraEditor->camera(),
                m_ambientColorEditor->color() );
}

void EnvironmentSettingsEditor::retranslateSelfOnly()
{
    m_lightEditorGroupBox->setTitle( tr( "Light" ) );
    m_cameraEditorGroupBox->setTitle( tr( "Camera" ) );
    m_ambientColorLabel->setText( tr( "Ambient Color:" ) );
}
