#include "ColorsPicker.h"

#include "ColorPicker.h"
#include "Utilities/GeneralUtilities.h"
#include "Utilities/ImageUtilities.h"

#include <QDateTime>
#include <QDoubleSpinBox>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>

using namespace Sphereflake;

const int ColorsPicker::s_minColorsCountDefault = 1;
const int ColorsPicker::s_maxColorsCountDefault = 1;
const int ColorsPicker::s_areColorsMutuallyExclusiveDefault = false;

ColorsPicker::ColorsPicker( QWidget* parent, Qt::WindowFlags f )
    : QWidget( parent, f )
    , m_addColorButton( nullptr )
    , m_removeColorButton( nullptr )
    , m_colorsLayout( nullptr )
    , m_colorsCountRange( 1, 1 )
    , m_areColorsMutuallyExclusive( s_areColorsMutuallyExclusiveDefault )
{
    m_colorsLayout = new QGridLayout( this );
    m_colorsLayout->setContentsMargins( QMargins() );
    m_addColorButton = new QPushButton( QIcon( QPixmap( "://Add_16.png" ) ), QString() );
    connect( m_addColorButton, SIGNAL(clicked()), this, SLOT(addColor()) );

    m_removeColorButton = new QPushButton( QIcon( QPixmap( "://Remove_16.png" ) ), QString() );
    connect( m_removeColorButton, SIGNAL(clicked()), this, SLOT(removeLastColor()) );

    this->applyColorsCountRange();
    this->retranslateSelfOnly();
}

void ColorsPicker::setMinColorsCount( const int aMinColorsCount )
{
    this->setColorsCountRange( aMinColorsCount, m_colorsCountRange.max() );
}

int ColorsPicker::minColorsCount() const
{
    return m_colorsCountRange.min();
}

void ColorsPicker::setMaxColorsCount( const int aMaxColorsCount )
{
    this->setColorsCountRange( m_colorsCountRange.min(), aMaxColorsCount );
}

int ColorsPicker::maxColorsCount() const
{
    return m_colorsCountRange.max();
}

void ColorsPicker::setColorsCountRange( const int aMinColorsCount, const int aMaxColorsCount )
{
    if ( ! GeneralUtilities::isCountValid( aMinColorsCount, false )
         || ! GeneralUtilities::isCountValid( aMaxColorsCount, false ) )
    {
        return;
    }

    const NumberRange newColorsCountRange( aMinColorsCount, aMaxColorsCount );
    if ( ! newColorsCountRange.isValid()
         || newColorsCountRange == m_colorsCountRange )
    {
        return;
    }

    m_colorsCountRange = newColorsCountRange;
    this->applyColorsCountRange();
}

void ColorsPicker::setAreColorsMutuallyExclusive( const bool aAreColorsMutuallyExclusive )
{
    if ( aAreColorsMutuallyExclusive == m_areColorsMutuallyExclusive )
    {
        return;
    }

    m_areColorsMutuallyExclusive = aAreColorsMutuallyExclusive;
    this->applyAreColorsMutuallyExclusive();
}

bool ColorsPicker::areColorsMutuallyExclusive() const
{
    return m_areColorsMutuallyExclusive;
}

void ColorsPicker::retranslate()
{
    this->retranslateSelfOnly();
}

void ColorsPicker::removeLastColor()
{
    this->setColorsCount( this->colorsCount() - 1 );
}

void ColorsPicker::addColor()
{
    this->setColorsCount( this->colorsCount() + 1 );
}

void ColorsPicker::retranslateSelfOnly()
{
    const int colorLabelPickersCount = m_colorLabelPickers.size();
    for ( int i = 0; i < colorLabelPickersCount; ++ i )
    {
        m_colorLabelPickers[ i ].first->setText( ColorsPicker::colorLabelText( i ) );
    }
}

QString ColorsPicker::colorLabelText( const int colorIndex )
{
    return tr( "Color %1:" ).arg( colorIndex + 1 );
}

void ColorsPicker::setColorsCount( const int aColorsCount )
{
    const int colorsCount = this->colorsCount();
    if ( ! this->isColorsCountValid( aColorsCount )
         || aColorsCount == colorsCount )
    {
        return;
    }

    for ( int colorsToRemove = colorsCount - aColorsCount;
          colorsToRemove > 0;
          -- colorsToRemove )
    {
        const QPair< QLabel*, ColorPicker* > colorLabelPicker = m_colorLabelPickers.takeLast();
        QLabel* const      colorLabel  = colorLabelPicker.first;
        ColorPicker* const colorEditor = colorLabelPicker.second;

        m_colorsLayout->removeWidget( colorLabel );
        m_colorsLayout->removeWidget( colorEditor );

        colorLabel->deleteLater();
        colorEditor->deleteLater();
    }

    QSet< QColor > uniqueColors;
    if ( m_areColorsMutuallyExclusive )
    {
        uniqueColors = this->uniqueColors();
    }

    int colorsToAdd = aColorsCount - colorsCount;
    for ( int i = 0; i < colorsToAdd; ++ i )
    {
        const int colorIndex = colorsCount + i;
        QLabel* const colorLabel  = new QLabel( ColorsPicker::colorLabelText( colorIndex ) );

        QColor newColor;
        if ( m_areColorsMutuallyExclusive )
        {
            newColor = ImageUtilities::randomDifferentColor( uniqueColors );
            uniqueColors.insert( newColor );
        }
        else
        {
            newColor = ImageUtilities::randomColor();
        }

        ColorPicker* const colorEditor = new ColorPicker( newColor );

        m_colorsLayout->addWidget( colorLabel, colorIndex, 1 );
        m_colorsLayout->addWidget( colorEditor, colorIndex, 2 );

        m_colorLabelPickers.append( QPair< QLabel*, ColorPicker* >( colorLabel, colorEditor ) );
    }

    m_colorsLayout->removeWidget( m_addColorButton );
    m_colorsLayout->removeWidget( m_removeColorButton );

    m_colorsLayout->addWidget( m_removeColorButton, aColorsCount - 1, 0 );
    m_colorsLayout->addWidget( m_addColorButton, aColorsCount, 0 );

    m_removeColorButton->setEnabled( aColorsCount > this->minColorsCount() );
    m_addColorButton->setEnabled( aColorsCount < this->maxColorsCount() );
}

int ColorsPicker::colorsCount() const
{
    return m_colorLabelPickers.size();
}

bool ColorsPicker::isColorsCountValid( const int aColorsCount ) const
{
    return m_colorsCountRange.contains( aColorsCount );
}

void ColorsPicker::applyColorsCountRange()
{
    this->setColorsCount( m_colorsCountRange.boundedValue( this->colorsCount() ) );
}

void ColorsPicker::applyAreColorsMutuallyExclusive()
{
    if ( ! m_areColorsMutuallyExclusive )
    {
        return;
    }

    QList< QColor > colors = this->colors();
    const int colorsCount = colors.size();
    if ( colorsCount <= 1 )
    {
        return;
    }

    QSet< QColor > uniqueColors;
    QMutableListIterator< QColor > colorsIter( colors );
    while ( colorsIter.hasNext() )
    {
        QColor& color = colorsIter.next();
        if ( uniqueColors.contains( color ) )
        {
            colorsIter.remove();
        }
        else
        {
            uniqueColors.insert( color );
        }
    }

    if ( colors.size() == colorsCount )
    {
        return;
    }

    this->setColors( colors );
}

void ColorsPicker::setColors( const QList< QColor >& aColors )
{
    if ( ! this->areColorsValid( aColors ) )
    {
        return;
    }

    const int colorsCount = aColors.size();
    this->setColorsCount( colorsCount );
    for ( int i = 0; i < colorsCount; ++ i )
    {
        m_colorLabelPickers[ i ].second->setColor( aColors[ i ] );
    }
}

QList< QColor > ColorsPicker::colors() const
{
    QList< QColor > colors;
    const int colorCount = m_colorLabelPickers.size();
    if ( colorCount == 0 )
    {
        return colors;
    }

    colors.reserve( colorCount );
    for ( int i = 0; i < colorCount; ++ i )
    {
        colors.append( m_colorLabelPickers[ i ].second->color() );
    }

    return colors;
}

QSet< QColor > ColorsPicker::uniqueColors() const
{
    return this->colors().toSet();
}

bool ColorsPicker::areColorsValid( const QList< QColor >& aColors ) const
{
    const int colorsCount = aColors.size();
    if ( ! this->isColorsCountValid( colorsCount ) )
    {
        return false;
    }

    if ( ! m_areColorsMutuallyExclusive )
    {
        return true;
    }

    for ( int i = 0; i < colorsCount; ++ i )
    {
        for ( int j = i + 1; j < colorsCount; ++ j )
        {
            const QColor& colorLeft = aColors[ i ];
            const QColor& colorRight = aColors[ j ];
            if ( ! colorLeft.isValid()
                 || ! colorRight.isValid()
                 || colorLeft == colorRight )
            {
                return false;
            }
        }
    }

    return true;
}
