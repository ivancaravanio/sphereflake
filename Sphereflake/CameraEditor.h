#ifndef SPHEREFLAKE_CAMERAEDITOR_H
#define SPHEREFLAKE_CAMERAEDITOR_H

#include "Data/Camera.h"
#include "Utilities/Retranslatable.h"

#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QDoubleSpinBox)
QT_FORWARD_DECLARE_CLASS(QLabel)

namespace Sphereflake {

class Vec3Editor;

using namespace Sphereflake::Data;
using namespace Sphereflake::Utilities;

class CameraEditor : public QWidget, public Retranslatable
{
    Q_OBJECT

public:
    explicit CameraEditor( QWidget* const parent = nullptr, Qt::WindowFlags f = 0 );

    void setCamera( const Camera& aCamera );
    Camera camera() const;

    void retranslate() /* override */;

private:
    void retranslateSelfOnly();

private:
    QLabel*         m_positionLabel;
    Vec3Editor*     m_positionEditor;
    QLabel*         m_directionLabel;
    Vec3Editor*     m_directionEditor;
    QLabel*         m_zoomFactorLabel;
    QDoubleSpinBox* m_zoomFactorEditor;
};

}

#endif // SPHEREFLAKE_CAMERAEDITOR_H
