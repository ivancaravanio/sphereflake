#ifndef SPHEREFLAKE_MAINCONTROLLER_H
#define SPHEREFLAKE_MAINCONTROLLER_H

#include <QObject>
#include <QScopedPointer>

namespace Sphereflake {
namespace Data {

class Room;

}

using namespace Sphereflake::Data;

class MainWindow;

class MainController : public QObject
{
    Q_OBJECT

public:
    explicit MainController( QObject* const parent = nullptr );
    ~MainController() /* override */;

    void init();

private:
    QScopedPointer< MainWindow > m_mainWindow;
};

}

#endif // SPHEREFLAKE_MAINCONTROLLER_H
