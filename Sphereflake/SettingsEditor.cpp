#include "SettingsEditor.h"

#include "EnvironmentSettingsEditor.h"
#include "SphereflakeSettingsEditor.h"

#include <QGroupBox>
#include <QPushButton>
#include <QVBoxLayout>

using namespace Sphereflake;

SettingsEditor::SettingsEditor( QWidget* parent, Qt::WindowFlags f )
    : QWidget( parent, f )
    , m_sphereflakeSettingsEditor( nullptr )
    , m_sphereflakeSettingsEditorGroupBox( nullptr )
    , m_environmentSettingsEditor( nullptr )
    , m_environmentSettingsEditorGroupBox( nullptr )
    , m_applyButton( nullptr )
{
    m_sphereflakeSettingsEditor         = new SphereflakeSettingsEditor;
    m_sphereflakeSettingsEditorGroupBox = new QGroupBox;

    QVBoxLayout* const sphereflakeSettingsEditorGroupBoxLayout = new QVBoxLayout( m_sphereflakeSettingsEditorGroupBox );
    sphereflakeSettingsEditorGroupBoxLayout->addWidget( m_sphereflakeSettingsEditor );

    m_environmentSettingsEditor         = new EnvironmentSettingsEditor;
    m_environmentSettingsEditorGroupBox = new QGroupBox;

    QVBoxLayout* const environmentSettingsEditorGroupBoxLayout = new QVBoxLayout( m_environmentSettingsEditorGroupBox );
    environmentSettingsEditorGroupBoxLayout->addWidget( m_environmentSettingsEditor );

    m_applyButton = new QPushButton;
    connect( m_applyButton, SIGNAL(clicked()), this, SLOT(requestSettingsChange()) );

    QVBoxLayout* const mainLayout = new QVBoxLayout( this );
    mainLayout->setContentsMargins( QMargins() );
    mainLayout->addWidget( m_sphereflakeSettingsEditorGroupBox );
    mainLayout->addWidget( m_environmentSettingsEditorGroupBox );
    mainLayout->addWidget( m_applyButton                       );

    m_cachedSettings = this->settings();

    this->setSettings( Settings::defaultSettings() );
    this->retranslateSelfOnly();
}

void SettingsEditor::setSettings( const Settings& aSettings )
{
    if ( ! aSettings.isValid() )
    {
        return;
    }

    m_sphereflakeSettingsEditor->setSphereflakeSettings( aSettings.sphereflakeSettings() );
    m_environmentSettingsEditor->setEnvironmentSettings( aSettings.environmentSettings() );
}

void SettingsEditor::retranslate()
{
    this->retranslateSelfOnly();
}

void Sphereflake::SettingsEditor::requestSettingsChange()
{
    const Settings settings = this->settings();
    if ( settings == m_cachedSettings )
    {
        return;
    }

    m_previousSettings = m_cachedSettings;
    m_cachedSettings = settings;

    emit settingsChanged();
}

Settings SettingsEditor::settings() const
{
    return Settings( m_sphereflakeSettingsEditor->sphereflakeSettings(),
                     m_environmentSettingsEditor->environmentSettings() );
}

void SettingsEditor::retranslateSelfOnly()
{
    m_sphereflakeSettingsEditorGroupBox->setTitle( tr( "Sphereflake" ) );
    m_environmentSettingsEditorGroupBox->setTitle( tr( "Environment" ) );
    m_applyButton->setText( tr( "Apply" ) );
}
