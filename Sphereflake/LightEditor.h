#ifndef SPHEREFLAKE_LIGHTEDITOR_H
#define SPHEREFLAKE_LIGHTEDITOR_H

#include "Data/Light.h"
#include "Utilities/Retranslatable.h"

#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QComboBox)
QT_FORWARD_DECLARE_CLASS(QDoubleSpinBox)
QT_FORWARD_DECLARE_CLASS(QLabel)

namespace Sphereflake {

class ColorPicker;
class Vec3Editor;

using namespace Sphereflake::Data;
using namespace Sphereflake::Utilities;

class LightEditor : public QWidget, public Retranslatable
{
    Q_OBJECT

public:
    explicit LightEditor( QWidget* const parent = nullptr, Qt::WindowFlags f = 0 );

    void setLight( const Light& aLight );
    Light light() const;

    void retranslate() /* override */;

private slots:
    void applyType();

private:
    void retranslateSelfOnly();
    void setType( const Light::Type aType );
    Light::Type type() const;

private:
    QLabel*         m_typeLabel;
    QComboBox*      m_typeEditor;

    QLabel*         m_positionLabel;
    Vec3Editor*     m_positionEditor;

    QLabel*         m_directionLabel;
    Vec3Editor*     m_directionEditor;

    QLabel*         m_colorLabel;
    ColorPicker*    m_colorEditor;

    QLabel*         m_intensityLevelLabel;
    QDoubleSpinBox* m_intensityLevelEditor;

    QLabel*         m_cutoffAngleLabel;
    QDoubleSpinBox* m_cutoffAngleEditor;

    QLabel*         m_coneConcentrationExponentLabel;
    QDoubleSpinBox* m_coneConcentrationExponentEditor;

    QLabel*         m_constantAttenuationLabel;
    QDoubleSpinBox* m_constantAttenuationEditor;

    QLabel*         m_linearAttenuationLabel;
    QDoubleSpinBox* m_linearAttenuationEditor;

    QLabel*         m_quadraticAttenuationLabel;
    QDoubleSpinBox* m_quadraticAttenuationEditor;
};

}

#endif // SPHEREFLAKE_LIGHTEDITOR_H
