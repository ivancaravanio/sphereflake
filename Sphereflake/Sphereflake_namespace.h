#ifndef SPHEREFLAKE_SPHEREFLAKE_NAMESPACE_H
#define SPHEREFLAKE_SPHEREFLAKE_NAMESPACE_H

#define STRINGIFY(x) #x
#define STRINGIFY_PROXY(x) STRINGIFY(x)

namespace Sphereflake {

class CommonEntities
{
public:
};

}

#endif // SPHEREFLAKE_SPHEREFLAKE_NAMESPACE_H
