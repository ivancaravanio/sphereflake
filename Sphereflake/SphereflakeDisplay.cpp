#include "SphereflakeDisplay.h"

#include "Data/Vector2D.h"
#include "Utilities/GeneralUtilities.h"
#include "Utilities/MathUtilities.h"
#include "Utilities/OpenGl/ClearColorAlternator.h"
#include "Utilities/OpenGl/LineWidthAlternator.h"
#include "Utilities/OpenGl/OpenGlErrorLogger.h"
#include "Utilities/OpenGl/OpenGlUtilities.h"

#include <QDebug>
#include <QKeyEvent>
#include <qmath.h>
#ifndef QT_NO_WHEELEVENT
    #include <QWheelEvent>
#endif

// #define DUMP_PERFORMANCE
#ifdef DUMP_PERFORMANCE
    #include <QTime>
#endif

// #define DRAW_TRIANGLES_BORDERS

using namespace Sphereflake;

using namespace Sphereflake::Utilities;

const QList< GLuint > SphereflakeDisplay::s_allOrderedGenericVertexAttributeIndices =
        QList< GLuint >()
        << SphereflakeDisplay::GenericVertexAttributeIndexVertexPos;

const float SphereflakeDisplay::s_initialSphereDefaultRadius = 1.0f;

SphereflakeDisplay::SphereflakeDisplay( const QGLFormat& aOpenGlFormat, QWidget* const parent )
    : QGLWidget( aOpenGlFormat, parent )
    , m_shaderProgram( aOpenGlFormat )
    , m_vao( OpenGlUtilities::s_invalidUnsignedGlValue )
    , m_vbo( OpenGlUtilities::s_invalidUnsignedGlValue )
    , m_verticesCount( 0 )
    , m_isGlInitialized( false )
{
}

SphereflakeDisplay::~SphereflakeDisplay()
{
    this->disposeOpenGlResources();
}

void SphereflakeDisplay::setSettings( const Settings& aSettings )
{
    if ( ! aSettings.isValid()
         || aSettings == m_settings )
    {
        return;
    }

    const Settings previousSettings = m_settings;
    m_settings = aSettings;
    this->applySettings( previousSettings );
}

void SphereflakeDisplay::initializeGL()
{
    this->init();
}

void SphereflakeDisplay::resizeGL( int w, int h )
{
    Q_UNUSED( w )
    Q_UNUSED( h )

    this->resetViewport();
    this->resetProjectionMatrix();
}

void SphereflakeDisplay::paintGL()
{
    if ( ! OpenGlUtilities::isUnsignedGlValueValid( m_vao )
         || ! OpenGlUtilities::isUnsignedGlValueValid( m_vbo ) )
    {
        return;
    }

    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    LOG_OPENGL_ERROR();

    m_shaderProgram.activate();

    OpenGlObjectBinder vaoBinder = this->vaoBinder( false );
    vaoBinder.bind();

    this->enableGenericVertexAttributeArrays( true );

#ifdef DRAW_TRIANGLES_BORDERS
    this->setShouldShadeSurface( true );
    OpenGlObjectBinder polygonModeFill( OpenGlObjectBinder::ObjectTypePolygonMode,
                                        GL_FRONT_AND_BACK,
                                        GL_FILL );
    polygonModeFill.bind();
#endif

    this->drawSpheres();

#ifdef DRAW_TRIANGLES_BORDERS
    this->setShouldShadeSurface( false );
    OpenGlObjectBinder polygonModeLines( OpenGlObjectBinder::ObjectTypePolygonMode,
                                         GL_FRONT_AND_BACK,
                                         GL_LINE );
    polygonModeLines.bind();
    this->drawSpheres();
#endif

    vaoBinder.unbind();

    this->enableGenericVertexAttributeArrays( false );

    m_shaderProgram.deactivate();
}

void SphereflakeDisplay::disposeOpenGlResources()
{
    if ( OpenGlUtilities::isUnsignedGlValueValid( m_vao ) )
    {
        glDeleteVertexArrays( 1, & m_vao );
        LOG_OPENGL_ERROR();

        m_vao = OpenGlUtilities::s_invalidUnsignedGlValue;
    }

    QVector< GLuint > vbosToDispose;
    if ( OpenGlUtilities::isUnsignedGlValueValid( m_vbo ) )
    {
        vbosToDispose.append( m_vbo );
        m_vao = OpenGlUtilities::s_invalidUnsignedGlValue;
    }

    const int vbosToDisposeCount = vbosToDispose.size();
    if ( vbosToDisposeCount > 0 )
    {
        glDeleteBuffers( vbosToDisposeCount, vbosToDispose.constData() );
        LOG_OPENGL_ERROR();
    }
}

QRect SphereflakeDisplay::viewport() const
{
    return QRect( 0, 0, this->width(), this->height() );
}

ViewportAlternator SphereflakeDisplay::viewportAlternator( const bool autoRollback ) const
{
    return this->viewportAlternator( this->viewport(), autoRollback );
}

void SphereflakeDisplay::resetViewport()
{
    ViewportAlternator viewportAlternator = this->viewportAlternator( false );
    viewportAlternator.change();
}

OpenGlObjectBinder SphereflakeDisplay::vaoBinder( const bool autoUnbind ) const
{
    return this->vaoBinder( m_vao, autoUnbind );
}

OpenGlObjectBinder SphereflakeDisplay::vboBinder( const bool autoUnbind ) const
{
    return this->vboBinder( m_vbo, autoUnbind );
}

QVector< Vector3Df > SphereflakeDisplay::vertices( const Triangle2D& triangle )
{
    const Triangle2D counterClockwiseWindingOrderTriangle = triangle.havingWindingOrder( false );
    return GeneralUtilities::convertedCollection< QVector< Vector2Dd >, QVector< Vector3Df > >( counterClockwiseWindingOrderTriangle.vertices() );
}

QVector< Vector3Df > SphereflakeDisplay::vertices( const QList< Triangle2D >& triangles )
{
    QVector< Vector3Df > vertices;

    const int trianglesCount = triangles.size();
    if ( trianglesCount == 0 )
    {
        return vertices;
    }

    vertices.reserve( trianglesCount * Triangle2D::s_verticesCount );
    for ( int i = 0; i < trianglesCount; ++ i )
    {
        const Triangle2D& triangle = triangles[ i ];
        vertices << SphereflakeDisplay::vertices( triangle );
    }

    return vertices;
}

QVector< Vector3Df > SphereflakeDisplay::vertices( const QList< Triangle2D* >& triangles )
{
    QVector< Vector3Df > vertices;

    const int trianglesCount = triangles.size();
    if ( trianglesCount == 0 )
    {
        return vertices;
    }

    vertices.reserve( trianglesCount * Triangle2D::s_verticesCount );
    for ( int i = 0; i < trianglesCount; ++ i )
    {
        const Triangle2D* const triangle = triangles[ i ];
        vertices << SphereflakeDisplay::vertices( *triangle );
    }

    return vertices;
}

QVector< Vector3Df > SphereflakeDisplay::vertices( const QList< Polygon2D >& polygons )
{
    QVector< Vector3Df > vertices;
    foreach ( const Polygon2D& polygon, polygons )
    {
        const QList< Triangle2D* > triangles = polygon.toTriangles();
        vertices << SphereflakeDisplay::vertices( triangles );

        qDeleteAll( triangles );
    }

    return vertices;
}

bool SphereflakeDisplay::isMatrixTypeValid( const SphereflakeDisplay::MatrixType matrixType )
{
    return    matrixType == MatrixTypeModel
           || matrixType == MatrixTypeNormal
           || matrixType == MatrixTypeView
           || matrixType == MatrixTypeProjection;
}

void SphereflakeDisplay::setMatrix(
        const MatrixType aMatrixType,
        const QMatrix4x4& aMatrix,
        const QMatrix3x3* const aNormalMatrix,
        const bool aShouldUpdateDisplay,
        const bool aNotifyCameraChangedIfNeeded )
{
    if ( ! SphereflakeDisplay::isMatrixTypeValid( aMatrixType )
         || aMatrixType == MatrixTypeNormal )
    {
        return;
    }

    QMatrix4x4* const existingMatrix = this->mutableMatrix( aMatrixType );
    if ( existingMatrix == nullptr )
    {
        return;
    }

    if ( MathUtilities::areMatricesEqual( aMatrix, *existingMatrix ) )
    {
        return;
    }

    *existingMatrix = aMatrix;

    this->applyMatrix( aMatrixType, aNormalMatrix, aShouldUpdateDisplay );

    if ( aMatrixType != MatrixTypeView )
    {
        return;
    }

    const Camera newCamera( m_viewMatrix );
    if ( newCamera == m_settings.environmentSettings().camera() )
    {
        return;
    }

    EnvironmentSettings newEnvironmentSettings = m_settings.environmentSettings();
    newEnvironmentSettings.setCamera( newCamera );

    m_settings.setEnvironmentSettings( newEnvironmentSettings );

    if ( aNotifyCameraChangedIfNeeded )
    {
        emit cameraChanged();
    }
}

void SphereflakeDisplay::applyMatrix(
        const SphereflakeDisplay::MatrixType matrixType,
        const QMatrix3x3* const normalMatrix,
        const bool shouldUpdateDisplay )
{
    if ( ! SphereflakeDisplay::isMatrixTypeValid( matrixType )
         || matrixType == MatrixTypeNormal )
    {
        return;
    }

    m_shaderProgram.setUniformMatrix( this->matrixUniformVariableLocation( matrixType ),
                                      this->matrix( matrixType ) );
    if ( matrixType == MatrixTypeModel )
    {
        m_shaderProgram.setUniformMatrix( this->matrixUniformVariableLocation( MatrixTypeNormal ),
                                          normalMatrix != nullptr
                                          ? *normalMatrix
                                          : this->normalMatrixCalculated() );
    }

    if ( shouldUpdateDisplay )
    {
        this->updateGL();
    }
}

void SphereflakeDisplay::applyMatrices( const bool shouldUpdateDisplay )
{
    this->applyMatrix( MatrixTypeModel, nullptr, shouldUpdateDisplay );
    // don't; normal matrix is automatically applied when applying model's matrix
    // this->applyMatrix( MatrixTypeNormal, shouldUpdateDisplay );
    this->applyMatrix( MatrixTypeView, nullptr, shouldUpdateDisplay );
    this->applyMatrix( MatrixTypeProjection, nullptr, shouldUpdateDisplay );
}

const QMatrix4x4& SphereflakeDisplay::matrix( const SphereflakeDisplay::MatrixType matrixType ) const
{
    switch ( matrixType )
    {
        case MatrixTypeModel:      return m_modelMatrix;
        case MatrixTypeView:       return m_viewMatrix;
        case MatrixTypeProjection: return m_projectionMatrix;
    }

    static const QMatrix4x4 invalidMatrix;
    return invalidMatrix;
}

QMatrix4x4* SphereflakeDisplay::mutableMatrix( const SphereflakeDisplay::MatrixType matrixType )
{
    switch ( matrixType )
    {
        case MatrixTypeModel:      return & m_modelMatrix;
        case MatrixTypeView:       return & m_viewMatrix;
        case MatrixTypeProjection: return & m_projectionMatrix;
    }

    return nullptr;
}

QMatrix3x3 SphereflakeDisplay::normalMatrixCalculated() const
{
    return SphereflakeDisplay::normalMatrix( m_modelMatrix );
}

QMatrix4x4 SphereflakeDisplay::projectionMatrixCalculated() const
{
    QMatrix4x4 projectionMatrix;

    const int w = this->width();
    const int h = this->height();
    const int minSize = qMin( w, h );

    static const float volumeSphereSizeFactor = 2.0f;
    static const float volumeHalfDimensionSize = volumeSphereSizeFactor * s_initialSphereDefaultRadius;
    const qreal halfWidth  = qreal( w ) / minSize * volumeHalfDimensionSize;
    const qreal halfHeight = qreal( h ) / minSize * volumeHalfDimensionSize;
    const qreal maxHalfDimensionSize = qMax( halfWidth, halfHeight );
    projectionMatrix.ortho( -halfWidth, halfWidth,
                            -halfHeight, halfHeight,
                            maxHalfDimensionSize, -maxHalfDimensionSize );

    return projectionMatrix;
}

void SphereflakeDisplay::resetProjectionMatrix( const bool shouldUpdateDisplay )
{
    this->setMatrix( MatrixTypeProjection,
                     this->projectionMatrixCalculated(),
                     nullptr,
                     shouldUpdateDisplay );
}

QVector< Vector3Df > SphereflakeDisplay::octahedronTriangles()
{
    const Vector3Df left( - s_initialSphereDefaultRadius, 0.0f, 0.0f );
    const Vector3Df right(  s_initialSphereDefaultRadius, 0.0f, 0.0f );
    const Vector3Df top( 0.0f, s_initialSphereDefaultRadius, 0.0f );
    const Vector3Df bottom( 0.0f, - s_initialSphereDefaultRadius, 0.0f );
    const Vector3Df front( 0.0f, 0.0f,  s_initialSphereDefaultRadius );
    const Vector3Df back( 0.0f, 0.0f, - s_initialSphereDefaultRadius );

    return QVector< Vector3Df >()
           << front << right << top
           << right << back << top
           << back << left << top
           << left << front << top

           << front << left << bottom
           << left << back << bottom
           << back << right << bottom
           << right << front << bottom;
}

void SphereflakeDisplay::loadGeometry()
{
    if ( ! m_isGlInitialized )
    {
        return;
    }

    if ( ! OpenGlUtilities::isUnsignedGlValueValid( m_vbo ) )
    {
        glGenBuffers( 1, & m_vbo );
        LOG_OPENGL_ERROR();
    }

    OpenGlObjectBinder vboBinder = this->vboBinder();
    vboBinder.bind();

    const QVector< Vector3Df > vertices = SphereflakeDisplay::octahedronTriangles();
    m_verticesCount = vertices.size();
    glBufferData( GL_ARRAY_BUFFER,
                  m_verticesCount * sizeof( Vector3Df ),
                  vertices.constData(),
                  GL_STATIC_DRAW );
    LOG_OPENGL_ERROR();
}

void SphereflakeDisplay::setShouldShadeSurface( const bool aShouldShadeSurface )
{
    m_shaderProgram.setUniformVariable1i( this->uniformVariableCachedLocation( UniformVariableIndexShouldShadeSurface ),
                                          aShouldShadeSurface ? 1 : 0 );
}

void SphereflakeDisplay::applySettings(
        const Settings& previousSettings,
        const bool shouldUpdateDisplayIfNeeded )
{
    if ( ! m_isGlInitialized )
    {
        return;
    }

    bool shouldUpdateDisplay = false;

    const bool arePreviousSettingsValid = previousSettings.isValid();

    const SphereflakeSettings& previousSphereflakeSettings = previousSettings.sphereflakeSettings();
    const SphereflakeSettings& sphereflakeSettings = m_settings.sphereflakeSettings();
    const quint64 sphereflakeSettingsChangedTraits = arePreviousSettingsValid
                                                     ? sphereflakeSettings.differingTraits( previousSphereflakeSettings )
                                                     : SphereflakeSettings::TraitsAll;
    if ( sphereflakeSettingsChangedTraits != ChangeNotifier::s_noneTrait )
    {
        shouldUpdateDisplay = true;
    }

    static const quint64 sphereGeometryFormingTraits = SphereflakeSettings::TraitLevelsCount
                                                       | SphereflakeSettings::TraitInitialSphereRadius
                                                       | SphereflakeSettings::TraitChildSphereRadiusFactor
                                                       | SphereflakeSettings::TraitChildSphereOffsetFactor;

    if ( sphereflakeSettingsChangedTraits & sphereGeometryFormingTraits )
    {
        this->resetSpheresDrawData();
    }

    if ( sphereflakeSettingsChangedTraits & SphereflakeSettings::TraitShininess )
    {
        m_shaderProgram.setUniformVariable1f( this->uniformVariableCachedLocation( UniformVariableIndexMaterialShininessLevel ),
                                              sphereflakeSettings.shininess() );
    }

    const EnvironmentSettings& previousEnvironmentSettings = previousSettings.environmentSettings();
    const EnvironmentSettings& environmentSettings = m_settings.environmentSettings();
    const quint64 environmentSettingsChangedTraits = arePreviousSettingsValid
                                                     ? environmentSettings.differingTraits( previousEnvironmentSettings )
                                                     : EnvironmentSettings::TraitsAll;
    if ( sphereflakeSettingsChangedTraits != ChangeNotifier::s_noneTrait )
    {
        shouldUpdateDisplay = true;
    }

    const Light& light = environmentSettings.light();
    const quint64 lightChangedTraits = arePreviousSettingsValid
                                       ? light.differingTraits( previousEnvironmentSettings.light() )
                                       : Light::TraitsAll;
    if ( lightChangedTraits & Light::TraitPosition )
    {
        m_shaderProgram.setUniformVector( this->uniformVariableCachedLocation( UniformVariableIndexLightPosition ),
                                          light.position() );
    }

    if ( lightChangedTraits & Light::TraitColor )
    {
        m_shaderProgram.setUniformColor( this->uniformVariableCachedLocation( UniformVariableIndexLightColor ),
                                         light.color(),
                                         false );
    }

    if ( lightChangedTraits & Light::TraitIntensityLevel )
    {
        m_shaderProgram.setUniformVariable1f( this->uniformVariableCachedLocation( UniformVariableIndexLightIntensityLevel ),
                                              light.intensityLevel() );
    }

    if ( lightChangedTraits & Light::TraitConstantAttenuation )
    {
        m_shaderProgram.setUniformVariable1f( this->uniformVariableCachedLocation( UniformVariableIndexLightAttenuationConstant ),
                                              light.constantAttenuation() );
    }

    if ( lightChangedTraits & Light::TraitLinearAttenuation )
    {
        m_shaderProgram.setUniformVariable1f( this->uniformVariableCachedLocation( UniformVariableIndexLightAttenuationLinear ),
                                              light.linearAttenuation() );
    }

    if ( lightChangedTraits & Light::TraitQuadraticAttenuation )
    {
        m_shaderProgram.setUniformVariable1f( this->uniformVariableCachedLocation( UniformVariableIndexLightAttenuationQuadratic ),
                                              light.quadraticAttenuation() );
    }

    if ( environmentSettingsChangedTraits & EnvironmentSettings::TraitCamera )
    {
        this->setMatrix( MatrixTypeView,
                         environmentSettings.camera().toMatrix(),
                         nullptr,
                         true,
                         false );
    }

    if ( environmentSettingsChangedTraits & EnvironmentSettings::TraitAmbientColor )
    {
        m_shaderProgram.setUniformColor( this->uniformVariableCachedLocation( UniformVariableIndexAmbientColor ),
                                         environmentSettings.ambientColor(),
                                         false );
    }

    if ( shouldUpdateDisplayIfNeeded && shouldUpdateDisplay )
    {
        this->updateGL();
    }
}

void SphereflakeDisplay::drawSpheres()
{
    const int spheresCount = m_spheresDrawData.size();

#ifdef DUMP_PERFORMANCE
    qDebug() << "spheresCount:" << spheresCount;

    QTime t;
    t.start();
#endif

    const QList< QColor >& spheresAlternatingColors = m_settings.sphereflakeSettings().spheresAlternatingColors();
    const int sphereColorsCount = spheresAlternatingColors.size();

    for ( int i = 0;
          i < spheresCount;
          ++ i )
    {
        const SphereDrawData& sphereDrawingData = m_spheresDrawData[ i ];
        this->setMatrix( MatrixTypeModel,
                         sphereDrawingData.modelMatrix,
                         & sphereDrawingData.normalMatrix,
                         false );

        m_shaderProgram.setUniformColor( this->uniformVariableCachedLocation( UniformVariableIndexMaterialColor ),
                                         spheresAlternatingColors[ sphereDrawingData.level % sphereColorsCount ] );
        glDrawArrays( GL_PATCHES,
                      0,
                      m_verticesCount );
        LOG_OPENGL_ERROR();
    }

#ifdef DUMP_PERFORMANCE
    const int time = t.elapsed();
    qDebug() << "spheres drawing time (ms):" << time;
#endif
}

void SphereflakeDisplay::resetSpheresDrawData()
{
    m_spheresDrawData.clear();

    const SphereflakeSettings& sphereflakeSettings = m_settings.sphereflakeSettings();

    QMatrix4x4 mat;
    mat.scale( sphereflakeSettings.initialSphereRadius() / s_initialSphereDefaultRadius );
    this->cacheSphere( mat,
                       0ULL,
                       true );
}

void SphereflakeDisplay::cacheChildSpheres(
        const QMatrix4x4& motherSphereMatrix,
        const quint64&    motherSphereLevelIndex,
        const bool        shouldCacheChildSpheres )
{
    const quint64 childSphereLevelIndex = motherSphereLevelIndex + 1ULL;
    if ( ! this->isLevelIndexValid( childSphereLevelIndex ) )
    {
        return;
    }

    static const int equatorChildSpheresCount = 6;
    static const float angleBetweenAdjacentEquatorChildSpheres = MathUtilities::s_doublePiDegrees / equatorChildSpheresCount;
    for ( int i = 0; i < equatorChildSpheresCount; ++ i )
    {
        this->cacheChildSphere( motherSphereMatrix,
                               childSphereLevelIndex,
                               i * angleBetweenAdjacentEquatorChildSpheres,
                               Value< float >( 0.0f ),
                               shouldCacheChildSpheres );
    }

#define CACHE_AUREOLE
#ifdef CACHE_AUREOLE
    // intentionally remove remainder
    static const int aureoleChildSpheresCount = equatorChildSpheresCount / 2;
    static const float angleBetweenAdjacentAureoleChildSpheres = MathUtilities::s_doublePiDegrees / aureoleChildSpheresCount;

    static const float aureoleEquatorAngle = 60.0;
    for ( int i = 0; i < aureoleChildSpheresCount; ++ i )
    {
        this->cacheChildSphere( motherSphereMatrix,
                                childSphereLevelIndex,
                                angleBetweenAdjacentEquatorChildSpheres / 2.0f
                                + i * angleBetweenAdjacentAureoleChildSpheres,
                                Value< float >( aureoleEquatorAngle, true ),
                                shouldCacheChildSpheres );
    }
#endif
}

void SphereflakeDisplay::cacheChildSphere(
        const QMatrix4x4&     motherSphereMatrix,
        const quint64&        levelIndex,
        const float           zRotationAngle,
        const Value< float >& yRotationAngle,
        const bool            shouldDrawChildSpheres )
{
    const SphereflakeSettings& sphereflakeSettings = m_settings.sphereflakeSettings();

    QMatrix4x4 childSphereMatrix;
    childSphereMatrix.rotate( zRotationAngle, MathUtilities::s_zDir );
    if ( yRotationAngle.isSet() )
    {
        childSphereMatrix.rotate( yRotationAngle.value(), MathUtilities::s_yDir );
    }

    childSphereMatrix.translate( s_initialSphereDefaultRadius * ( 1.0 + sphereflakeSettings.childSphereOffsetFactor() ), 0.0, 0.0 );
    childSphereMatrix.scale( sphereflakeSettings.childSphereRadiusFactor() );
    childSphereMatrix.translate( s_initialSphereDefaultRadius, 0.0, 0.0 );

    childSphereMatrix.rotate( 90.0, MathUtilities::s_zDir );
    childSphereMatrix.rotate( -90.0, MathUtilities::s_xDir );

    this->cacheSphere( motherSphereMatrix * childSphereMatrix,
                       levelIndex,
                       shouldDrawChildSpheres );
}

bool SphereflakeDisplay::isLevelIndexValid( const quint64& levelIndex ) const
{
    const quint64 levelNumber = levelIndex + 1ULL;
    if ( ! SphereflakeSettings::isLevelsCountValid( levelNumber ) )
    {
        return false;
    }

    return levelNumber <= m_settings.sphereflakeSettings().levelsCount();
}

void SphereflakeDisplay::notifyFocusChanged()
{
    emit isFocusedChanged( this->hasFocus() );
}

void SphereflakeDisplay::cacheSphere(
        const QMatrix4x4& sphereMatrix,
        const quint64&    levelIndex,
        const bool        shouldCacheChildSpheres )
{
    if ( ! this->isLevelIndexValid( levelIndex ) )
    {
        return;
    }

    m_spheresDrawData.append( SphereDrawData( levelIndex,
                                              sphereMatrix,
                                              SphereflakeDisplay::normalMatrix( sphereMatrix ) ) );
    if ( shouldCacheChildSpheres )
    {
        this->cacheChildSpheres( sphereMatrix,
                                 levelIndex,
                                 shouldCacheChildSpheres );
    }
}

int SphereflakeDisplay::matrixUniformVariableIndex( const SphereflakeDisplay::MatrixType matrixType )
{
    switch ( matrixType )
    {
        case MatrixTypeModel:      return UniformVariableIndexModelMatrix;
        case MatrixTypeNormal:     return UniformVariableIndexNormalMatrix;
        case MatrixTypeView:       return UniformVariableIndexViewMatrix;
        case MatrixTypeProjection: return UniformVariableIndexProjectionMatrix;
    }

    return UniformVariableIndexInvalid;
}

GLint SphereflakeDisplay::matrixUniformVariableLocation( const SphereflakeDisplay::MatrixType matrixType ) const
{
    return this->uniformVariableCachedLocation( SphereflakeDisplay::matrixUniformVariableIndex( matrixType ) );
}

QMatrix3x3 SphereflakeDisplay::normalMatrix( const QMatrix4x4& matrix )
{
    // the way the normal matrix is derived:
    // http://www.arcsynthesis.org/gltut/Illumination/Tut09%20Normal%20Transformation.html
    // return m_modelMatrix.inverted().transposed().toGenericMatrix< 3, 3 >();

    return matrix.normalMatrix();
}

void SphereflakeDisplay::enableGenericVertexAttributeArrays( const bool enable )
{
    foreach ( const GLuint genericVertexAttributeIndex, s_allOrderedGenericVertexAttributeIndices )
    {
        if ( enable )
        {
            glEnableVertexAttribArray( genericVertexAttributeIndex );
            LOG_OPENGL_ERROR();
        }
        else
        {
            glDisableVertexAttribArray( genericVertexAttributeIndex );
            LOG_OPENGL_ERROR();
        }
    }
}

QByteArray SphereflakeDisplay::uniformVariableName( const int index )
{
    switch ( index )
    {
        case UniformVariableIndexModelMatrix:               return "modelMatrix";
        case UniformVariableIndexNormalMatrix:              return "normalMatrix";
        case UniformVariableIndexViewMatrix:                return "viewMatrix";
        case UniformVariableIndexProjectionMatrix:          return "projectionMatrix";

        case UniformVariableIndexSphereCenterPt:            return "sphereCenterPt";
        case UniformVariableIndexSphereRadius:              return "sphereRadius";

        case UniformVariableIndexAmbientColor:              return "ambientColor";

        case UniformVariableIndexMaterialColor:             return "material.color";
        case UniformVariableIndexMaterialShininessLevel:    return "material.shininessLevel";

        case UniformVariableIndexLightPosition:             return "light.position";
        case UniformVariableIndexLightColor:                return "light.color";
        case UniformVariableIndexLightIntensityLevel:       return "light.intensityLevel";
        case UniformVariableIndexLightAttenuationConstant:  return "light.attenuation.constant";
        case UniformVariableIndexLightAttenuationLinear:    return "light.attenuation.linear";
        case UniformVariableIndexLightAttenuationQuadratic: return "light.attenuation.quadratic";

        case UniformVariableIndexCameraPos:                 return "cameraPos";
        case UniformVariableIndexShouldShadeSurface:        return "shouldShadeSurface";
    }

    return QByteArray();
}

bool SphereflakeDisplay::isUniformVariableIndexValid( const int index )
{
    return 0 <= index && index < UniformVariablesCount;
}

GLint SphereflakeDisplay::uniformVariableLocation( const int index ) const
{
    if ( ! SphereflakeDisplay::isUniformVariableIndexValid( index )
         || ! m_shaderProgram.isValid() )
    {
        return OpenGlUtilities::s_invalidSignedGlValue;
    }

    const QByteArray uniformVariableName = SphereflakeDisplay::uniformVariableName( index );
    return uniformVariableName.isEmpty()
           ? OpenGlUtilities::s_invalidSignedGlValue
           : m_shaderProgram.uniformVarLocation( uniformVariableName );
}

void SphereflakeDisplay::cacheUniformVariableLocations()
{
    m_uniformVariableCachedLocations.clear();
    m_uniformVariableCachedLocations.reserve( UniformVariablesCount );
    for ( int i = 0; i < UniformVariablesCount; ++ i )
    {
        m_uniformVariableCachedLocations.insert( i, this->uniformVariableLocation( i ) );
    }
}

GLint SphereflakeDisplay::uniformVariableCachedLocation( const int index ) const
{
    return m_uniformVariableCachedLocations.value( index, OpenGlUtilities::s_invalidSignedGlValue );
}

void SphereflakeDisplay::mousePressEvent( QMouseEvent* event )
{
    this->QGLWidget::mousePressEvent( event );
}

void SphereflakeDisplay::mouseMoveEvent( QMouseEvent* event )
{
    this->QGLWidget::mouseMoveEvent( event );
}

void SphereflakeDisplay::mouseReleaseEvent( QMouseEvent* event )
{
    this->QGLWidget::mouseReleaseEvent( event );

    if ( ! this->hasFocus() )
    {
        this->setFocus( Qt::MouseFocusReason );
    }
}

void SphereflakeDisplay::keyPressEvent( QKeyEvent* event )
{
    static int i = 0;
    this->QGLWidget::keyPressEvent( event );

    const int keys = event->key();

    QMatrix4x4 viewMatrixModifier;

    static const qreal rotationIncrement = 5.0;
    const bool isRotateBackwardPressed = keys == Qt::Key_X;
    const bool isRotateForwardPressed = keys == Qt::Key_2;
    if ( isRotateBackwardPressed && isRotateForwardPressed )
    {
        return;
    }

    if ( isRotateBackwardPressed || isRotateForwardPressed )
    {
        viewMatrixModifier.rotate( isRotateBackwardPressed
                                   ? rotationIncrement
                                   : - rotationIncrement,
                                   MathUtilities::s_xDir );
    }

    const bool isRotateLeftPressed = keys == Qt::Key_Q;
    const bool isRotateRightPressed = keys == Qt::Key_E;
    if ( isRotateLeftPressed && isRotateRightPressed )
    {
        return;
    }

    if ( isRotateLeftPressed || isRotateRightPressed )
    {
        viewMatrixModifier.rotate( isRotateLeftPressed
                                   ? rotationIncrement
                                   : - rotationIncrement,
                                   MathUtilities::s_yDir );
    }

    Vector3Dd translator;

    static const qreal translationIncrement = 0.5;

    const bool isMoveLeftPressed = keys == Qt::Key_A;
    const bool isMoveRightPressed = keys == Qt::Key_D;
    if ( isMoveLeftPressed && isMoveRightPressed )
    {
        return;
    }

    if ( isMoveLeftPressed || isMoveRightPressed )
    {
        translator.setX( isMoveLeftPressed
                         ? - translationIncrement
                         : translationIncrement );
    }

    const bool isMoveBackwardPressed = keys == Qt::Key_S;
    const bool isMoveForwardPressed = keys == Qt::Key_W;
    if ( isMoveBackwardPressed && isMoveForwardPressed )
    {
        return;
    }

    if ( isMoveBackwardPressed || isMoveForwardPressed )
    {
        translator.setZ( isMoveBackwardPressed
                         ? translationIncrement
                         : - translationIncrement );
    }

    const bool isMoveDownPressed = keys == Qt::Key_F;
    const bool isMoveUpPressed = keys == Qt::Key_R;
    if ( isMoveDownPressed && isMoveUpPressed )
    {
        return;
    }

    if ( isMoveDownPressed || isMoveUpPressed )
    {
        translator.setY( isMoveDownPressed
                         ? - translationIncrement
                         : translationIncrement );
    }

    if ( ! translator.isZero() )
    {
        viewMatrixModifier.translate( translator.toQVector3D() );
    }

    if ( viewMatrixModifier.isIdentity() )
    {
        return;
    }

    this->setMatrix( MatrixTypeView, m_viewMatrix * viewMatrixModifier.inverted() );
}

void SphereflakeDisplay::focusInEvent( QFocusEvent* event )
{
    this->QGLWidget::focusInEvent( event );

    this->notifyFocusChanged();
}

void SphereflakeDisplay::focusOutEvent( QFocusEvent* event )
{
    this->QGLWidget::focusOutEvent( event );

    this->notifyFocusChanged();
}

#ifndef QT_NO_WHEELEVENT
void SphereflakeDisplay::wheelEvent( QWheelEvent* event )
{
    this->QGLWidget::wheelEvent( event );

    const float zoomFactorDelta = event->delta() / 8.0 / MathUtilities::s_doublePiDegrees;

    Camera newCamera = m_settings.environmentSettings().camera();
    newCamera.setZoomFactor( newCamera.zoomFactor() + zoomFactorDelta );
    this->setMatrix( MatrixTypeView, newCamera.toMatrix() );
}
#endif

void SphereflakeDisplay::init()
{
    if ( m_isGlInitialized )
    {
        return;
    }

    m_isGlInitialized = true;

    if ( OpenGlUtilities::isUnsignedGlValueValid( m_vao )
         && OpenGlUtilities::isUnsignedGlValueValid( m_vbo )
         && ! m_uniformVariableCachedLocations.isEmpty() )
    {
        return;
    }

    // problem with nVidia chips:
    // http://stackoverflow.com/questions/8302625/segmentation-fault-at-glgenvertexarrays-1-vao
    glewExperimental = GL_TRUE;

    GLenum err = glewInit();
    LOG_OPENGL_ERROR();
    if ( GLEW_OK != err )
    {
        qDebug().nospace() << "Error initializing GLEW: " << QString::fromLatin1( reinterpret_cast< const char* >( glewGetErrorString( err ) ) );
        return;
    }

    const ShaderInfo vertexShaderInfo(
                ShaderInfo::ShaderTypeVertex,
                GeneralUtilities::fileContents( "://shaders/Sphereflake.vert" ) );

    const ShaderInfo tessControlShaderInfo(
                ShaderInfo::ShaderTypeTessellationControl,
                GeneralUtilities::fileContents( "://shaders/Sphereflake.tc" ) );

    const ShaderInfo tessEvaluationShaderInfo(
                ShaderInfo::ShaderTypeTessellationEvaluation,
                GeneralUtilities::fileContents( "://shaders/Sphereflake.te" ) );

    const ShaderInfo fragmentShaderInfo(
                ShaderInfo::ShaderTypeFragment,
                GeneralUtilities::fileContents( "://shaders/Sphereflake.frag" ) );

    m_shaderProgram.setShaders(
                QList< ShaderInfo >()
                << vertexShaderInfo
                << tessControlShaderInfo
                << tessEvaluationShaderInfo
                << fragmentShaderInfo );

    const bool isBuiltOk = m_shaderProgram.build();
    if ( ! isBuiltOk )
    {
        return;
    }

#ifdef DRAW_TRIANGLES_BORDERS
    glEnable( GL_LINE_SMOOTH );
    LOG_OPENGL_ERROR();

    glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
    LOG_OPENGL_ERROR();
#endif

    glEnable( GL_POLYGON_SMOOTH );
    LOG_OPENGL_ERROR();

    glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
    LOG_OPENGL_ERROR();

    glEnable( GL_DEPTH_TEST );
    LOG_OPENGL_ERROR();

#ifdef DRAW_TRIANGLES_BORDERS
    LineWidthAlternator lineWidthAlternator( 2.0f, false );
    lineWidthAlternator.change();
#endif

    ClearColorAlternator clearColorAlternator( this->palette().color( QPalette::Window ), false );
    clearColorAlternator.change();

    OpenGlCapabilityEnabler cullFaceEnabler( GL_CULL_FACE, true, false );
    cullFaceEnabler.change();

    OpenGlObjectBinder cullFaceModeAlternator(
                OpenGlObjectBinder::ObjectTypeCullFaceMode,
                GL_BACK,
                OpenGlUtilities::invalidEnumGlValue(),
                false );
    cullFaceModeAlternator.bind();

    this->cacheUniformVariableLocations();
    this->loadGeometry();
    this->applyMatrices( false );

    m_shaderProgram.setUniformVector( this->uniformVariableCachedLocation( UniformVariableIndexSphereCenterPt ),
                                      Vector3Df( 0.0f ) );
    m_shaderProgram.setUniformVariable1f( this->uniformVariableCachedLocation( UniformVariableIndexSphereRadius ),
                                          s_initialSphereDefaultRadius );
    m_shaderProgram.setUniformVector( this->uniformVariableCachedLocation( UniformVariableIndexCameraPos ),
                                      Vector3Df( 0.0f ) );

    this->applySettings();
    this->setShouldShadeSurface( true );

    OpenGlObjectBinder vboBinder = this->vboBinder( false );
    vboBinder.bind();

    glGenVertexArrays( 1, & m_vao );
    LOG_OPENGL_ERROR();

    OpenGlObjectBinder vaoBinder = this->vaoBinder( false );
    vaoBinder.bind();

    this->enableGenericVertexAttributeArrays( true );

    glVertexAttribPointer( GenericVertexAttributeIndexVertexPos,
                           MathUtilities::s_axesCount3D,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof( Vector3Df ),
                           nullptr );
    LOG_OPENGL_ERROR();

    vaoBinder.unbind();
    vboBinder.unbind();

    this->enableGenericVertexAttributeArrays( false );
}

void SphereflakeDisplay::printGLInfo()
{
#ifdef THOROUGH_LOGGING_ENABLED
    const GLubyte* renderer = glGetString( GL_RENDERER );
    LOG_OPENGL_ERROR();

    const GLubyte* vendor = glGetString( GL_VENDOR );
    LOG_OPENGL_ERROR();

    const GLubyte* version = glGetString( GL_VERSION );
    LOG_OPENGL_ERROR();

    const GLubyte* glslVersion = glGetString( GL_SHADING_LANGUAGE_VERSION );
    LOG_OPENGL_ERROR();

    GLint major = -1;
    GLint minor = -1;
    glGetIntegerv( GL_MAJOR_VERSION, &major );
    LOG_OPENGL_ERROR();

    glGetIntegerv( GL_MINOR_VERSION, &minor );
    LOG_OPENGL_ERROR();

    GLint maxVertexTextureUnitsCount = 0;
    glGetIntegerv( GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, & maxVertexTextureUnitsCount );
    LOG_OPENGL_ERROR();

    GLint maxFragmentTextureUnitsCount = 0;
    glGetIntegerv( GL_MAX_TEXTURE_IMAGE_UNITS, & maxFragmentTextureUnitsCount );
    LOG_OPENGL_ERROR();

    GLint maxCombinedTextureUnitsCount = 0;
    glGetIntegerv( GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, & maxCombinedTextureUnitsCount );
    LOG_OPENGL_ERROR();

    qDebug() << "GL Vendor:" << QLatin1String( reinterpret_cast< const char* > ( vendor ) ) << endl
             << "GL Renderer:" << QLatin1String( reinterpret_cast< const char* > ( renderer ) ) << endl
             << "GL Version (string):" << QLatin1String( reinterpret_cast< const char* > ( version ) ) << endl
             << "GL Version (integer):" << QString( "%1.%2" ).arg( major ).arg( minor ) << endl
             << "GLSL Version:" << QLatin1String( reinterpret_cast< const char* > ( glslVersion ) ) << endl
             << "Vertex texture units count:" << maxVertexTextureUnitsCount << endl
             << "Fragment texture units count:" << maxFragmentTextureUnitsCount << endl
             << "Combined texture units count:" << maxCombinedTextureUnitsCount;

    GLint extensionsCount = 0;
    glGetIntegerv( GL_NUM_EXTENSIONS, &extensionsCount );
    LOG_OPENGL_ERROR();

    qDebug() << "extensions:";

    const int extensionsCountDigitsCount = qFloor( log10( static_cast< float >( extensionsCount ) ) ) + 1;
    for ( int i = 0; i < extensionsCount; ++i )
    {
        qDebug().nospace() << '\t' << QString( "%1" ).arg( i, extensionsCountDigitsCount, 10, QChar( QLatin1Char( ' ' ) ) ) << ": " << QLatin1String( reinterpret_cast< const char* > ( glGetStringi( GL_EXTENSIONS, i ) ) );
        LOG_OPENGL_ERROR();
    }

    GLint frontFace = -1;
    glGetIntegerv( GL_FRONT_FACE, & frontFace );

    qDebug() << "GL_FRONT_FACE:" << ( frontFace == GL_CCW
                                      ? "CCW"
                                      : ( frontFace == GL_CW
                                          ? "CW"
                                          : "unknown" ) );
#endif
}


SphereflakeDisplay::SphereDrawData::SphereDrawData(
        const quint64& aLevel,
        const QMatrix4x4& aModelMatrix,
        const QMatrix3x3& aNormalMatrix )
    : level( aLevel )
    , modelMatrix( aModelMatrix )
    , normalMatrix( aNormalMatrix )
{
}
