#include "LightEditor.h"

#include "ColorPicker.h"
#include "Data/LightTypesModel.h"
#include "Vec3Editor.h"

#include <QComboBox>
#include <QDoubleSpinBox>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>

using namespace Sphereflake;

LightEditor::LightEditor( QWidget* parent, Qt::WindowFlags f )
    : QWidget( parent, f )
    , m_typeLabel( nullptr )
    , m_typeEditor( nullptr )
    , m_positionLabel( nullptr )
    , m_positionEditor( nullptr )
    , m_directionLabel( nullptr )
    , m_directionEditor( nullptr )
    , m_colorLabel( nullptr )
    , m_colorEditor( nullptr )
    , m_intensityLevelLabel( nullptr )
    , m_intensityLevelEditor( nullptr )
    , m_cutoffAngleLabel( nullptr )
    , m_cutoffAngleEditor( nullptr )
    , m_coneConcentrationExponentLabel( nullptr )
    , m_coneConcentrationExponentEditor( nullptr )
    , m_constantAttenuationLabel( nullptr )
    , m_constantAttenuationEditor( nullptr )
    , m_linearAttenuationLabel( nullptr )
    , m_linearAttenuationEditor( nullptr )
    , m_quadraticAttenuationLabel( nullptr )
    , m_quadraticAttenuationEditor( nullptr )
{
    m_typeLabel = new QLabel;
    m_typeEditor = new QComboBox;
    m_typeEditor->setModel( & LightTypesModel::instance() );
    connect( m_typeEditor, SIGNAL(currentIndexChanged(int)), this, SLOT(applyType()) );

    m_positionLabel = new QLabel;
    m_positionEditor = new Vec3Editor;

    m_directionLabel = new QLabel;
    m_directionEditor = new Vec3Editor;

    m_colorLabel = new QLabel;
    m_colorEditor = new ColorPicker;

    m_intensityLevelLabel = new QLabel;
    m_intensityLevelEditor = new QDoubleSpinBox;
    m_intensityLevelEditor->setRange( Light::s_intensityLevelMin, Light::s_intensityLevelMax );
    m_intensityLevelEditor->setSingleStep( 0.001 );
    m_intensityLevelEditor->setDecimals( 3 );

    m_cutoffAngleLabel = new QLabel;
    m_cutoffAngleEditor = new QDoubleSpinBox;
    m_cutoffAngleEditor->setRange( Light::s_cutoffAngleMin, Light::s_cutoffAngleMax );
    m_cutoffAngleEditor->setSingleStep( 0.001 );
    m_cutoffAngleEditor->setDecimals( 3 );
    m_cutoffAngleEditor->setSuffix( QChar( 0x00B0 ) );

    m_coneConcentrationExponentLabel = new QLabel;
    m_coneConcentrationExponentEditor = new QDoubleSpinBox;
    m_coneConcentrationExponentEditor->setRange( Light::s_coneConcentrationExponentMin, Light::s_coneConcentrationExponentMax );
    m_coneConcentrationExponentEditor->setSingleStep( 0.001 );
    m_coneConcentrationExponentEditor->setDecimals( 3 );

    m_constantAttenuationLabel = new QLabel;
    m_constantAttenuationEditor = new QDoubleSpinBox;
    m_constantAttenuationEditor->setRange( Light::s_attenuationMin, Light::s_attenuationMax );
    m_constantAttenuationEditor->setSingleStep( 0.001 );
    m_constantAttenuationEditor->setDecimals( 3 );

    m_linearAttenuationLabel = new QLabel;
    m_linearAttenuationEditor = new QDoubleSpinBox;
    m_linearAttenuationEditor->setRange( Light::s_attenuationMin, Light::s_attenuationMax );
    m_linearAttenuationEditor->setSingleStep( 0.001 );
    m_linearAttenuationEditor->setDecimals( 3 );

    m_quadraticAttenuationLabel = new QLabel;
    m_quadraticAttenuationEditor = new QDoubleSpinBox;
    m_quadraticAttenuationEditor->setRange( Light::s_attenuationMin, Light::s_attenuationMax );
    m_quadraticAttenuationEditor->setSingleStep( 0.001 );
    m_quadraticAttenuationEditor->setDecimals( 3 );

    QGridLayout* const mainLayout = new QGridLayout( this );
    mainLayout->setContentsMargins( QMargins() );
    mainLayout->addWidget( m_typeLabel, 0, 0 );
    mainLayout->addWidget( m_typeEditor, 0, 1 );
    mainLayout->addWidget( m_positionLabel, 1, 0 );
    mainLayout->addWidget( m_positionEditor, 1, 1 );
    mainLayout->addWidget( m_directionLabel, 2, 0 );
    mainLayout->addWidget( m_directionEditor, 2, 1 );
    mainLayout->addWidget( m_colorLabel, 3, 0 );
    mainLayout->addWidget( m_colorEditor, 3, 1 );
    mainLayout->addWidget( m_intensityLevelLabel, 4, 0 );
    mainLayout->addWidget( m_intensityLevelEditor, 4, 1 );
    mainLayout->addWidget( m_cutoffAngleLabel, 5, 0 );
    mainLayout->addWidget( m_cutoffAngleEditor, 5, 1 );
    mainLayout->addWidget( m_coneConcentrationExponentLabel, 6, 0 );
    mainLayout->addWidget( m_coneConcentrationExponentEditor, 6, 1 );
    mainLayout->addWidget( m_constantAttenuationLabel, 7, 0 );
    mainLayout->addWidget( m_constantAttenuationEditor, 7, 1 );
    mainLayout->addWidget( m_linearAttenuationLabel, 8, 0 );
    mainLayout->addWidget( m_linearAttenuationEditor, 8, 1 );
    mainLayout->addWidget( m_quadraticAttenuationLabel, 9, 0 );
    mainLayout->addWidget( m_quadraticAttenuationEditor, 9, 1 );

    this->setLight( Light::defaultLight() );
    this->applyType();
    this->retranslateSelfOnly();
}

void LightEditor::retranslate()
{
    this->retranslateSelfOnly();
}

void LightEditor::setLight( const Light& aLight )
{
    if ( ! aLight.isValid() )
    {
        return;
    }

    this->setType( aLight.type() );
    m_positionEditor->setValue( aLight.position() );
    m_directionEditor->setValue( aLight.direction() );
    m_colorEditor->setColor( aLight.color() );
    m_intensityLevelEditor->setValue( aLight.intensityLevel() );
    m_cutoffAngleEditor->setValue( aLight.cutoffAngle() );
    m_coneConcentrationExponentEditor->setValue( aLight.coneConcentrationExponent() );
    m_constantAttenuationEditor->setValue( aLight.constantAttenuation() );
    m_linearAttenuationEditor->setValue( aLight.linearAttenuation() );
    m_quadraticAttenuationEditor->setValue( aLight.quadraticAttenuation() );
}

Light Sphereflake::LightEditor::light() const
{
    return Light(
                this->type(),
                m_positionEditor->value(),
                m_directionEditor->value(),
                m_colorEditor->color(),
                m_intensityLevelEditor->value(),
                m_cutoffAngleEditor->value(),
                m_coneConcentrationExponentEditor->value(),
                m_constantAttenuationEditor->value(),
                m_linearAttenuationEditor->value(),
                m_quadraticAttenuationEditor->value() );
}

void LightEditor::retranslateSelfOnly()
{
    m_typeLabel->setText( tr( "Type:" ) );
    m_positionLabel->setText( tr( "Position:" ) );
    m_directionLabel->setText( tr( "Direction:" ) );
    m_colorLabel->setText( tr( "Color:" ) );
    m_intensityLevelLabel->setText( tr( "Intensity Level:" ) );
    m_cutoffAngleLabel->setText( tr( "Cutoff Angle:" ) );
    m_coneConcentrationExponentLabel->setText( tr( "Cone Concentration Exponent:" ) );
    m_constantAttenuationLabel->setText( tr( "Constant Attenuation:" ) );
    m_linearAttenuationLabel->setText( tr( "Linear Attenuation:" ) );
    m_quadraticAttenuationLabel->setText( tr( "Quadratic Attenuation:" ) );
}

void LightEditor::setType( const Light::Type aType )
{
    if ( ! Light::isTypeValid( aType ) )
    {
        return;
    }

    m_typeEditor->setCurrentIndex( LightTypesModel::instance().rowIndex( aType ) );
}

void LightEditor::applyType()
{
    const Light::Type type = this->type();
    const bool isDirectional = type == Light::TypeDirectional;
    const bool isSpot        = type == Light::TypeSpot;
    const bool isDirectionalOrSpot = isDirectional || isSpot;
    m_directionLabel->setEnabled( isDirectionalOrSpot );
    m_directionEditor->setEnabled( isDirectionalOrSpot );

    m_cutoffAngleLabel->setEnabled( isSpot );
    m_cutoffAngleEditor->setEnabled( isSpot );

    m_coneConcentrationExponentLabel->setEnabled( isSpot );
    m_coneConcentrationExponentEditor->setEnabled( isSpot );
}

Light::Type LightEditor::type() const
{
    QAbstractItemModel* const lightTypesModel = m_typeEditor->model();
    const QVariant lightTypeRaw = lightTypesModel->data( lightTypesModel->index( m_typeEditor->currentIndex(), 0 ), Qt::UserRole );
    bool converted = false;
    const Light::Type lightType = static_cast< Light::Type >( lightTypeRaw.toInt( & converted ) );

    return converted && Light::isTypeValid( lightType )
           ? lightType
           : Light::TypeInvalid;
}
