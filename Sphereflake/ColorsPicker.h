#ifndef SPHEREFLAKE_COLORSPICKER_H
#define SPHEREFLAKE_COLORSPICKER_H

#include "Data/NumberRange.h"
#include "Utilities/Retranslatable.h"

#include <QColor>
#include <QList>
#include <QPair>
#include <QSet>
#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QGridLayout)
QT_FORWARD_DECLARE_CLASS(QLabel)
QT_FORWARD_DECLARE_CLASS(QPushButton)
QT_FORWARD_DECLARE_CLASS(QSpinBox)

namespace Sphereflake {

class ColorPicker;

using namespace Sphereflake::Data;
using namespace Sphereflake::Utilities;

class ColorsPicker : public QWidget, public Retranslatable
{
    Q_OBJECT

public:
    static const int s_minColorsCountDefault;
    static const int s_maxColorsCountDefault;
    static const int s_areColorsMutuallyExclusiveDefault;

public:
    explicit ColorsPicker( QWidget* const parent = nullptr, Qt::WindowFlags f = 0 );

    void setColors( const QList< QColor >& aColors );
    QList< QColor > colors() const;
    QSet< QColor > uniqueColors() const;
    bool areColorsValid( const QList< QColor >& aColors ) const;

    void setMinColorsCount( const int aMinColorsCount );
    int minColorsCount() const;

    void setMaxColorsCount( const int aMaxColorsCount );
    int maxColorsCount() const;

    void setColorsCountRange( const int aMinColorsCount, const int aMaxColorsCount );

    void setAreColorsMutuallyExclusive( const bool aAreColorsMutuallyExclusive );
    bool areColorsMutuallyExclusive() const;

    void retranslate() /* override */;

private slots:
    void removeLastColor();
    void addColor();

private:
    void retranslateSelfOnly();
    static QString colorLabelText( const int colorIndex );

    void setColorsCount( const int aSpheresAlternatingColorsCount );
    int colorsCount() const;
    bool isColorsCountValid( const int aColorsCount ) const;

    void applyColorsCountRange();
    void applyAreColorsMutuallyExclusive();

private:
    QList< QPair< QLabel*, ColorPicker* > > m_colorLabelPickers;
    QPushButton*                            m_addColorButton;
    QPushButton*                            m_removeColorButton;
    QGridLayout*                            m_colorsLayout;

    NumberRange                             m_colorsCountRange;
    bool                                    m_areColorsMutuallyExclusive;
};

}

#endif // SPHEREFLAKE_COLORSPICKER_H
