#include "SphereflakeSettingsEditor.h"

#include "ColorsPicker.h"
#include "Vec3Editor.h"

#include <QDoubleSpinBox>
#include <QGridLayout>
#include <QLabel>
#include <QSpinBox>

using namespace Sphereflake;

SphereflakeSettingsEditor::SphereflakeSettingsEditor( QWidget* parent, Qt::WindowFlags f )
    : QWidget( parent, f )
    , m_levelsCountLabel( nullptr )
    , m_levelsCountEditor( nullptr )
    , m_initialSphereRadiusLabel( nullptr )
    , m_initialSphereRadiusEditor( nullptr )
    , m_childSphereRadiusFactorLabel( nullptr )
    , m_childSphereRadiusFactorEditor( nullptr )
    , m_childSphereOffsetFactorLabel( nullptr )
    , m_childSphereOffsetFactorEditor( nullptr )
    , m_colorsPicker( nullptr )
    , m_shininessLabel( nullptr )
    , m_shininessEditor( nullptr )
{
    m_levelsCountLabel = new QLabel;
    m_levelsCountEditor = new QSpinBox;
    m_levelsCountEditor->setRange( SphereflakeSettings::s_levelsCountMin,
                                   qBound( 0ULL,
                                           SphereflakeSettings::s_levelsCountMax,
                                           static_cast< quint64 >( std::numeric_limits< int >::max() ) ) );

    m_initialSphereRadiusLabel = new QLabel;
    m_initialSphereRadiusEditor = new QDoubleSpinBox;
    m_initialSphereRadiusEditor->setRange( SphereflakeSettings::s_initialSphereRadiusMin, SphereflakeSettings::s_initialSphereRadiusMax );
    m_initialSphereRadiusEditor->setSingleStep( 0.001 );
    m_initialSphereRadiusEditor->setDecimals( 3 );

    m_childSphereRadiusFactorLabel = new QLabel;
    m_childSphereRadiusFactorEditor = new QDoubleSpinBox;
    m_childSphereRadiusFactorEditor->setRange( SphereflakeSettings::s_childSphereRadiusFactorMin,
                                               SphereflakeSettings::s_childSphereRadiusFactorMax );
    m_childSphereRadiusFactorEditor->setSingleStep( 0.001 );
    m_childSphereRadiusFactorEditor->setDecimals( 3 );

    m_childSphereOffsetFactorLabel = new QLabel;
    m_childSphereOffsetFactorEditor = new QDoubleSpinBox;
    m_childSphereOffsetFactorEditor->setRange( SphereflakeSettings::s_childSphereOffsetFactorMin,
                                               SphereflakeSettings::s_childSphereOffsetFactorMax );
    m_childSphereOffsetFactorEditor->setSingleStep( 0.001 );
    m_childSphereOffsetFactorEditor->setDecimals( 3 );

    m_colorsPicker = new ColorsPicker;
    m_colorsPicker->setAreColorsMutuallyExclusive( true );
    m_colorsPicker->setColorsCountRange( SphereflakeSettings::s_spheresAlternatingColorsCountMin,
                                         SphereflakeSettings::s_spheresAlternatingColorsCountMax );

    m_shininessLabel = new QLabel;
    m_shininessEditor = new QDoubleSpinBox;
    m_shininessEditor->setRange( SphereflakeSettings::s_shininessMin,
                                 SphereflakeSettings::s_shininessMax );
    m_shininessEditor->setSingleStep( 0.001 );
    m_shininessEditor->setDecimals( 3 );

    QGridLayout* const mainLayout = new QGridLayout( this );
    mainLayout->setContentsMargins( QMargins() );
    mainLayout->addWidget( m_levelsCountLabel, 0, 0 );
    mainLayout->addWidget( m_levelsCountEditor, 0, 1 );
    mainLayout->addWidget( m_initialSphereRadiusLabel, 1, 0 );
    mainLayout->addWidget( m_initialSphereRadiusEditor, 1, 1 );
    mainLayout->addWidget( m_childSphereRadiusFactorLabel, 2, 0 );
    mainLayout->addWidget( m_childSphereRadiusFactorEditor, 2, 1 );
    mainLayout->addWidget( m_childSphereOffsetFactorLabel, 3, 0 );
    mainLayout->addWidget( m_childSphereOffsetFactorEditor, 3, 1 );
    mainLayout->addWidget( m_colorsPicker, 4, 0, 1, 2 );
    mainLayout->addWidget( m_shininessLabel, 5, 0 );
    mainLayout->addWidget( m_shininessEditor, 5, 1 );

    this->setSphereflakeSettings( SphereflakeSettings::defaultSphereflakeSettings() );
    this->retranslateSelfOnly();
}

void SphereflakeSettingsEditor::setSphereflakeSettings( const SphereflakeSettings& aSphereflakeSettings )
{
    if ( ! aSphereflakeSettings.isValid() )
    {
        return;
    }

    m_levelsCountEditor->setValue( aSphereflakeSettings.levelsCount() );
    m_initialSphereRadiusEditor->setValue( aSphereflakeSettings.initialSphereRadius() );
    m_childSphereRadiusFactorEditor->setValue( aSphereflakeSettings.childSphereRadiusFactor() );
    m_childSphereOffsetFactorEditor->setValue( aSphereflakeSettings.childSphereOffsetFactor() );
    m_colorsPicker->setColors( aSphereflakeSettings.spheresAlternatingColors() );
    m_shininessEditor->setValue( aSphereflakeSettings.shininess() );
}

void SphereflakeSettingsEditor::retranslate()
{
    this->retranslateSelfOnly();
}

SphereflakeSettings Sphereflake::SphereflakeSettingsEditor::sphereflakeSettings() const
{
#ifdef max
    #undef max
#endif

    return SphereflakeSettings(
                qBound( 0, m_levelsCountEditor->value(), std::numeric_limits< int >::max() ),
                m_initialSphereRadiusEditor->value(),
                m_childSphereRadiusFactorEditor->value(),
                m_childSphereOffsetFactorEditor->value(),
                m_colorsPicker->colors(),
                m_shininessEditor->value() );
}

void SphereflakeSettingsEditor::retranslateSelfOnly()
{
    m_levelsCountLabel->setText( tr( "Levels Count:" ) );
    m_initialSphereRadiusLabel->setText( tr( "Initial Sphere Radius:" ) );
    m_childSphereRadiusFactorLabel->setText( tr( "Child Sphere Radius Factor:" ) );
    m_childSphereOffsetFactorLabel->setText( tr( "Child Sphere Offset Factor:" ) );
    m_shininessLabel->setText( tr( "Shininess:" ) );
}
