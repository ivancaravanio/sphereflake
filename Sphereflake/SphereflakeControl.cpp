#include "SphereflakeControl.h"

#include "SettingsEditor.h"
#include "SphereflakeDisplay.h"

#include <QAction>
#include <QFrame>
#include <QGLFormat>
#include <QHBoxLayout>
#include <QSplitter>
#include <QVBoxLayout>

using namespace Sphereflake;

SphereflakeControl::SphereflakeControl( QWidget* parent )
    : QWidget( parent )
    , m_sphereflakeDisplay( nullptr )
    , m_sphereflakeDisplayFocusFrame( nullptr )
    , m_settingsEditor( nullptr )
    , m_splitter( nullptr )
{
    QGLFormat glFormat;
    glFormat.setVersion( 4, 0 );
    glFormat.setProfile( QGLFormat::CoreProfile );

    m_sphereflakeDisplay = new SphereflakeDisplay( glFormat );
    m_sphereflakeDisplay->setFocusPolicy( static_cast< Qt::FocusPolicy > ( m_sphereflakeDisplay->focusPolicy() | Qt::WheelFocus ) );

    // Another way round is to use a QFocusFrame but don't know how to use it correctly or don't see its effect.
    // QGLWidget cannot be styled to have a border using qss. A way to do this is described here:
    // https://forum.qt.io/topic/26571/solved-qt-stylesheets-border-on-a-qglwidget

    m_sphereflakeDisplayFocusFrame = new QFrame;
    m_sphereflakeDisplayFocusFrame->setFrameShape( QFrame::Panel );
    m_sphereflakeDisplayFocusFrame->setFocusProxy( m_sphereflakeDisplay );
    QHBoxLayout* const sphereflakeDisplayFocusFrameLayout = new QHBoxLayout( m_sphereflakeDisplayFocusFrame );
    sphereflakeDisplayFocusFrameLayout->addWidget( m_sphereflakeDisplay );
    this->updateSphereflakeDisplayFocusFrame();

    connect( m_sphereflakeDisplay,
             SIGNAL(isFocusedChanged(bool)),
             this,
             SLOT(updateSphereflakeDisplayFocusFrame()) );

    QAction* const focusSphereflakeDisplayAction = new QAction( this );
    focusSphereflakeDisplayAction->setShortcut( Qt::CTRL + Qt::Key_F );
    connect( focusSphereflakeDisplayAction,
             SIGNAL(triggered(bool)),
             m_sphereflakeDisplay,
             SLOT(setFocus()) );

    this->addAction( focusSphereflakeDisplayAction );

    m_settingsEditor = new SettingsEditor;
    this->applySettingsToDisplay();
    connect( m_settingsEditor, SIGNAL(settingsChanged()), this, SLOT(applySettingsToDisplay()) );
    connect( m_sphereflakeDisplay, SIGNAL(cameraChanged()), this, SLOT(applySettingsFromDisplay()) );

    QWidget* const settingsEditorAligner = new QWidget;
    QHBoxLayout* const settingsEditorAlignerLayout = new QHBoxLayout( settingsEditorAligner );
    settingsEditorAlignerLayout->setContentsMargins( QMargins() );
    settingsEditorAlignerLayout->addWidget( m_settingsEditor, 0, Qt::AlignTop );

    m_splitter = new QSplitter;
    m_splitter->addWidget( m_sphereflakeDisplayFocusFrame );
    m_splitter->addWidget( settingsEditorAligner );
    m_splitter->setStyleSheet( "QSplitter::handle:horizontal { image: url(://HSplitterHandle.png); width: 10px; }" );

    QHBoxLayout* const mainLayout = new QHBoxLayout( this );
    mainLayout->addWidget( m_splitter );
}

void SphereflakeControl::showEvent( QShowEvent* event )
{
    this->QWidget::showEvent( event );

    const QList< int > sizes = m_splitter->sizes();

    int halfSize = 0;
    foreach ( const int size, sizes )
    {
        halfSize += size;
    }

    halfSize /= 2;

    m_splitter->setSizes( QList< int >() << halfSize << halfSize );
}

void SphereflakeControl::applySettingsToDisplay()
{
    m_sphereflakeDisplay->setSettings( m_settingsEditor->settings() );
}

void SphereflakeControl::applySettingsFromDisplay()
{
    m_settingsEditor->setSettings( m_sphereflakeDisplay->settings() );
}

void SphereflakeControl::updateSphereflakeDisplayFocusFrame()
{
    m_sphereflakeDisplayFocusFrame->setFrameShadow( m_sphereflakeDisplay->hasFocus()
                                                    ? QFrame::Sunken
                                                    : QFrame::Raised );
}
