#include "Light.h"

#include "../Sphereflake_namespace.h"
#include "../Utilities/MathUtilities.h"

#include <limits>

using namespace Sphereflake::Data;

using namespace Sphereflake::Utilities;

const Light::Type Light::s_typeDefault = Light::TypePoint;
const Vector3Df Light::s_positionDefault( 2.0f );
const Vector3Df Light::s_directionDefault = Vector3Df( -1.0f ).normalized();
const QColor Light::s_colorDefault( Qt::white );

const float Light::s_intensityLevelMin     = 0.0f;
const float Light::s_intensityLevelDefault = 1.0f;
const float Light::s_intensityLevelMax     = FLT_MAX;
const float Light::s_intensityLevelInvalid = -1.0f;

const float Light::s_cutoffAngleMin     = 0.0f;
const float Light::s_cutoffAngleDefault = 90.0f;
const float Light::s_cutoffAngleMax     = 90.0f;
const float Light::s_cutoffAngleInvalid = -1.0f;

const float Light::s_coneConcentrationExponentMin     = 0.0f;
const float Light::s_coneConcentrationExponentDefault = 0.0f;
const float Light::s_coneConcentrationExponentMax     = FLT_MAX;
const float Light::s_coneConcentrationExponentInvalid = -1.0;

const float Light::s_attenuationMin     = 0.0f;
const float Light::s_attenuationDefault = 0.0f;
const float Light::s_attenuationMax     = FLT_MAX;
const float Light::s_attenuationInvalid = -1.0;

Light::Light()
    : m_type( TypeInvalid )
    , m_intensityLevel( s_intensityLevelInvalid )
    , m_cutoffAngle( s_cutoffAngleInvalid )
    , m_coneConcentrationExponent( s_coneConcentrationExponentInvalid )
    , m_constantAttenuation( s_attenuationInvalid )
    , m_linearAttenuation( s_attenuationInvalid )
    , m_quadraticAttenuation( s_attenuationInvalid )
{
}

Light::Light(
        const Type       aType,
        const Vector3Df& aPosition,
        const Vector3Df& aDirection,
        const QColor&    aColor,
        const float      aIntensityLevel,
        const float      aCutoffAngle,
        const float      aConeConcentrationExponent,
        const float      aConstantAttenuation,
        const float      aLinearAttenuation,
        const float      aQuadraticAttenuation )
    : m_type( TypeInvalid )
    , m_intensityLevel( s_intensityLevelInvalid )
    , m_cutoffAngle( s_cutoffAngleInvalid )
    , m_coneConcentrationExponent( s_coneConcentrationExponentInvalid )
    , m_constantAttenuation( s_attenuationInvalid )
    , m_linearAttenuation( s_attenuationInvalid )
    , m_quadraticAttenuation( s_attenuationInvalid )
{
    this->set( aType,
               aPosition,
               aDirection,
               aColor,
               aIntensityLevel,
               aCutoffAngle,
               aConeConcentrationExponent,
               aConstantAttenuation,
               aLinearAttenuation,
               aQuadraticAttenuation );
}

void Light::set( const Light& other )
{
    this->set( other.type(),
               other.position(),
               other.direction(),
               other.color(),
               other.intensityLevel(),
               other.cutoffAngle(),
               other.coneConcentrationExponent(),
               other.constantAttenuation(),
               other.linearAttenuation(),
               other.quadraticAttenuation() );
}

void Light::set(
        const Type       aType,
        const Vector3Df& aPosition,
        const Vector3Df& aDirection,
        const QColor&    aColor,
        const float      aIntensityLevel,
        const float      aCutoffAngle,
        const float      aConeConcentrationExponent,
        const float      aConstantAttenuation,
        const float      aLinearAttenuation,
        const float      aQuadraticAttenuation )
{
    this->setType( aType );
    this->setPosition( aPosition );
    this->setDirection( aDirection );
    this->setColor( aColor );
    this->setIntensityLevel( aIntensityLevel );
    this->setCutoffAngle( aCutoffAngle );
    this->setConeConcentrationExponent( aConeConcentrationExponent );
    this->setConstantAttenuation( aConstantAttenuation );
    this->setLinearAttenuation( aLinearAttenuation );
    this->setQuadraticAttenuation( aQuadraticAttenuation );
}

void Light::setType( const Light::Type aType )
{
    if ( ! Light::isTypeValid( aType )
         || aType == m_type )
    {
        return;
    }

    m_type = aType;
    this->changed( TraitType );
}

bool Light::isTypeValid( const Light::Type aType )
{
    return    aType == TypePoint
           || aType == TypeDirectional
           || aType == TypeSpot;
}

QString Light::typeDescription( const Light::Type aType )
{
    QString description;
    switch ( aType )
    {
        case TypePoint:       description = "Point"; break;
        case TypeDirectional: description = "Directional"; break;
        case TypeSpot:        description = "Spot"; break;
    }

    return description.isEmpty()
           ? description
           : description + QString( " (%1)" ).arg( static_cast< int >( aType ) );
}

void Light::setPosition( const Vector3Df& aPosition )
{
    if ( aPosition == m_position )
    {
        return;
    }

    m_position = aPosition;
    this->changed( TraitPosition );
}

void Light::setDirection( const Vector3Df& aDirection )
{
    if ( ! Light::isDirectionValid( aDirection )
         || aDirection == m_direction )
    {
        return;
    }

    m_direction = aDirection;
    this->changed( TraitDirection );
}

bool Light::isDirectionValid( const Vector3Df& aDirection )
{
    return aDirection.isUnit();
}

void Light::setColor( const QColor& aColor )
{
    if ( ! aColor.isValid()
         || aColor == m_color )
    {
        return;
    }

    m_color = aColor;
    this->changed( TraitColor );
}

void Light::setIntensityLevel( const float aIntensityLevel )
{
    if ( ! Light::isIntensityLevelValid( aIntensityLevel )
         || MathUtilities::isFuzzyEqual( aIntensityLevel, m_intensityLevel ) )
    {
        return;
    }

    m_intensityLevel = aIntensityLevel;
    this->changed( TraitIntensityLevel );
}

bool Light::isIntensityLevelValid( const float aIntensityLevel )
{
    return MathUtilities::isFuzzyContainedInRegion( s_intensityLevelMin, aIntensityLevel, s_intensityLevelMax );
}

void Light::setCutoffAngle( const float aCutoffAngle )
{
    if ( ! Light::isCutoffAngleValid( aCutoffAngle )
         || MathUtilities::isFuzzyEqual( aCutoffAngle, m_cutoffAngle ) )
    {
        return;
    }

    m_cutoffAngle = aCutoffAngle;
    this->changed( TraitCutoffAngle );
}

bool Light::isCutoffAngleValid( const float aCutoffAngle )
{
    return MathUtilities::isFuzzyContainedInRegion( s_cutoffAngleMin, aCutoffAngle, s_cutoffAngleMax );
}

void Light::setConeConcentrationExponent( const float aConeConcentrationExponent )
{
    if ( ! Light::isConeConcentrationExponentValid( aConeConcentrationExponent )
         || MathUtilities::isFuzzyEqual( aConeConcentrationExponent, m_coneConcentrationExponent ) )
    {
        return;
    }

    m_coneConcentrationExponent = aConeConcentrationExponent;
    this->changed( TraitConeConcentrationExponent );
}

bool Light::isConeConcentrationExponentValid( const float aExponent )
{
    return MathUtilities::isFuzzyContainedInRegion( s_coneConcentrationExponentMin, aExponent, s_coneConcentrationExponentMax );
}

void Light::setConstantAttenuation( const float aConstantAttenuation )
{
    if ( ! Light::isAttenuationValid( aConstantAttenuation )
         || MathUtilities::isFuzzyEqual( aConstantAttenuation, m_constantAttenuation ) )
    {
        return;
    }

    m_constantAttenuation = aConstantAttenuation;
    this->changed( TraitConstantAttenuation );
}

void Light::setLinearAttenuation( const float aLinearAttenuation )
{
    if ( ! Light::isAttenuationValid( aLinearAttenuation )
         || MathUtilities::isFuzzyEqual( aLinearAttenuation, m_linearAttenuation ) )
    {
        return;
    }

    m_linearAttenuation = aLinearAttenuation;
    this->changed( TraitLinearAttenuation );
}

void Light::setQuadraticAttenuation( const float aQuadraticAttenuation )
{
    if ( ! Light::isAttenuationValid( aQuadraticAttenuation )
         || MathUtilities::isFuzzyEqual( aQuadraticAttenuation, m_quadraticAttenuation ) )
    {
        return;
    }

    m_quadraticAttenuation = aQuadraticAttenuation;
    this->changed( TraitQuadraticAttenuation );
}

bool Light::isAttenuationValid( const float aAttenuation )
{
    return MathUtilities::isFuzzyContainedInRegion( s_attenuationMin, aAttenuation, s_attenuationMax );
}

quint64 Light::differingTraits( const Light& other ) const
{
    if ( this == & other )
    {
        return s_noneTrait;
    }

    quint64 traits = s_noneTrait;
    if ( m_type != other.m_type )
    {
        traits |= TraitType;
    }

    if ( m_position != other.m_position )
    {
        traits |= TraitPosition;
    }

    if ( m_direction != other.m_direction )
    {
        traits |= TraitDirection;
    }

    if ( m_color != other.m_color )
    {
        traits |= TraitColor;
    }

    if ( ! MathUtilities::isFuzzyEqual( m_intensityLevel, other.m_intensityLevel ) )
    {
        traits |= TraitIntensityLevel;
    }

    if ( ! MathUtilities::isFuzzyEqual( m_cutoffAngle, other.m_cutoffAngle ) )
    {
        traits |= TraitCutoffAngle;
    }

    if ( ! MathUtilities::isFuzzyEqual( m_coneConcentrationExponent, other.m_coneConcentrationExponent ) )
    {
        traits |= TraitConeConcentrationExponent;
    }

    if ( ! MathUtilities::isFuzzyEqual( m_constantAttenuation, other.m_constantAttenuation ) )
    {
        traits |= TraitConstantAttenuation;
    }

    if ( ! MathUtilities::isFuzzyEqual( m_linearAttenuation, other.m_linearAttenuation ) )
    {
        traits |= TraitLinearAttenuation;
    }

    if ( ! MathUtilities::isFuzzyEqual( m_quadraticAttenuation, other.m_quadraticAttenuation ) )
    {
        traits |= TraitQuadraticAttenuation;
    }

    return traits;
}

bool Light::isValid() const
{
    if ( ! Light::isTypeValid( m_type )
         || ! m_color.isValid()
         || ! Light::isIntensityLevelValid( m_intensityLevel )
         || ! Light::isAttenuationValid( m_constantAttenuation )
         || ! Light::isAttenuationValid( m_linearAttenuation )
         || ! Light::isAttenuationValid( m_quadraticAttenuation ) )
    {
        return false;
    }

    switch ( m_type )
    {
        case TypeSpot:
            if ( ! Light::isCutoffAngleValid( m_cutoffAngle )
                 || ! Light::isConeConcentrationExponentValid( m_coneConcentrationExponent ) )
            {
                return false;
            }

            // don't break
        case TypeDirectional:
            if ( ! Light::isDirectionValid( m_direction ) )
            {
                return false;
            }

            break;

        default:
            break;
    }

    return true;
}

quint64 Light::allTraits() const
{
    return TraitsAll;
}

const Light& Light::defaultLight()
{
    static const Light light(
                s_typeDefault,
                s_positionDefault,
                s_directionDefault,
                s_colorDefault,
                s_intensityLevelDefault,
                s_cutoffAngleDefault,
                s_coneConcentrationExponentDefault,
                s_attenuationDefault,
                s_attenuationDefault,
                s_attenuationDefault );

    return light;
}

bool Light::operator==( const Light& other ) const
{
    return this == & other
           || (    m_type      == other.m_type
                && m_position  == other.m_position
                && m_direction == other.m_direction
                && m_color     == other.m_color
                && MathUtilities::isFuzzyEqual( m_intensityLevel,            other.m_intensityLevel )
                && MathUtilities::isFuzzyEqual( m_cutoffAngle,               other.m_cutoffAngle )
                && MathUtilities::isFuzzyEqual( m_coneConcentrationExponent, other.m_coneConcentrationExponent )
                && MathUtilities::isFuzzyEqual( m_constantAttenuation,       other.m_constantAttenuation  )
                && MathUtilities::isFuzzyEqual( m_linearAttenuation,         other.m_linearAttenuation    )
                && MathUtilities::isFuzzyEqual( m_quadraticAttenuation,      other.m_quadraticAttenuation ) );
}

bool Light::operator!=( const Light& other ) const
{
    return ! ( *this == other );
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const Light& light )
{
    return d << STRINGIFY( Light ) << '{' << endl
             << "\ttype                        =" << Light::typeDescription( light.type() ) << endl
             << "\tposition                    =" << light.position() << endl
             << "\tdirection                   =" << light.direction() << endl
             << "\tcolor                       =" << light.color() << endl
             << "\tintensity level             =" << light.intensityLevel() << endl
             << "\tcutoff angle                =" << light.cutoffAngle() << endl
             << "\tcone concentration exponent =" << light.coneConcentrationExponent() << endl
             << "\tconstant attenuation        =" << light.constantAttenuation() << endl
             << "\tlinear attenuation          =" << light.linearAttenuation() << endl
             << "\tquadratic attenuation       =" << light.quadraticAttenuation() << endl
             << '}';
}
#endif
