#ifndef SPHEREFLAKE_DATA_TRIANGLE2D_H
#define SPHEREFLAKE_DATA_TRIANGLE2D_H

#include "Vector2D.h"
#include "Polygon2D.h"

//         2
//        / \
//       /   \
//      /     \
//     /       \
//    /         \
//   /           \
//  0 ----------- 1

namespace Sphereflake {
namespace Data {

class Triangle2D : public Polygon2D
{
public:
    static const int s_verticesCount;

public:
    explicit Triangle2D(
            const Vector2Dd& aV0,
            const Vector2Dd& aV1,
            const Vector2Dd& aV2 );
    Triangle2D();
    ~Triangle2D() /* override */;

    void setVertices( const Vector2Dd& aV0,
                      const Vector2Dd& aV1,
                      const Vector2Dd& aV2 );

    void setV0( const Vector2Dd& aV0 );
    const Vector2Dd& v0() const;

    void setV1( const Vector2Dd& aV1 );
    const Vector2Dd& v1() const;

    void setV2( const Vector2Dd& aV2 );
    const Vector2Dd& v2() const;

    Vector2Dd vec10() const;
    Vector2Dd vec20() const;
    Vector2Dd vec21() const;

    LineSegment2D edge10() const;
    LineSegment2D edge20() const;
    LineSegment2D edge21() const;

    bool contains( const Vector2Dd& pt ) const;

    bool isValid() const /* override */;
    double area() const /* override */;

    double sign() const;
    void setWindingOrder( const bool aIsWindingOrderClockwise );
    bool isWindingOrderClockwise() const;
    void toggleIsWindingOrderClockwise();
    Triangle2D toggledWindingOrder() const;
    Triangle2D havingWindingOrder( const bool aIsWindingOrderClockwise ) const;

private:
    static double sign(
            const Vector2Dd& v0,
            const Vector2Dd& v1,
            const Vector2Dd& v2 );

    using Polygon2D::isConvex;
    using Polygon2D::isConcave;
    using Polygon2D::isSelfIntersecting;
    using Polygon2D::toTriangles;
    using Polygon2D::setVertices;

    using Polygon2D::append;
    using Polygon2D::prepend;
    using Polygon2D::insert;
    using Polygon2D::replace;
    using Polygon2D::remove;
    using Polygon2D::fillVertices;
};

}
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug qdebug, const Sphereflake::Data::Triangle2D& t );
#endif

#endif // SPHEREFLAKE_DATA_TRIANGLE2D_H
