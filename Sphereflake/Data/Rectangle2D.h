#ifndef SPHEREFLAKE_DATA_RECTANGLE_H
#define SPHEREFLAKE_DATA_RECTANGLE_H

#include "LineSegment2D.h"
#include "Size.h"
#include "Vector2D.h"

#include <QDebug>
#include <QList>
#include <QRect>
#include <QRectF>
#include <QVector>

/*
 *    y
 *    ^
 *    |                          top-right
 *    |             ____________
 *    |             |           |
 *    |             |           |
 *    |             |___________|
 *    | bottom-left
 *    |
 *    |____________________________> x
 *
 */

namespace Sphereflake {
namespace Data {

class Polygon2D;
class Triangle2D;

class Rectangle2D
{
public:
    Rectangle2D(
            const Vector2Dd& aBottomLeft,
            const Size& aSize );
    Rectangle2D(
            const Vector2Dd& aBottomLeft,
            const Vector2Dd& aTopRight );
    Rectangle2D(
            const double& aLeft, const double& aBottom,
            const double& aRight, const double& aTop );
    explicit Rectangle2D( const QRectF& other );
    explicit Rectangle2D( const QRect& other );
    explicit Rectangle2D( const Size& aSize );
    Rectangle2D();

    void setBottomLeft( const Vector2Dd& aBottomLeft );
    inline const Vector2Dd& bottomLeft() const
    {
        return m_bottomLeft;
    }

    void setX( const double& aX );
    inline double x() const
    {
        return m_bottomLeft.x();
    }

    void setY( const double& aY );
    inline double y() const
    {
        return m_bottomLeft.y();
    }

    void setSize( const Size& aSize );
    inline const Size& size() const
    {
        return m_size;
    }

    void setWidth( const double& aWidth );
    inline double width() const
    {
        return m_size.width();
    }

    void setHeight( const double& aHeight );
    inline double height() const
    {
        return m_size.height();
    }

    void set( const Vector2Dd& aBottomLeft,
              const Size& aSize );
    void set( const Vector2Dd& aBottomLeft,
              const Vector2Dd& aTopRight );

    Vector2Dd bottomRight() const;
    Vector2Dd topLeft() const;
    Vector2Dd topRight() const;
    Vector2Dd center() const;
    QVector< Vector2Dd > vertices() const;

    void translate( const Vector2Dd& distance );
    void moveCenter( const Vector2Dd& newPos );
    void moveBottomLeft( const Vector2Dd& newPos );
    void moveBottomRight( const Vector2Dd& newPos );
    void moveTopRight( const Vector2Dd& newPos );
    void moveTopLeft( const Vector2Dd& newPos );

    Rectangle2D translated( const Vector2Dd& distance ) const;
    Rectangle2D movedCenter( const Vector2Dd& newPos ) const;
    Rectangle2D movedBottomLeft( const Vector2Dd& newPos ) const;
    Rectangle2D movedBottomRight( const Vector2Dd& newPos ) const;
    Rectangle2D movedTopRight( const Vector2Dd& newPos ) const;
    Rectangle2D movedTopLeft( const Vector2Dd& newPos ) const;

    void expand( const double& leftMargin,
                 const double& bottomMargin,
                 const double& rightMargin,
                 const double& topMargin );
    void expand( const double& margin );
    Rectangle2D expanded( const double& leftMargin,
                          const double& bottomMargin,
                          const double& rightMargin,
                          const double& topMargin ) const;
    Rectangle2D expanded( const double& margin ) const;

    double top() const;
    double left() const;
    double bottom() const;
    double right() const;

    LineSegment2D topEdge() const;
    LineSegment2D leftEdge() const;
    LineSegment2D bottomEdge() const;
    LineSegment2D rightEdge() const;
    QList< LineSegment2D > edges() const;

    bool isEmpty() const;
    bool isValid() const;
    double area() const;

    bool contains( const Vector2Dd& pt, const bool accountBorders = true ) const;
    bool contains( const Rectangle2D& rect, const bool accountBorders = true ) const;
    bool intersects( const Rectangle2D& other,
                     const bool accountBorders = true,
                     Rectangle2D* const intersectionRect = nullptr ) const;

    Vector2Dd boundedPt( const Vector2Dd& pt, const bool allowBorders = true ) const;
    bool isContainedInInscribedCenteredCircle( const Vector2Dd& pt, const bool accountBorders = true ) const;

    bool operator==( const Rectangle2D& other ) const;
    bool operator!=( const Rectangle2D& other ) const;

    Polygon2D* toPolygon2D() const;
    QList< Triangle2D* > toTriangles() const;

    QRectF toQRectF() const;
    operator QRectF() const;

    QRect toQRect() const;
    operator QRect() const;

private:
    Vector2Dd m_bottomLeft;
    Size      m_size;
};

}
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const Sphereflake::Data::Rectangle2D& rect );
#endif

#endif // SPHEREFLAKE_DATA_RECTANGLE_H
