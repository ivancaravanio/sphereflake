﻿#ifndef SPHEREFLAKE_DATA_NUMBERRANGE_H
#define SPHEREFLAKE_DATA_NUMBERRANGE_H

#ifdef min
    #undef min
#endif

#ifdef max
    #undef max
#endif

namespace Sphereflake {
namespace Data {

class NumberRange
{
public:
    static const int s_minDefault;
    static const int s_maxDefault;

public:
    NumberRange();
    NumberRange( const int aMin, const int aMax );

    bool isUnit() const;

    static bool isValid( const int aMin, const int aMax );
    bool isValid() const;

    bool set( const int aMin, const int aMax );
    bool setMin( const int aMin );
    inline int min() const
    {
        return m_min;
    }

    bool setMax( const int aMax );
    inline int max() const
    {
        return m_max;
    }

    bool contains( const int value ) const;
    int boundedValue( const int value ) const;
    int cyclicValue( const int value ) const;
    int span( int number ) const;
    int span() const;
    double normalizedSpan( const int number ) const;

    bool operator==( const NumberRange& other ) const;
    bool operator!=( const NumberRange& other ) const;

private:
    int m_min;
    int m_max;
};

}
}

#endif // SPHEREFLAKE_DATA_NUMBERRANGE_H
