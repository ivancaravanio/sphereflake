#include "ShaderInfo.h"

#include "../Sphereflake_namespace.h"
#include "../Utilities/OpenGl/OpenGlUtilities.h"

using namespace Sphereflake::Data;

using namespace Sphereflake::Utilities::OpenGl;

ShaderInfo::ShaderInfo()
    : m_type( ShaderTypeInvalid )
{
}

ShaderInfo::ShaderInfo(
        const ShaderInfo::ShaderType aType,
        const QByteArray& aSourceCode )
    : sourceCode( aSourceCode )
    , m_type( ShaderTypeInvalid )

{
    this->setType( aType );
}

ShaderInfo::~ShaderInfo()
{
}

void ShaderInfo::setType( const ShaderInfo::ShaderType aType )
{
    if ( ! ShaderInfo::isShaderTypeValid( aType ) )
    {
        return;
    }

    m_type = aType;
}

ShaderInfo::ShaderType ShaderInfo::type() const
{
    return m_type;
}

bool ShaderInfo::isShaderTypeValid( const ShaderInfo::ShaderType shaderType )
{
    return    shaderType == ShaderTypeVertex
           || shaderType == ShaderTypeTessellationControl
           || shaderType == ShaderTypeTessellationEvaluation
           || shaderType == ShaderTypeGeometry
           || shaderType == ShaderTypeFragment
           || shaderType == ShaderTypeCompute;
}

GLuint ShaderInfo::openGlShaderTypeFromShaderType( const ShaderType shaderType )
{
    switch ( shaderType )
    {
        case ShaderTypeVertex:                 return GL_VERTEX_SHADER;
        case ShaderTypeTessellationControl:    return GL_TESS_CONTROL_SHADER;
        case ShaderTypeTessellationEvaluation: return GL_TESS_EVALUATION_SHADER;
        case ShaderTypeGeometry:               return GL_GEOMETRY_SHADER;
        case ShaderTypeFragment:               return GL_FRAGMENT_SHADER;
        case ShaderTypeCompute:                return GL_COMPUTE_SHADER;
        default:                               break;
    }

    return OpenGlUtilities::s_invalidUnsignedGlValue;
}

QString ShaderInfo::shaderTypeDescription( const ShaderInfo::ShaderType shaderType )
{
    switch ( shaderType )
    {
        case ShaderTypeVertex:                 return "vertex";
        case ShaderTypeTessellationControl:    return "tessellation control";
        case ShaderTypeTessellationEvaluation: return "tessellation evaluation";
        case ShaderTypeGeometry:               return "geometry";
        case ShaderTypeFragment:               return "fragment";
        case ShaderTypeCompute:                return "compute";
        default:                               break;
    }

    return QString();
}

GLuint ShaderInfo::openGlShaderType() const
{
    return ShaderInfo::openGlShaderTypeFromShaderType( m_type );
}

QDebug operator<<( QDebug d, const ShaderInfo& shaderInfo )
{
    d << STRINGIFY( ShaderInfo ) << endl
      << '{' << endl
      << "\ttype:" << ShaderInfo::shaderTypeDescription( shaderInfo.type() ) << endl
      << "\tsource:" << endl
      << shaderInfo.sourceCode << endl
      << '}';

    return d;
}
