#ifndef SPHEREFLAKE_DATA_LIGHT_H
#define SPHEREFLAKE_DATA_LIGHT_H

#include "../Utilities/ChangeNotifier.h"
#include "Vector3D.h"

#include <QColor>
#include <QString>
#include <QtGlobal>

namespace Sphereflake {
namespace Data {

using namespace Sphereflake::Utilities;

class Light : public ChangeNotifier
{
public:
    enum Type
    {
        TypeInvalid,
        TypePoint,
        TypeDirectional,
        TypeSpot
    };

    enum Trait : quint64
    {
        TraitType                      = 0x1 << 0,
        TraitPosition                  = 0x1 << 1,
        TraitDirection                 = 0x1 << 2,
        TraitColor                     = 0x1 << 3,
        TraitIntensityLevel            = 0x1 << 4,
        TraitCutoffAngle               = 0x1 << 5,
        TraitConeConcentrationExponent = 0x1 << 6,
        TraitConstantAttenuation       = 0x1 << 7,
        TraitLinearAttenuation         = 0x1 << 8,
        TraitQuadraticAttenuation      = 0x1 << 9,

        TraitsAll                      = TraitType
                                         | TraitPosition
                                         | TraitDirection
                                         | TraitColor
                                         | TraitIntensityLevel
                                         | TraitCutoffAngle
                                         | TraitConeConcentrationExponent
                                         | TraitConstantAttenuation
                                         | TraitLinearAttenuation
                                         | TraitQuadraticAttenuation
    };

public:
    static const Type s_typeDefault;

    static const Vector3Df s_positionDefault;
    static const Vector3Df s_directionDefault;
    static const QColor s_colorDefault;

    static const float s_intensityLevelMin;
    static const float s_intensityLevelDefault;
    static const float s_intensityLevelMax;
    static const float s_intensityLevelInvalid;

    static const float s_cutoffAngleMin;
    static const float s_cutoffAngleDefault;
    static const float s_cutoffAngleMax;
    static const float s_cutoffAngleInvalid;

    static const float s_coneConcentrationExponentMin;
    static const float s_coneConcentrationExponentDefault;
    static const float s_coneConcentrationExponentMax;
    static const float s_coneConcentrationExponentInvalid;

    static const float s_attenuationMin;
    static const float s_attenuationDefault;
    static const float s_attenuationMax;
    static const float s_attenuationInvalid;

public:
    Light();
    explicit Light( const Type       aType,
                    const Vector3Df& aPosition,
                    const Vector3Df& aDirection,
                    const QColor&    aColor,
                    const float      aIntensityLevel,
                    const float      aCutoffAngle,
                    const float      aConeConcentrationExponent,
                    const float      aConstantAttenuation,
                    const float      aLinearAttenuation,
                    const float      aQuadraticAttenuation );

    // in contrast to operator= this setter raises
    // notifications about eventual traits' changes
    void set( const Light& other );
    void set( const Type       aType,
              const Vector3Df& aPosition,
              const Vector3Df& aDirection,
              const QColor&    aColor,
              const float      aIntensityLevel,
              const float      aCutoffAngle,
              const float      aConeConcentrationExponent,
              const float      aConstantAttenuation,
              const float      aLinearAttenuation,
              const float      aQuadraticAttenuation );

    void setType( const Type aType );
    inline Type type() const
    {
        return m_type;
    }

    static bool isTypeValid( const Type aType );
    static QString typeDescription( const Type aType );

    void setPosition( const Vector3Df& aPosition );
    inline const Vector3Df& position() const
    {
        return m_position;
    }

    void setDirection( const Vector3Df& aDirection );
    inline const Vector3Df& direction() const
    {
        return m_direction;
    }

    static bool isDirectionValid( const Vector3Df& aDirection );

    void setColor( const QColor& aColor );
    inline const QColor& color() const
    {
        return m_color;
    }

    void setIntensityLevel( const float aIntensityLevel );
    inline float intensityLevel() const
    {
        return m_intensityLevel;
    }

    static bool isIntensityLevelValid( const float aIntensityLevel );

    void setCutoffAngle( const float aCutoffAngle );
    inline float cutoffAngle() const
    {
        return m_cutoffAngle;
    }

    static bool isCutoffAngleValid( const float aCutoffAngle );

    void setConeConcentrationExponent( const float aConeConcentrationExponent );
    inline float coneConcentrationExponent() const
    {
        return m_coneConcentrationExponent;
    }

    static bool isConeConcentrationExponentValid( const float aExponent );

    void setConstantAttenuation( const float aConstantAttenuation );
    inline float constantAttenuation() const
    {
        return m_constantAttenuation;
    }

    void setLinearAttenuation( const float aLinearAttenuation );
    inline float linearAttenuation() const
    {
        return m_linearAttenuation;
    }

    void setQuadraticAttenuation( const float aQuadraticAttenuation );
    inline float quadraticAttenuation() const
    {
        return m_quadraticAttenuation;
    }

    static bool isAttenuationValid( const float aAttenuation );

    bool isValid() const;

    quint64 allTraits() const /* override */;
    quint64 differingTraits( const Light& other ) const;

    bool operator==( const Light& other ) const;
    bool operator!=( const Light& other ) const;

    static const Light& defaultLight();

private:
    Type      m_type;
    Vector3Df m_position;
    Vector3Df m_direction;
    QColor    m_color;
    float     m_intensityLevel;
    float     m_cutoffAngle;
    float     m_coneConcentrationExponent;
    float     m_constantAttenuation;
    float     m_linearAttenuation;
    float     m_quadraticAttenuation;
};

}
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const Sphereflake::Data::Light& light );
#endif

#endif // SPHEREFLAKE_DATA_LIGHT_H
