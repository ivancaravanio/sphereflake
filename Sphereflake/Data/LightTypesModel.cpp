#include "LightTypesModel.h"

using namespace Sphereflake::Data;

LightTypesModel& LightTypesModel::instance()
{
    static LightTypesModel model;
    return model;
}

int Sphereflake::Data::LightTypesModel::rowCount( const QModelIndex& parent ) const
{
    Q_UNUSED( parent )

    return m_allLightTypes.size();
}

QVariant LightTypesModel::data( const QModelIndex& index, int role ) const
{
    const int rowIndex = index.row();
    if ( ! this->isRowIndexValid( rowIndex )
         || ! this->isColumnIndexValid( index.column() )
         || ( role != Qt::DisplayRole
              && role != Qt::UserRole ) )
    {
        return QVariant();
    }

    const Light::Type lightType = m_allLightTypes[ rowIndex ];
    switch ( role )
    {
        case Qt::DisplayRole: return LightTypesModel::lightTypeDescription( lightType );
        case Qt::UserRole: return static_cast< int >( lightType );
        default: break;
    }

    return QVariant();
}

int LightTypesModel::rowIndex( const Light::Type lightType ) const
{
    return Light::isTypeValid( lightType )
           ? m_allLightTypes.indexOf( lightType )
           : Light::TypeInvalid;
}

void LightTypesModel::retranslate()
{
    const int rowsCount = this->rowCount();
    if ( rowsCount == 0 )
    {
        return;
    }

    emit dataChanged( this->index( 0 ),
                      this->index( rowsCount - 1 ) );
}

LightTypesModel::LightTypesModel( QObject* const parent )
    : QAbstractListModel( parent )
{
    m_allLightTypes.reserve( 3 );
    m_allLightTypes << Light::TypePoint
                    << Light::TypeDirectional
                    << Light::TypeSpot;
}

QString LightTypesModel::lightTypeDescription( const Light::Type lightType )
{
    switch ( lightType )
    {
        case Light::TypePoint:       return tr( "Point" );
        case Light::TypeDirectional: return tr( "Directional" );
        case Light::TypeSpot:        return tr( "Spot" );
    }

    return QString();
}

bool LightTypesModel::isRowIndexValid( const int rowIndex ) const
{
    return 0 <= rowIndex && rowIndex < this->rowCount();
}

bool LightTypesModel::isColumnIndexValid( const int columnIndex ) const
{
    return columnIndex == 0;
}
