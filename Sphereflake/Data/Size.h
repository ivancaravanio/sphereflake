#ifndef SPHEREFLAKE_DATA_SIZE_H
#define SPHEREFLAKE_DATA_SIZE_H

#include "Vector2D.h"

#include <QDebug>
#include <QSize>
#include <QSizeF>
#include <QtGlobal>

namespace Sphereflake {
namespace Data {

class Size
{
public:
    explicit Size( const double& aWidth, const double& aHeight );
    explicit Size( const int aWidth, const int aHeight );
    explicit Size( const QSize& aSize );
    explicit Size( const QSizeF& aSize );
    explicit Size( const Vector2Dd& aVector2Dd );
    Size();

    void setWidth( const double& aWidth );
    inline double width() const
    {
        return m_width;
    }

    void setHeight( const double& aHeight );
    inline double height() const
    {
        return m_height;
    }

    void set( const double& aWidth, const double& aHeight );

    bool isEmpty() const;
    bool isValid() const;
    void clear();

    double area() const;

    Size operator+( const Size& other ) const;
    Size& operator+=( const Size& other );
    Size operator-( const Size& other ) const;
    Size& operator-=( const Size& other );
    Size operator*( const double& factor ) const;
    Size& operator*=( const double& factor );
    Size operator/( const double& factor ) const;
    Size& operator/=( const double& factor );

    double inscriptionScale( const double& boundaryWidth, const double& boundaryHeight ) const;
    double inscriptionScale( const Size& boundarySize ) const;

    void inscribe( const double& boundaryWidth, const double& boundaryHeight );
    Size inscribed( const double& boundaryWidth, const double& boundaryHeight ) const;
    void inscribe( const Size& boundarySize );
    Size inscribed( const Size& boundarySize ) const;

    void expand( const double& horizontalMargin,
                 const double& verticalMargin );
    Size expanded( const double& horizontalMargin,
                   const double& verticalMargin ) const;

    void expand( const double& margin );
    Size expanded( const double& margin ) const;

    double minLength() const;
    double maxLength() const;
    Size minSize() const;
    Size maxSize() const;

    QSize toQSize() const;
    operator QSize() const;

    QSizeF toQSizeF() const;
    operator QSizeF() const;

    Vector2Dd toVector2D() const;

    bool operator==( const Size& other ) const;
    bool operator!=( const Size& other ) const;

private:
    double m_width;
    double m_height;
};

}
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const Sphereflake::Data::Size& s );
#endif

#endif // SPHEREFLAKE_DATA_SIZE_H
