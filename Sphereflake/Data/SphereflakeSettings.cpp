#include "SphereflakeSettings.h"

#include "../Sphereflake_namespace.h"
#include "../Utilities/MathUtilities.h"

#include <limits>

using namespace Sphereflake::Data;

using namespace Sphereflake::Utilities;

const quint64         SphereflakeSettings::s_levelsCountMin     = 1;
const quint64         SphereflakeSettings::s_levelsCountDefault = 2;
const quint64         SphereflakeSettings::s_levelsCountMax     = ULLONG_MAX;
const quint64         SphereflakeSettings::s_levelsCountInvalid = 0;

const float           SphereflakeSettings::s_initialSphereRadiusMin     = 0.001f;
const float           SphereflakeSettings::s_initialSphereRadiusDefault = 0.5f;
const float           SphereflakeSettings::s_initialSphereRadiusMax     = 1.0f;
const float           SphereflakeSettings::s_initialSphereRadiusInvalid = -FLT_MAX;

const float           SphereflakeSettings::s_childSphereRadiusFactorMin     = 0.001f;
const float           SphereflakeSettings::s_childSphereRadiusFactorDefault = 1.0f / 3.0f;
const float           SphereflakeSettings::s_childSphereRadiusFactorMax     = 10.0f;
const float           SphereflakeSettings::s_childSphereRadiusFactorInvalid = -FLT_MAX;

const float           SphereflakeSettings::s_childSphereOffsetFactorMin     = -1.0f;
const float           SphereflakeSettings::s_childSphereOffsetFactorDefault = 0.0f;
const float           SphereflakeSettings::s_childSphereOffsetFactorMax     = 10.0f;
const float           SphereflakeSettings::s_childSphereOffsetFactorInvalid = -FLT_MAX;

const int             SphereflakeSettings::s_spheresAlternatingColorsCountMin = 1;
const int             SphereflakeSettings::s_spheresAlternatingColorsCountMax = 3;
const QList< QColor > SphereflakeSettings::s_spheresAlternatingColorsDefault  = QList< QColor >()
                                                                                << Qt::red
                                                                                << Qt::green
                                                                                << Qt::blue;

const float           SphereflakeSettings::s_shininessMin     = 0.0f;
const float           SphereflakeSettings::s_shininessDefault = 128.0f;
const float           SphereflakeSettings::s_shininessMax     = 128.0f;
const float           SphereflakeSettings::s_shininessInvalid = -1.0f;

SphereflakeSettings::SphereflakeSettings()
    : m_levelsCount( s_levelsCountInvalid )
    , m_initialSphereRadius( s_initialSphereRadiusInvalid )
    , m_childSphereRadiusFactor( s_childSphereRadiusFactorInvalid )
    , m_childSphereOffsetFactor( s_childSphereOffsetFactorInvalid )
    , m_shininess( s_shininessInvalid )
{
}

SphereflakeSettings::SphereflakeSettings(
        const quint64&         aLevelsCount,
        const float            aInitialSphereRadius,
        const float            aChildSphereRadiusFactor,
        const float            aChildSphereOffsetFactor,
        const QList< QColor >& aSpheresAlternatingColors,
        const float            aShininess )
    : m_levelsCount( s_levelsCountInvalid )
    , m_initialSphereRadius( s_initialSphereRadiusInvalid )
    , m_childSphereRadiusFactor( s_childSphereRadiusFactorInvalid )
    , m_childSphereOffsetFactor( s_childSphereOffsetFactorInvalid )
    , m_shininess( s_shininessInvalid )
{
    this->set( aLevelsCount,
               aInitialSphereRadius,
               aChildSphereRadiusFactor,
               aChildSphereOffsetFactor,
               aSpheresAlternatingColors,
               aShininess );
}

void SphereflakeSettings::setLevelsCount( const quint64& aLevelsCount )
{
    if ( ! SphereflakeSettings::isLevelsCountValid( aLevelsCount )
         || aLevelsCount == m_levelsCount )
    {
        return;
    }

    m_levelsCount = aLevelsCount;
    this->changed( TraitLevelsCount );
}

bool SphereflakeSettings::isLevelsCountValid( const quint64& aLevelsCount )
{
    return s_levelsCountMin <= aLevelsCount && aLevelsCount <= s_levelsCountMax;
}

void SphereflakeSettings::setInitialSphereRadius( const float aInitialSphereRadius )
{
    if ( ! SphereflakeSettings::isInitialSphereRadiusValid( aInitialSphereRadius )
         || MathUtilities::isFuzzyEqual( aInitialSphereRadius, m_initialSphereRadius ) )
    {
        return;
    }

    m_initialSphereRadius = aInitialSphereRadius;
    this->changed( TraitInitialSphereRadius );
}

bool SphereflakeSettings::isInitialSphereRadiusValid( const float aInitialSphereRadius )
{
    return MathUtilities::isFuzzyContainedInRegion( s_initialSphereRadiusMin,
                                                    aInitialSphereRadius,
                                                    s_initialSphereRadiusMax );
}

void SphereflakeSettings::setChildSphereRadiusFactor( const float aChildSphereRadiusFactor )
{
    if ( ! SphereflakeSettings::isChildSphereRadiusFactorValid( aChildSphereRadiusFactor )
         || MathUtilities::isFuzzyEqual( aChildSphereRadiusFactor, m_childSphereRadiusFactor ) )
    {
        return;
    }

    m_childSphereRadiusFactor = aChildSphereRadiusFactor;
    this->changed( TraitChildSphereRadiusFactor );
}

bool SphereflakeSettings::isChildSphereRadiusFactorValid( const float aChildSphereRadiusFactor )
{
    return MathUtilities::isFuzzyContainedInRegion( s_childSphereRadiusFactorMin,
                                                    aChildSphereRadiusFactor,
                                                    s_childSphereRadiusFactorMax );
}

void SphereflakeSettings::setChildSphereOffsetFactor( const float aChildSphereOffsetFactor )
{
    if ( ! SphereflakeSettings::isChildSphereOffsetFactorValid( aChildSphereOffsetFactor )
         || MathUtilities::isFuzzyEqual( aChildSphereOffsetFactor, m_childSphereOffsetFactor ) )
    {
        return;
    }

    m_childSphereOffsetFactor = aChildSphereOffsetFactor;
    this->changed( TraitChildSphereOffsetFactor );
}

bool SphereflakeSettings::isChildSphereOffsetFactorValid( const float aChildSphereOffsetFactor )
{
    return MathUtilities::isFuzzyContainedInRegion( s_childSphereOffsetFactorMin,
                                                    aChildSphereOffsetFactor,
                                                    s_childSphereOffsetFactorMax );
}

void SphereflakeSettings::setSpheresAlternatingColors( const QList< QColor >& aSpheresAlternatingColors )
{
    if ( ! SphereflakeSettings::areSpheresAlternatingColorsValid( aSpheresAlternatingColors )
         || aSpheresAlternatingColors == m_spheresAlternatingColors )
    {
        return;
    }

    m_spheresAlternatingColors = aSpheresAlternatingColors;
    this->changed( TraitSpheresAlternatingColors );
}

bool SphereflakeSettings::isSpheresAlternatingColorsCountValid( const int aSpheresAlternatingColorsCountValid )
{
    return s_spheresAlternatingColorsCountMin <= aSpheresAlternatingColorsCountValid
           && aSpheresAlternatingColorsCountValid <= s_spheresAlternatingColorsCountMax;
}

bool SphereflakeSettings::areSpheresAlternatingColorsValid( const QList< QColor >& aSpheresAlternatingColors )
{
    const int spheresAlternatingColorsCount = aSpheresAlternatingColors.size();
    if ( ! SphereflakeSettings::isSpheresAlternatingColorsCountValid( spheresAlternatingColorsCount ) )
    {
        return false;
    }

    for ( int i = 0; i < spheresAlternatingColorsCount; ++ i )
    {
        for ( int j = i + 1; j < spheresAlternatingColorsCount; ++ j )
        {
            const QColor& colorLeft = aSpheresAlternatingColors[ i ];
            const QColor& colorRight = aSpheresAlternatingColors[ j ];
            if ( ! colorLeft.isValid()
                 || ! colorRight.isValid()
                 || colorLeft == colorRight )
            {
                return false;
            }
        }
    }

    return true;
}

void SphereflakeSettings::setShininess( const float aShininess )
{
    if ( ! SphereflakeSettings::isShininessValid( aShininess )
         || MathUtilities::isFuzzyEqual( aShininess, m_shininess ) )
    {
        return;
    }

    m_shininess = aShininess;
    this->changed( TraitShininess );
}

bool SphereflakeSettings::isShininessValid( const float aShininess )
{
    return MathUtilities::isFuzzyContainedInRegion( s_shininessMin, aShininess, s_shininessMax );
}

void SphereflakeSettings::set( const SphereflakeSettings& other )
{
    this->set( other.levelsCount(),
               other.initialSphereRadius(),
               other.childSphereRadiusFactor(),
               other.childSphereOffsetFactor(),
               other.spheresAlternatingColors(),
               other.shininess() );
}

void SphereflakeSettings::set(
        const quint64&         aLevelsCount,
        const float            aInitialSphereRadius,
        const float            aChildSphereRadiusFactor,
        const float            aChildSphereOffsetFactor,
        const QList< QColor >& aSpheresAlternatingColors,
        const float            aShininess )
{
    this->setLevelsCount( aLevelsCount );
    this->setInitialSphereRadius( aInitialSphereRadius );
    this->setChildSphereRadiusFactor( aChildSphereRadiusFactor );
    this->setChildSphereOffsetFactor( aChildSphereOffsetFactor );
    this->setSpheresAlternatingColors( aSpheresAlternatingColors );
    this->setShininess( aShininess );
}

bool SphereflakeSettings::operator==( const SphereflakeSettings& other ) const
{
    return    m_initialSphereRadius == other.m_levelsCount
           && MathUtilities::isFuzzyEqual( m_initialSphereRadius,     other.m_initialSphereRadius )
           && MathUtilities::isFuzzyEqual( m_childSphereRadiusFactor, other.m_childSphereRadiusFactor )
           && MathUtilities::isFuzzyEqual( m_childSphereOffsetFactor, other.m_childSphereOffsetFactor )
           && m_spheresAlternatingColors == other.m_spheresAlternatingColors
           && MathUtilities::isFuzzyEqual( m_shininess,               other.m_shininess );
}

bool SphereflakeSettings::operator!=( const SphereflakeSettings& other ) const
{
    return ! ( *this == other );
}

quint64 SphereflakeSettings::allTraits() const
{
    return TraitsAll;
}

quint64 SphereflakeSettings::differingTraits( const SphereflakeSettings& other ) const
{
    if ( this == & other )
    {
        return s_noneTrait;
    }

    quint64 traits = s_noneTrait;
    if ( m_levelsCount != other.m_levelsCount )
    {
        traits |= TraitLevelsCount;
    }

    if ( ! MathUtilities::isFuzzyEqual( m_initialSphereRadius, other.m_initialSphereRadius ) )
    {
        traits |= TraitInitialSphereRadius;
    }

    if ( ! MathUtilities::isFuzzyEqual( m_childSphereRadiusFactor, other.m_childSphereRadiusFactor ) )
    {
        traits |= TraitChildSphereRadiusFactor;
    }

    if ( ! MathUtilities::isFuzzyEqual( m_childSphereOffsetFactor, other.m_childSphereOffsetFactor ) )
    {
        traits |= TraitChildSphereOffsetFactor;
    }

    if ( m_spheresAlternatingColors != other.m_spheresAlternatingColors )
    {
        traits |= TraitSpheresAlternatingColors;
    }

    if ( ! MathUtilities::isFuzzyEqual( m_shininess, other.m_shininess ) )
    {
        traits |= TraitShininess;
    }

    return traits;
}

bool SphereflakeSettings::isValid() const
{
    return SphereflakeSettings::isLevelsCountValid( m_levelsCount )
           && SphereflakeSettings::isInitialSphereRadiusValid( m_initialSphereRadius )
           && SphereflakeSettings::isChildSphereRadiusFactorValid( m_childSphereRadiusFactor )
           && SphereflakeSettings::isChildSphereOffsetFactorValid( m_childSphereOffsetFactor )
           && SphereflakeSettings::areSpheresAlternatingColorsValid( m_spheresAlternatingColors )
           && SphereflakeSettings::isShininessValid( m_shininess );
}

const SphereflakeSettings& SphereflakeSettings::defaultSphereflakeSettings()
{
    static const SphereflakeSettings settings(
                s_levelsCountDefault,
                s_initialSphereRadiusDefault,
                s_childSphereRadiusFactorDefault,
                s_childSphereOffsetFactorDefault,
                s_spheresAlternatingColorsDefault,
                s_shininessDefault );

    return settings;
}


#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const SphereflakeSettings& settings )
{
    return d << STRINGIFY( SphereflakeSettings ) << '{' << endl
             << "\tinitial sphere radius:     " << settings.initialSphereRadius() << endl
             << "\tchild sphere radius factor:" << settings.childSphereRadiusFactor() << endl
             << "\tchild sphere offset factor:" << settings.childSphereOffsetFactor() << endl
             << "\tspheres alternating colors:" << settings.spheresAlternatingColors() << endl
             << "\tshininess:                 " << settings.shininess() << endl
             << '}';
}
#endif
