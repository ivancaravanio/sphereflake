#include "Camera.h"

#include "../Sphereflake_namespace.h"
#include "../Utilities/MathUtilities.h"
#include "Vector2D.h"

#include <limits>

using namespace Sphereflake::Data;

using namespace Sphereflake::Utilities;

const Vector3Df Camera::s_positionDefault( 0.0, 0.0, 0.0 );
const Vector3Df Camera::s_directionDefault( -1.0, 0.0, 0.0 );

const float Camera::s_zoomFactorMin     = 1.0f;
const float Camera::s_zoomFactorDefault = 1.0f;
const float Camera::s_zoomFactorMax     = FLT_MAX;
const float Camera::s_zoomFactorInvalid = -1.0f;

Camera::Camera()
    : m_zoomFactor( s_zoomFactorInvalid )
{
}

Camera::Camera(
        const Vector3Df& aPosition,
        const Vector3Df& aDirection,
        const float      aZoomFactor )
    : m_zoomFactor( s_zoomFactorInvalid )
{
    this->set( aPosition,
               aDirection,
               aZoomFactor );
}

Camera::Camera( const QMatrix4x4& aMatrix )
    : m_zoomFactor( s_zoomFactorInvalid )
{
    const QVector3D coordSysBeginningPt = aMatrix.map( QVector3D( 0.0, 0.0, 0.0 ) );
    const QVector3D dir = aMatrix.map( QVector3D( 1.0, 0.0, 0.0 ) ) - coordSysBeginningPt;

    this->set( Vector3Df( coordSysBeginningPt ),
               Vector3Df( dir.normalized() ),
               static_cast< float >( dir.length() ) );
}

void Camera::setZoomFactor( const float aZoomFactor )
{
    if ( ! Camera::isZoomFactorValid( aZoomFactor )
         || MathUtilities::isFuzzyEqual( aZoomFactor, m_zoomFactor ) )
    {
        return;
    }

    m_zoomFactor = aZoomFactor;
    this->changed( TraitZoomFactor );
}

bool Camera::isZoomFactorValid( const float aZoomFactor )
{
    return MathUtilities::isFuzzyContainedInRegion( s_zoomFactorMin,
                                                    aZoomFactor,
                                                    s_zoomFactorMax );
}

QMatrix4x4 Camera::toMatrix() const
{
    QMatrix4x4 viewMatrix;
    viewMatrix.translate( m_position );

    viewMatrix.rotate( Vector2Df( - m_direction.z(), m_direction.y() ).angle(), MathUtilities::s_xDir );
    viewMatrix.rotate( Vector2Df( m_direction.x(), - m_direction.z() ).angle(), MathUtilities::s_yDir );
    viewMatrix.rotate( Vector2Df( m_direction.x(), m_direction.y() ).angle(), MathUtilities::s_zDir );

    viewMatrix.scale( m_zoomFactor );

    return viewMatrix;
}

void Camera::set( const Camera& other )
{
    this->set( other.position(),
               other.direction(),
               other.zoomFactor() );
}

void Camera::setPosition( const Vector3Df& aPosition )
{
    if ( aPosition == m_position )
    {
        return;
    }

    m_position = aPosition;
    this->changed( TraitPosition );
}

void Camera::setDirection( const Vector3Df& aDirection )
{
    if ( ! Camera::isDirectionValid( aDirection )
         || aDirection == m_direction )
    {
        return;
    }

    m_direction = aDirection;
    this->changed( TraitDirection );
}

bool Camera::isDirectionValid( const Vector3Df& aDirection )
{
    return aDirection.isUnit();
}

void Camera::set( const Vector3Df& aPosition,
                  const Vector3Df& aDirection,
                  const float      aZoomFactor )
{
    this->setPosition( aPosition );
    this->setDirection( aDirection );
    this->setZoomFactor( aZoomFactor );
}

bool Camera::isValid() const
{
    return    Camera::isDirectionValid( m_direction )
           && Camera::isZoomFactorValid( m_zoomFactor );
}

bool Camera::operator==( const Camera& other ) const
{
    return    m_position  == other.m_position
           && m_direction == other.m_direction
           && MathUtilities::isFuzzyEqual( m_zoomFactor, other.m_zoomFactor );
}

bool Camera::operator!=( const Camera& other ) const
{
    return ! ( *this == other );
}

quint64 Camera::allTraits() const
{
    return TraitsAll;
}

quint64 Camera::differingTraits( const Camera& other ) const
{
    if ( this == & other )
    {
        return s_noneTrait;
    }

    quint64 traits = s_noneTrait;
    if ( m_position != other.m_position )
    {
        traits |= TraitPosition;
    }

    if ( m_direction != other.m_direction )
    {
        traits |= TraitDirection;
    }

    if ( ! MathUtilities::isFuzzyEqual( m_zoomFactor, other.m_zoomFactor ) )
    {
        traits |= TraitZoomFactor;
    }

    return traits;
}

const Camera& Camera::defaultCamera()
{
    static const Camera camera(
                s_positionDefault,
                s_directionDefault,
                s_zoomFactorDefault );

    return camera;
}


#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const Camera& camera )
{
    return d << STRINGIFY( Camera ) << '{' << endl
             << "\tposition:   " << camera.position() << endl
             << "\tdirection:  " << camera.direction() << endl
             << "\tzoom factor:" << camera.zoomFactor() << endl
             << '}';
}
#endif
