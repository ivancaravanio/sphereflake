#ifndef SPHEREFLAKE_DATA_LIGHTTYPESMODEL_H
#define SPHEREFLAKE_DATA_LIGHTTYPESMODEL_H

#include "../Utilities/Retranslatable.h"
#include "Light.h"

#include <QAbstractListModel>
#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QComboBox)
QT_FORWARD_DECLARE_CLASS(QDoubleSpinBox)
QT_FORWARD_DECLARE_CLASS(QLabel)

namespace Sphereflake {
namespace Data {

using namespace Sphereflake::Utilities;

class LightTypesModel : public QAbstractListModel, public Retranslatable
{
    Q_OBJECT

public:
    static LightTypesModel& instance();

    int rowCount( const QModelIndex& parent = QModelIndex() ) const /* override */;
    QVariant data( const QModelIndex& index, int role = Qt::DisplayRole ) const /* override */;

    int rowIndex( const Light::Type lightType ) const;

    void retranslate() /* override */;

private:
    explicit LightTypesModel( QObject* const parent = nullptr );
    void retranslateSelfOnly();

    static QString lightTypeDescription( const Light::Type lightType );
    bool isRowIndexValid( const int rowIndex ) const;
    bool isColumnIndexValid( const int columnIndex ) const;

private:
    QList< Light::Type > m_allLightTypes;
};

}
}

#endif // SPHEREFLAKE_DATA_LIGHTTYPESMODEL_H
