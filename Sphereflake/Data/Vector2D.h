#ifndef SPHEREFLAKE_DATA_VECTOR2D_H
#define SPHEREFLAKE_DATA_VECTOR2D_H

#include "../Sphereflake_namespace.h"
#include "../Utilities/MathUtilities.h"

#include <QDebug>
#include <qmath.h>
#include <QPoint>
#include <QPointF>

#include <math.h>
#include <stdexcept>

namespace Sphereflake {
namespace Data {

using namespace Sphereflake::Utilities;

template < typename T >
class Vector2D
{
public:
    explicit Vector2D( const T& aX, const T& aY );
    explicit Vector2D( const QPoint& pt );
    explicit Vector2D( const QPointF& pt );
    Vector2D();

    static Vector2D fromAngleLength( const T& aAngle, const T& aLength );

    void setX( const T& aX );
    inline T x() const
    {
        return m_x;
    }

    void setY( const T& aY );
    inline T y() const
    {
        return m_y;
    }

    void set( const T& aX, const T& aY );
    void zero();

    T length() const;
    bool isUnit() const;
    T distanceTo( const Vector2D& other ) const;
    T dot( const Vector2D& other ) const;

    bool operator==( const Vector2D& other ) const;
    bool operator!=( const Vector2D& other ) const;

    bool operator<( const Vector2D& other ) const;
    bool operator<=( const Vector2D& other ) const;

    bool operator>( const Vector2D& other ) const;
    bool operator>=( const Vector2D& other ) const;

    Vector2D operator+( const Vector2D& other ) const;
    Vector2D& operator+=( const Vector2D& other );
    Vector2D operator-( const Vector2D& other ) const;
    Vector2D& operator-=( const Vector2D& other );
    Vector2D operator*( const T& factor ) const;
    Vector2D& operator*=( const T& factor );
    Vector2D operator/( const T& factor ) const;
    Vector2D& operator/=( const T& factor );
    T operator[]( const int axisIndex ) const;

    void normalize();
    Vector2D normalized() const;
    Vector2D normal() const;
    T angle() const;
    T angleTo( const Vector2D& other ) const;

    QPoint toQPoint() const;
    operator QPoint() const;

    QPointF toQPointF() const;
    operator QPointF() const;

private:
    T m_x;
    T m_y;
};

template < typename T >
Vector2D< T >::Vector2D( const T& aX, const T& aY )
    : m_x( aX )
    , m_y( aY )
{
}

template < typename T >
Vector2D< T >::Vector2D( const QPoint& pt )
    : m_x( pt.x() )
    , m_y( pt.y() )
{
}

template < typename T >
Vector2D< T >::Vector2D( const QPointF& pt )
    : m_x( pt.x() )
    , m_y( pt.y() )
{
}

template < typename T >
Vector2D< T >::Vector2D()
    : m_x( 0.0 )
    , m_y( 0.0 )
{
}

template < typename T >
Vector2D< T > Vector2D< T >::fromAngleLength( const T& aAngle, const T& aLength )
{
    return Vector2D( cos( MathUtilities::radians( aAngle ) ), sin( MathUtilities::radians( aAngle ) ) ) * aLength;
}

template < typename T >
void Vector2D< T >::setX( const T& aX )
{
    m_x = aX;
}

template < typename T >
void Vector2D< T >::setY( const T& aY )
{
    m_y = aY;
}

template < typename T >
void Vector2D< T >::set( const T& aX, const T& aY )
{
    this->setX( aX );
    this->setY( aY );
}

template < typename T >
void Vector2D< T >::zero()
{
    this->set( 0.0, 0.0 );
}

template < typename T >
T Vector2D< T >::length() const
{
    return sqrt( this->dot( *this ) );
}

template < typename T >
bool Vector2D< T >::isUnit() const
{
    return MathUtilities::isFuzzyEqual( this->length(), 1.0 );
}

template < typename T >
T Vector2D< T >::distanceTo( const Vector2D< T >& other ) const
{
    return Vector2D( other - *this ).length();
}

template < typename T >
T Vector2D< T >::dot( const Vector2D< T >& other ) const
{
    return m_x * other.m_x + m_y * other.m_y;
}

template < typename T >
bool Vector2D< T >::operator==( const Vector2D< T >& other ) const
{
    return MathUtilities::isFuzzyEqual( m_x, other.m_x )
           && MathUtilities::isFuzzyEqual( m_y, other.m_y );
}

template < typename T >
bool Vector2D< T >::operator!=( const Vector2D< T >& other ) const
{
    return ! ( *this == other );
}

template < typename T >
bool Vector2D< T >::operator<( const Vector2D< T >& other ) const
{
    return    MathUtilities::isFuzzyLessThan( m_x, other.m_x )
           && MathUtilities::isFuzzyLessThan( m_y, other.m_y );
}

template < typename T >
bool Vector2D< T >::operator<=( const Vector2D< T >& other ) const
{
    return    MathUtilities::isFuzzyLessThanOrEqual( m_x, other.m_x )
           && MathUtilities::isFuzzyLessThanOrEqual( m_y, other.m_y );
}

template < typename T >
bool Vector2D< T >::operator>( const Vector2D< T >& other ) const
{
    return    MathUtilities::isFuzzyGreaterThan( m_x, other.m_x )
           && MathUtilities::isFuzzyGreaterThan( m_y, other.m_y );
}

template < typename T >
bool Vector2D< T >::operator>=( const Vector2D< T >& other ) const
{
    return    MathUtilities::isFuzzyGreaterThanOrEqual( m_x, other.m_x )
           && MathUtilities::isFuzzyGreaterThanOrEqual( m_y, other.m_y );
}

template < typename T >
Vector2D< T > Vector2D< T >::operator+( const Vector2D< T >& other ) const
{
    Vector2D vec = *this;
    vec += other;

    return vec;
}

template < typename T >
Vector2D< T >& Vector2D< T >::operator+=( const Vector2D< T >& other )
{
    m_x += other.m_x;
    m_y += other.m_y;

    return *this;
}

template < typename T >
Vector2D< T > Vector2D< T >::operator-( const Vector2D< T >& other ) const
{
    Vector2D vec = *this;
    vec -= other;

    return vec;
}

template < typename T >
Vector2D< T >& Vector2D< T >::operator-=( const Vector2D< T >& other )
{
    m_x -= other.m_x;
    m_y -= other.m_y;

    return *this;
}

template < typename T >
Vector2D< T > Vector2D< T >::operator*( const T& factor ) const
{
    Vector2D vec = *this;
    vec *= factor;

    return vec;
}

template < typename T >
Vector2D< T >& Vector2D< T >::operator*=( const T& factor )
{
    m_x *= factor;
    m_y *= factor;

    return *this;
}

template < typename T >
Vector2D< T > Vector2D< T >::operator/( const T& factor ) const
{
    Vector2D vec = *this;
    vec /= factor;

    return vec;
}

template < typename T >
Vector2D< T >& Vector2D< T >::operator/=( const T& factor )
{
    if ( MathUtilities::isFuzzyZero( factor ) )
    {
        const T inf = std::numeric_limits< T >::infinity();
        m_x = MathUtilities::sign( m_x ) * inf;
        m_y = MathUtilities::sign( m_y ) * inf;
    }
    else
    {
        m_x /= factor;
        m_y /= factor;
    }

    return *this;
}

template < typename T >
T Vector2D< T >::operator[]( const int axisIndex ) const
{
    switch ( axisIndex )
    {
        case MathUtilities::AxisIndexX: return m_x;
        case MathUtilities::AxisIndexY: return m_y;
    }

    QT_THROW( std::out_of_range( QString( "index %1 is out of [%2, %3] bounds" )
                                 .arg( axisIndex )
                                 .arg( MathUtilities::AxisIndexX )
                                 .arg( MathUtilities::AxisIndexY ).toStdString() ) );

    return std::numeric_limits< T >::signaling_NaN();
}

template < typename T >
void Vector2D< T >::normalize()
{
    *this /= this->length();
}

template < typename T >
Vector2D< T > Vector2D< T >::normalized() const
{
    Vector2D vec = *this;
    vec.normalize();

    return vec;
}

template < typename T >
Vector2D< T > Vector2D< T >::normal() const
{
    return Vector2D( -m_y, m_x );
}

template < typename T >
T Vector2D< T >::angle() const
{
    return MathUtilities::toPositive360DegreesAngleFromRadians( atan2( m_y, m_x ) );
}

template < typename T >
T Vector2D< T >::angleTo( const Vector2D< T >& other ) const
{
    // angles diff: slower due to 2 atan2 operations, compared to sqrts + acos for the dot product one:
    // http://stackoverflow.com/questions/9316995/which-is-more-efficient-atan2-or-sqrt
    // return other.angle() - this->angle();

    // dot product implementation
    return MathUtilities::toPositive360DegreesAngleFromRadians( acos( this->normalized().dot( other.normalized() ) ) );
}

template < typename T >
QPoint Vector2D< T >::toQPoint() const
{
    return QPoint( static_cast< int >( m_x ),
                   static_cast< int >( m_y ) );
}

template < typename T >
Vector2D< T >::operator QPoint() const
{
    return this->toQPoint();
}

template < typename T >
QPointF Vector2D< T >::toQPointF() const
{
    return QPointF( m_x, m_y );
}

template < typename T >
Vector2D< T >::operator QPointF() const
{
    return this->toQPointF();
}

typedef Vector2D< float > Vector2Df;
typedef Vector2D< double > Vector2Dd;

}
}

#ifndef QT_NO_DEBUG_STREAM
template < typename T >
QDebug operator<<( QDebug d, const Sphereflake::Data::Vector2D< T >& vec )
{
    return d.nospace() << "Vector2D< " << STRINGIFY( T ) << " >( " << vec.x() << ", " << vec.y() << " )";
}
#endif

#endif // SPHEREFLAKE_DATA_VECTOR2D_H
