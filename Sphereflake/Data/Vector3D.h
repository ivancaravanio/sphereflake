#ifndef SPHEREFLAKE_DATA_VECTOR3D_H
#define SPHEREFLAKE_DATA_VECTOR3D_H

#include "../Sphereflake_namespace.h"
#include "../Utilities/MathUtilities.h"
#include "Vector2D.h"

#include <QDebug>
#include <qmath.h>
#include <QVector3D>

#include <math.h>
#include <stdexcept>

namespace Sphereflake {
namespace Data {

using namespace Sphereflake::Utilities;

template < typename T >
class Vector3D
{
public:
    explicit Vector3D( const T& aX, const T& aY, const T& aZ );
    explicit Vector3D( const T& aCoord );
    explicit Vector3D( const QVector3D& aVector );

    template < typename P >
    explicit Vector3D( const Vector2D< P >& vector2D, const T& aZ = 0.0 );
    Vector3D();

    void setX( const T& aX );
    inline T x() const
    {
        return m_x;
    }

    void setY( const T& aY );
    inline T y() const
    {
        return m_y;
    }

    void setZ( const T& aZ );
    inline T z() const
    {
        return m_z;
    }

    void set( const T& aX, const T& aY, const T& aZ );

    T length() const;
    bool isZero() const;
    bool isUnit() const;
    T distanceTo( const Vector3D& other ) const;
    T dot( const Vector3D& other ) const;
    Vector3D cross( const Vector3D& other ) const;

    bool operator==( const Vector3D& other ) const;
    bool operator!=( const Vector3D& other ) const;

    bool operator<( const Vector3D& other ) const;
    bool operator<=( const Vector3D& other ) const;

    bool operator>( const Vector3D& other ) const;
    bool operator>=( const Vector3D& other ) const;

    Vector3D operator+( const Vector3D& other ) const;
    Vector3D& operator+=( const Vector3D& other );
    Vector3D operator-( const Vector3D& other ) const;
    Vector3D& operator-=( const Vector3D& other );
    Vector3D operator*( const T& factor ) const;
    Vector3D& operator*=( const T& factor );
    Vector3D operator/( const T& factor ) const;
    Vector3D& operator/=( const T& factor );
    T operator[]( const int axisIndex ) const;

    void normalize();
    Vector3D normalized() const;
    Vector3D normal( const Vector3D& other ) const;
    T angle() const;
    T angleTo( const Vector3D& other ) const;

    template < typename P >
    Vector3D< P > toVector3D() const;

    template < typename P >
    operator Vector3D< P >() const;

    template < typename P >
    Vector2D< P > toVector2D() const;

    template < typename P >
    operator Vector2D< P >() const;

    QVector3D toQVector3D() const;
    operator QVector3D() const;

private:
    T m_x;
    T m_y;
    T m_z;
};

template < typename T >
Vector3D< T >::Vector3D( const T& aX, const T& aY, const T& aZ )
    : m_x( aX )
    , m_y( aY )
    , m_z( aZ )
{
}

template < typename T >
Vector3D< T >::Vector3D( const T& aCoord )
    : m_x( aCoord )
    , m_y( aCoord )
    , m_z( aCoord )
{
}

template < typename T >
Vector3D< T >::Vector3D( const QVector3D& aVector )
    : m_x( aVector.x() )
    , m_y( aVector.y() )
    , m_z( aVector.z() )
{
}

template < typename T >
template < typename P >
Vector3D< T >::Vector3D( const Vector2D< P >& vector2D, const T& aZ )
    : m_x( vector2D.x() )
    , m_y( vector2D.y() )
    , m_z( aZ )
{
}

template < typename T >
Vector3D< T >::Vector3D()
    : m_x( 0.0 )
    , m_y( 0.0 )
    , m_z( 0.0 )
{
}

template < typename T >
void Vector3D< T >::setX( const T& aX )
{
    m_x = aX;
}

template < typename T >
void Vector3D< T >::setY( const T& aY )
{
    m_y = aY;
}

template < typename T >
void Vector3D< T >::setZ( const T& aZ )
{
    m_z = aZ;
}

template < typename T >
void Vector3D< T >::set( const T& aX, const T& aY, const T& aZ )
{
    this->setX( aX );
    this->setY( aY );
    this->setZ( aZ );
}

template < typename T >
T Vector3D< T >::length() const
{
    const T lengthSquared = this->dot( *this );
    return MathUtilities::isFuzzyGreaterThan( lengthSquared, 0.0 )
           ? sqrt( lengthSquared )
           : 0.0;
}

template < typename T >
bool Vector3D< T >::isZero() const
{
    return MathUtilities::isFuzzyEqual< T >( this->length(), 0.0 );
}

template < typename T >
bool Vector3D< T >::isUnit() const
{
    return MathUtilities::isFuzzyEqual< T >( this->length(), 1.0 );
}

template < typename T >
T Vector3D< T >::distanceTo( const Vector3D< T >& other ) const
{
    return Vector3D( other - *this ).length();
}

template < typename T >
T Vector3D< T >::dot( const Vector3D< T >& other ) const
{
    return m_x * other.m_x + m_y * other.m_y + m_z * other.m_z;
}

template < typename T >
Vector3D< T > Vector3D< T >::cross( const Vector3D< T >& other ) const
{
    return Vector3D< T >( m_y * other.m_z - m_z * other.m_y,
                          m_z * other.m_x - m_x * other.m_z,
                          m_x * other.m_y - m_y * other.m_x );
}

template < typename T >
bool Vector3D< T >::operator==( const Vector3D< T >& other ) const
{
    return    MathUtilities::isFuzzyEqual( m_x, other.m_x )
           && MathUtilities::isFuzzyEqual( m_y, other.m_y )
           && MathUtilities::isFuzzyEqual( m_z, other.m_z );
}

template < typename T >
bool Vector3D< T >::operator!=( const Vector3D< T >& other ) const
{
    return ! ( *this == other );
}

template < typename T >
bool Vector3D< T >::operator<( const Vector3D< T >& other ) const
{
    return    MathUtilities::isFuzzyLessThan( m_x, other.m_x )
           && MathUtilities::isFuzzyLessThan( m_y, other.m_y )
           && MathUtilities::isFuzzyLessThan( m_z, other.m_z );
}

template < typename T >
bool Vector3D< T >::operator<=( const Vector3D< T >& other ) const
{
    return    MathUtilities::isFuzzyLessThanOrEqual( m_x, other.m_x )
           && MathUtilities::isFuzzyLessThanOrEqual( m_y, other.m_y )
           && MathUtilities::isFuzzyLessThanOrEqual( m_z, other.m_z );
}

template < typename T >
bool Vector3D< T >::operator>( const Vector3D< T >& other ) const
{
    return    MathUtilities::isFuzzyGreaterThan( m_x, other.m_x )
           && MathUtilities::isFuzzyGreaterThan( m_y, other.m_y )
           && MathUtilities::isFuzzyGreaterThan( m_z, other.m_z );
}

template < typename T >
bool Vector3D< T >::operator>=( const Vector3D< T >& other ) const
{
    return    MathUtilities::isFuzzyGreaterThanOrEqual( m_x, other.m_x )
           && MathUtilities::isFuzzyGreaterThanOrEqual( m_y, other.m_y )
           && MathUtilities::isFuzzyGreaterThanOrEqual( m_z, other.m_z );
}

template < typename T >
Vector3D< T > Vector3D< T >::operator+( const Vector3D< T >& other ) const
{
    Vector3D pt = *this;
    pt += other;

    return pt;
}

template < typename T >
Vector3D< T >& Vector3D< T >::operator+=( const Vector3D< T >& other )
{
    m_x += other.m_x;
    m_y += other.m_y;
    m_z += other.m_z;

    return *this;
}

template < typename T >
Vector3D< T > Vector3D< T >::operator-( const Vector3D< T >& other ) const
{
    Vector3D pt = *this;
    pt -= other;

    return pt;
}

template < typename T >
Vector3D< T >& Vector3D< T >::operator-=( const Vector3D< T >& other )
{
    m_x -= other.m_x;
    m_y -= other.m_y;
    m_z -= other.m_z;

    return *this;
}

template < typename T >
Vector3D< T > Vector3D< T >::operator*( const T& factor ) const
{
    Vector3D pt = *this;
    pt *= factor;

    return pt;
}

template < typename T >
Vector3D< T >& Vector3D< T >::operator*=( const T& factor )
{
    m_x *= factor;
    m_y *= factor;
    m_z *= factor;

    return *this;
}

template < typename T >
Vector3D< T > Vector3D< T >::operator/( const T& factor ) const
{
    Vector3D pt = *this;
    pt /= factor;

    return pt;
}

template < typename T >
Vector3D< T >& Vector3D< T >::operator/=( const T& factor )
{
    if ( MathUtilities::isFuzzyZero( factor ) )
    {
        const T inf = std::numeric_limits< T >::infinity();
        m_x = MathUtilities::sign( m_x ) * inf;
        m_y = MathUtilities::sign( m_y ) * inf;
        m_z = MathUtilities::sign( m_z ) * inf;
    }
    else
    {
        m_x /= factor;
        m_y /= factor;
        m_z /= factor;
    }

    return *this;
}

template < typename T >
T Vector3D< T >::operator[]( const int axisIndex ) const
{
    switch ( axisIndex )
    {
        case MathUtilities::AxisIndexX: return m_x;
        case MathUtilities::AxisIndexY: return m_y;
        case MathUtilities::AxisIndexZ: return m_z;
    }

    QT_THROW( std::out_of_range( QString( "index %1 is out of [%2, %3] bounds" )
                                 .arg( axisIndex )
                                 .arg( MathUtilities::AxisIndexX )
                                 .arg( MathUtilities::AxisIndexZ ).toStdString() ) );

    return std::numeric_limits< T >::signaling_NaN();
}

template < typename T >
void Vector3D< T >::normalize()
{
    *this /= this->length();
}

template < typename T >
Vector3D< T > Vector3D< T >::normalized() const
{
    Vector3D pt = *this;
    pt.normalize();

    return pt;
}

template < typename T >
Vector3D< T > Vector3D< T >::normal( const Vector3D< T >& other ) const
{
    return this->cross( other ).normalized();
}

template < typename T >
T Vector3D< T >::angle() const
{
    return MathUtilities::toPositive360DegreesAngleFromRadians( atan2( m_y, m_x ) );
}

template < typename T >
T Vector3D< T >::angleTo( const Vector3D< T >& other ) const
{
    // angles diff: slower due to 2 atan2 operations, compared to sqrts + acos for the dot product one:
    // http://stackoverflow.com/questions/9316995/which-is-more-efficient-atan2-or-sqrt
    // return other.angle() - this->angle();

    // dot product implementation
    return MathUtilities::degrees( acos( this->normalized().dot( other.normalized() ) ) );
}

template < typename T >
template < typename P >
Vector3D< P > Vector3D< T >::toVector3D() const
{
    return Vector3D< P >( m_x, m_y, m_z );
}

template < typename T >
template < typename P >
Vector3D< T >::operator Vector3D< P >() const
{
    return this->toVector3D< P >();
}

template < typename T >
template < typename P >
Vector2D< P > Vector3D< T >::toVector2D() const
{
    return Vector2D< P >( m_x, m_y );
}

template < typename T >
template < typename P >
Vector3D< T >::operator Vector2D< P >() const
{
    return this->toVector2D< P >();
}

template < typename T >
QVector3D Vector3D< T >::toQVector3D() const
{
    return QVector3D( m_x, m_y, m_z );
}

template < typename T >
Vector3D< T >::operator QVector3D() const
{
    return this->toQVector3D();
}

typedef Vector3D< float > Vector3Df;
typedef Vector3D< double > Vector3Dd;

}
}

template < typename T >
Sphereflake::Data::Vector3D< T > operator*( const T& factor, const Sphereflake::Data::Vector3D< T >& pt )
{
    return pt * factor;
}

#ifndef QT_NO_DEBUG_STREAM
template < typename T >
QDebug operator<<( QDebug d, const Sphereflake::Data::Vector3D< T >& pt )
{
    return d.nospace() << "Vector3D< " << STRINGIFY( T ) << " >( " << pt.x() << ", " << pt.y() << ", " << pt.z() << " )";
}
#endif

#endif // SPHEREFLAKE_DATA_VECTOR3D_H
