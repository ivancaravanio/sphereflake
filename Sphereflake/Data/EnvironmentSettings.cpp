#include "EnvironmentSettings.h"

#include "../Sphereflake_namespace.h"
#include "../Utilities/MathUtilities.h"

#include <limits>

using namespace Sphereflake::Data;

// const QColor EnvironmentSettings::s_ambientColorDefault = QColor::fromRgbF( 0.1, 0.1, 0.1 );
const QColor EnvironmentSettings::s_ambientColorDefault = QColor::fromRgbF( 0.1, 0.1, 0.1 );

EnvironmentSettings::EnvironmentSettings()
{
}

EnvironmentSettings::EnvironmentSettings(
        const Light&  aLight,
        const Camera& aCamera,
        const QColor& aAmbientColor )
{
    this->set( aLight,
               aCamera,
               aAmbientColor );
}

void EnvironmentSettings::setLight( const Light& aLight )
{
    if ( ! aLight.isValid()
         || aLight == m_light )
    {
        return;
    }

    m_light = aLight;
    this->changed( TraitLight );
}

void EnvironmentSettings::setCamera( const Camera& aCamera )
{
    if ( ! aCamera.isValid()
         || aCamera == m_camera )
    {
        return;
    }

    m_camera = aCamera;
    this->changed( TraitCamera );
}

void EnvironmentSettings::setAmbientColor( const QColor& aAmbientColor )
{
    if ( ! aAmbientColor.isValid()
         || aAmbientColor == m_ambientColor )
    {
        return;
    }

    m_ambientColor = aAmbientColor;
    this->changed( TraitAmbientColor );
}

void EnvironmentSettings::set(
        const Light&  aLight,
        const Camera& aCamera,
        const QColor& aAmbientColor )
{
    this->setLight( aLight );
    this->setCamera( aCamera );
    this->setAmbientColor( aAmbientColor );
}

bool EnvironmentSettings::operator==( const EnvironmentSettings& other ) const
{
    return this == & other
           || (    m_light == other.m_light
                && m_camera == other.m_camera
                && m_ambientColor == other.m_ambientColor );
}

bool EnvironmentSettings::operator!=( const EnvironmentSettings& other ) const
{
    return ! ( *this == other );
}

quint64 EnvironmentSettings::allTraits() const
{
    return TraitsAll;
}

quint64 EnvironmentSettings::differingTraits( const EnvironmentSettings& other ) const
{
    if ( this == & other )
    {
        return s_noneTrait;
    }

    quint64 traits = s_noneTrait;
    if ( m_light != other.m_light )
    {
        traits |= TraitLight;
    }

    if ( m_camera != other.m_camera )
    {
        traits |= TraitCamera;
    }

    if ( m_ambientColor != other.m_ambientColor )
    {
        traits |= TraitAmbientColor;
    }

    return traits;
}

bool EnvironmentSettings::isValid() const
{
    return m_light.isValid()
           && m_camera.isValid()
           && m_ambientColor.isValid();
}

const EnvironmentSettings& EnvironmentSettings::defaultEnvironmentSettings()
{
    static const EnvironmentSettings settings(
                Light::defaultLight(),
                Camera::defaultCamera(),
                s_ambientColorDefault );

    return settings;
}


#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const EnvironmentSettings& settings )
{
    return d << STRINGIFY( EnvironmentSettings ) << '{' << endl
             << "\tlight: " << settings.light() << endl
             << "\tcamera: " << settings.camera() << endl
             << "\tambient color:" << settings.ambientColor() << endl
             << '}';
}
#endif
