﻿#include "NumberRange.h"

#include "../Utilities/MathUtilities.h"

#include <qmath.h>
#include <QtGlobal>

using namespace Sphereflake::Data;

using namespace Sphereflake::Utilities;

const int NumberRange::s_minDefault = 0;
const int NumberRange::s_maxDefault = 0;

NumberRange::NumberRange()
    : m_min( s_minDefault )
    , m_max( s_maxDefault )
{
}

NumberRange::NumberRange( const int aMin, const int aMax )
    : m_min( s_minDefault )
    , m_max( s_maxDefault )
{
    this->set( aMin, aMax );
}

bool NumberRange::isUnit() const
{
    return m_min == m_max;
}

bool NumberRange::isValid( const int aMin, const int aMax )
{
    return aMin <= aMax;
}

bool NumberRange::isValid() const
{
    return NumberRange::isValid( m_min, m_max );
}

bool NumberRange::set( const int aMin, const int aMax )
{
    if ( ! NumberRange::isValid( aMin, aMax ) )
    {
        return false;
    }

    m_min = aMin;
    m_max = aMax;

    return true;
}

bool NumberRange::setMin( const int aMin )
{
    return this->set( aMin, m_max );
}

bool NumberRange::setMax( const int aMax )
{
    return this->set( m_min, aMax );
}

bool NumberRange::contains( const int value ) const
{
    return m_min <= value && value <= m_max;
}

int NumberRange::boundedValue( const int value ) const
{
    return qBound( m_min, value, m_max );
}

int NumberRange::cyclicValue( const int value ) const
{
    return MathUtilities::cyclicNumber( m_min, value, m_max );
}

int NumberRange::span( const int number ) const
{
    if ( number < m_min )
    {
        return 0;
    }

    return qMin( number, m_max ) - m_min + 1;
}

int NumberRange::span() const
{
    return this->span( m_max );
}

double NumberRange::normalizedSpan( const int number ) const
{
    return this->isValid()
           ? static_cast< double >( this->span( number ) ) / this->span()
           : 0.0;
}

bool NumberRange::operator==( const NumberRange& other ) const
{
    return m_min == other.m_min
           && m_max == other.m_max;
}

bool NumberRange::operator!=( const NumberRange& other ) const
{
    return ! ( *this == other );
}
