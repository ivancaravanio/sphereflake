#ifndef SPHEREFLAKE_DATA_CAMERA_H
#define SPHEREFLAKE_DATA_CAMERA_H

#include "../Utilities/ChangeNotifier.h"
#include "Vector3D.h"

#include <QDebug>
#include <QMatrix4x4>
#include <QtGlobal>

namespace Sphereflake {
namespace Data {

using namespace Sphereflake::Utilities;

class Camera : public ChangeNotifier
{
public:
    enum Trait : quint64
    {
        TraitPosition   = 0x1 << 0,
        TraitDirection  = 0x1 << 1,
        TraitZoomFactor = 0x1 << 2,

        TraitsAll       = TraitPosition
                          | TraitDirection
                          | TraitZoomFactor
    };

public:
    static const Vector3Df s_positionDefault;
    static const Vector3Df s_directionDefault;

    static const float s_zoomFactorMin;
    static const float s_zoomFactorDefault;
    static const float s_zoomFactorMax;
    static const float s_zoomFactorInvalid;

public:
    Camera();
    Camera( const Vector3Df& aPosition,
            const Vector3Df& aDirection,
            const float      aZoomFactor );
    explicit Camera( const QMatrix4x4& aMatrix );

    void setPosition( const Vector3Df& aPosition );
    inline const Vector3Df& position() const
    {
        return m_position;
    }

    void setDirection( const Vector3Df& aDirection );
    inline const Vector3Df& direction() const
    {
        return m_direction;
    }

    static bool isDirectionValid( const Vector3Df& aDirection );

    void setZoomFactor( const float aZoomFactor );
    inline float zoomFactor() const
    {
        return m_zoomFactor;
    }

    static bool isZoomFactorValid( const float aZoomFactor );

    QMatrix4x4 toMatrix() const;

    // in contrast to operator= this setter raises
    // notifications about eventual traits' changes
    void set( const Camera& other );
    void set( const Vector3Df& aPosition,
              const Vector3Df& aDirection,
              const float      aZoomFactor );

    bool isValid() const;

    bool operator==( const Camera& other ) const;
    bool operator!=( const Camera& other ) const;

    quint64 allTraits() const /* override */;
    quint64 differingTraits( const Camera& other ) const;

    static const Camera& defaultCamera();

private:
    Vector3Df m_position;
    Vector3Df m_direction;
    float     m_zoomFactor;
};

}
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const Sphereflake::Data::Camera& camera );
#endif

#endif // SPHEREFLAKE_DATA_CAMERA_H
