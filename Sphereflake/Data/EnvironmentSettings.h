#ifndef SPHEREFLAKE_DATA_ENVIRONMENTSETTINGS_H
#define SPHEREFLAKE_DATA_ENVIRONMENTSETTINGS_H

#include "../Utilities/ChangeNotifier.h"
#include "Camera.h"
#include "Light.h"

#include <QColor>
#include <QDebug>
#include <QList>
#include <QtGlobal>

namespace Sphereflake {
namespace Data {

using namespace Sphereflake::Utilities;

class EnvironmentSettings : public ChangeNotifier
{
public:
    enum Trait : quint64
    {
        TraitLight        = 0x1 << 0,
        TraitCamera       = 0x1 << 1,
        TraitAmbientColor = 0x1 << 2,

        TraitsAll         = TraitLight
                            | TraitCamera
                            | TraitAmbientColor
    };

public:
    static const QColor s_ambientColorDefault;

public:
    EnvironmentSettings();
    EnvironmentSettings(
            const Light&  aLight,
            const Camera& aCamera,
            const QColor& aAmbientColor );

    void setLight( const Light& aLight );
    inline const Light& light() const
    {
        return m_light;
    }

    void setCamera( const Camera& aCamera );
    inline const Camera& camera() const
    {
        return m_camera;
    }

    void setAmbientColor( const QColor& aAmbientColor );
    inline const QColor& ambientColor() const
    {
        return m_ambientColor;
    }

    // in contrast to operator= this setter raises
    // notifications about eventual traits' changes
    void set( const EnvironmentSettings& other );
    void set( const Light&  aLight,
              const Camera& aCamera,
              const QColor& aAmbientColor );

    bool operator==( const EnvironmentSettings& other ) const;
    bool operator!=( const EnvironmentSettings& other ) const;

    quint64 allTraits() const /* override */;
    quint64 differingTraits( const EnvironmentSettings& other ) const /* override */;

    bool isValid() const;

    static const EnvironmentSettings& defaultEnvironmentSettings();

private:
    Light  m_light;
    Camera m_camera;
    QColor m_ambientColor;
};

}
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const Sphereflake::Data::EnvironmentSettings& environmentSettings );
#endif

#endif // SPHEREFLAKE_DATA_ENVIRONMENTSETTINGS_H
