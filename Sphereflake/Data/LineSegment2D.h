#ifndef SPHEREFLAKE_DATA_LINESEGMENT2D_H
#define SPHEREFLAKE_DATA_LINESEGMENT2D_H

#include "Vector2D.h"

#include <QDebug>
#include <QLine>
#include <QLineF>

namespace Sphereflake {
namespace Data {

/*
 *  line:         <----------------->
 *  ray:          |----------------->
 *  line segment: |-----------------|
 */

class LineSegment2D
{
public:
    explicit LineSegment2D( const Vector2Dd& aStartPt, const Vector2Dd& aEndPt );
    explicit LineSegment2D( const QLineF& other );
    explicit LineSegment2D( const QLine& other );
    LineSegment2D();

    void setStartPt( const Vector2Dd& aStartPt );
    inline const Vector2Dd& startPt() const
    {
        return m_startPt;
    }

    void setEndPt( const Vector2Dd& aEndPt );
    inline const Vector2Dd& endPt() const
    {
        return m_endPt;
    }

    void set( const Vector2Dd& aStartPt, const Vector2Dd& aEndPt );

    bool isValid() const;
    bool isPoint() const;
    double length() const;

    bool operator==( const LineSegment2D& other ) const;
    bool operator!=( const LineSegment2D& other ) const;

    double angle() const;

    // ------------- x |-------------|
    bool lineIntersects(
            const LineSegment2D& lineSegment,
            const bool accountEnds = true,
            Vector2Dd* const intersectionPt = nullptr ) const;

    // |-------------> x |-------------|
    bool rayIntersects(
            const LineSegment2D& other,
            const bool accountEnds = true,
            Vector2Dd* const intersectionPt = nullptr ) const;

    // |-------------| x |-------------|
    bool intersects(
            const LineSegment2D& other,
            const bool accountEnds = true,
            Vector2Dd* const intersectionPt = nullptr ) const;
    bool isParallelTo( const LineSegment2D& other ) const;
    bool contains( const Vector2Dd& pt, const bool accountEnds = true ) const;
    bool contains( const LineSegment2D& other, const bool accountEnds = true ) const;

    double minX() const;
    double maxX() const;
    double minY() const;
    double maxY() const;

    bool boundingBoxContainsX( const double& xValue, const bool accountEnds = true ) const;
    bool boundingBoxContainsY( const double& yValue, const bool accountEnds = true ) const;
    bool boundingBoxContains( const Vector2Dd& pt, const bool accountEnds = true ) const;
    double slope() const;
    double xIntercept() const;
    double yIntercept() const;

    Vector2Dd vector() const;
    Vector2Dd unitVector() const;
    bool isHorizontal() const;
    bool isVertical() const;

    QLineF toQLineF() const;
    operator QLineF() const;

    QLine toQLine() const;
    operator QLine() const;

private:
    Vector2Dd m_startPt;
    Vector2Dd m_endPt;
};

}
}

Sphereflake::Data::LineSegment2D operator*( const double& factor, const Sphereflake::Data::LineSegment2D& lineSegment );

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const Sphereflake::Data::LineSegment2D& lineSegment );
#endif

#endif // SPHEREFLAKE_DATA_LINESEGMENT2D_H
