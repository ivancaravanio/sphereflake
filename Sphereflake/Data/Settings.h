#ifndef SPHEREFLAKE_DATA_SETTINGS_H
#define SPHEREFLAKE_DATA_SETTINGS_H

#include "../Utilities/ChangeNotifier.h"
#include "SphereflakeSettings.h"
#include "EnvironmentSettings.h"

#include <QDebug>

namespace Sphereflake {
namespace Data {

using namespace Sphereflake::Utilities;

class Settings : public ChangeNotifier
{
public:
    enum Trait : quint64
    {
        TraitSphereflakeSettings = 0x1 << 0,
        TraitEnvironmentSettings = 0x1 << 1,
        TraitsAll                = TraitSphereflakeSettings
                                   | TraitEnvironmentSettings
    };

public:
    Settings();
    Settings( const SphereflakeSettings& aSphereflakeSettings,
              const EnvironmentSettings& aEnvironmentSettings );

    void setSphereflakeSettings( const SphereflakeSettings& aSphereflakeSettings );
    inline const SphereflakeSettings& sphereflakeSettings() const
    {
        return m_sphereflakeSettings;
    }

    void setEnvironmentSettings( const EnvironmentSettings& aEnvironmentSettings );
    inline const EnvironmentSettings& environmentSettings() const
    {
        return m_environmentSettings;
    }

    // in contrast to operator= this setter raises
    // notifications about eventual traits' changes
    void set( const Settings& other );
    void set( const SphereflakeSettings& aSphereflakeSettings,
              const EnvironmentSettings& aEnvironmentSettings );

    bool operator==( const Settings& other ) const;
    bool operator!=( const Settings& other ) const;

    quint64 allTraits() const /* override */;
    quint64 differingTraits( const Settings& other ) const /* override */;

    bool isValid() const;

    static const Settings& defaultSettings();

private:
    SphereflakeSettings m_sphereflakeSettings;
    EnvironmentSettings m_environmentSettings;
};

}
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const Sphereflake::Data::Settings& Settings );
#endif

#endif // SPHEREFLAKE_DATA_SETTINGS_H
