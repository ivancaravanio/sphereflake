#include "Settings.h"

#include "../Sphereflake_namespace.h"
#include "../Utilities/MathUtilities.h"

#include <limits>

using namespace Sphereflake::Data;

Settings::Settings()
{
}

Settings::Settings(
        const SphereflakeSettings& aSphereflakeSettings,
        const EnvironmentSettings& aEnvironmentSettings )
{
    this->set( aSphereflakeSettings,
               aEnvironmentSettings );
}

void Settings::setSphereflakeSettings( const SphereflakeSettings& aSphereflakeSettings )
{
    if ( ! aSphereflakeSettings.isValid()
         || aSphereflakeSettings == m_sphereflakeSettings )
    {
        return;
    }

    m_sphereflakeSettings = aSphereflakeSettings;
    this->changed( TraitSphereflakeSettings );
}

void Settings::setEnvironmentSettings( const EnvironmentSettings& aEnvironmentSettings )
{
    if ( ! aEnvironmentSettings.isValid()
         || aEnvironmentSettings == m_environmentSettings )
    {
        return;
    }

    m_environmentSettings = aEnvironmentSettings;
    this->changed( TraitEnvironmentSettings );
}

void Settings::set( const Settings& other )
{
    this->set( other.sphereflakeSettings(),
               other.environmentSettings() );
}

void Settings::set(
        const SphereflakeSettings& aSphereflakeSettings,
        const EnvironmentSettings& aEnvironmentSettings )
{
    this->setSphereflakeSettings( aSphereflakeSettings );
    this->setEnvironmentSettings( aEnvironmentSettings );
}

bool Settings::operator==( const Settings& other ) const
{
    return this == & other
           || (    m_sphereflakeSettings == other.m_sphereflakeSettings
                && m_environmentSettings == other.m_environmentSettings );
}

bool Settings::operator!=( const Settings& other ) const
{
    return ! ( *this == other );
}

quint64 Settings::allTraits() const
{
    return TraitsAll;
}

quint64 Settings::differingTraits( const Settings& other ) const
{
    if ( this == & other )
    {
        return s_noneTrait;
    }

    quint64 traits = s_noneTrait;
    if ( m_sphereflakeSettings != other.m_sphereflakeSettings )
    {
        traits |= TraitSphereflakeSettings;
    }

    if ( m_environmentSettings != other.m_environmentSettings )
    {
        traits |= TraitEnvironmentSettings;
    }

    return traits;
}

bool Settings::isValid() const
{
    return m_sphereflakeSettings.isValid()
           && m_environmentSettings.isValid();
}

const Settings& Settings::defaultSettings()
{
    static const Settings settings(
                SphereflakeSettings::defaultSphereflakeSettings(),
                EnvironmentSettings::defaultEnvironmentSettings() );

    return settings;
}


#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const Settings& settings )
{
    return d << STRINGIFY( Settings ) << '{' << endl
             << "\tsphereflake settings: " << settings.sphereflakeSettings() << endl
             << "\tenvironment settings: " << settings.environmentSettings() << endl
             << '}';
}
#endif
