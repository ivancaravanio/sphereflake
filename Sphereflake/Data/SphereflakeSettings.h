#ifndef SPHEREFLAKE_DATA_SPHEREFLAKESETTINGS_H
#define SPHEREFLAKE_DATA_SPHEREFLAKESETTINGS_H

#include "../Utilities/ChangeNotifier.h"
#include "Vector3D.h"

#include <QColor>
#include <QDebug>
#include <QList>
#include <QtGlobal>

namespace Sphereflake {
namespace Data {

using namespace Sphereflake::Utilities;

class SphereflakeSettings : public ChangeNotifier
{
public:
    enum Trait : quint64
    {
        TraitLevelsCount              = 0x1 << 0,
        TraitInitialSphereRadius      = 0x1 << 1,
        TraitChildSphereRadiusFactor  = 0x1 << 2,
        TraitChildSphereOffsetFactor  = 0x1 << 3,
        TraitSpheresAlternatingColors = 0x1 << 4,
        TraitShininess                = 0x1 << 5,
        TraitsAll                     = TraitLevelsCount
                                        | TraitInitialSphereRadius
                                        | TraitChildSphereRadiusFactor
                                        | TraitChildSphereOffsetFactor
                                        | TraitSpheresAlternatingColors
                                        | TraitShininess
    };

public:
    static const quint64         s_levelsCountMin;
    static const quint64         s_levelsCountDefault;
    static const quint64         s_levelsCountMax;
    static const quint64         s_levelsCountInvalid;

    static const float           s_initialSphereRadiusMin;
    static const float           s_initialSphereRadiusDefault;
    static const float           s_initialSphereRadiusMax;
    static const float           s_initialSphereRadiusInvalid;

    static const float           s_childSphereRadiusFactorMin;
    static const float           s_childSphereRadiusFactorDefault;
    static const float           s_childSphereRadiusFactorMax;
    static const float           s_childSphereRadiusFactorInvalid;

    static const float           s_childSphereOffsetFactorMin;
    static const float           s_childSphereOffsetFactorDefault;
    static const float           s_childSphereOffsetFactorMax;
    static const float           s_childSphereOffsetFactorInvalid;

    static const int             s_spheresAlternatingColorsCountMin;
    static const int             s_spheresAlternatingColorsCountMax;
    static const QList< QColor > s_spheresAlternatingColorsDefault;

    static const float           s_shininessMin;
    static const float           s_shininessDefault;
    static const float           s_shininessMax;
    static const float           s_shininessInvalid;

public:
    SphereflakeSettings();
    SphereflakeSettings(
            const quint64&         aLevelsCount,
            const float            aInitialSphereRadius,
            const float            aChildSphereRadiusFactor,
            const float            aChildSphereOffsetFactor,
            const QList< QColor >& aSpheresAlternatingColors,
            const float            aShininess );

    void setLevelsCount( const quint64& aLevelsCount );
    inline const quint64& levelsCount() const
    {
        return m_levelsCount;
    }

    static bool isLevelsCountValid( const quint64& aLevelsCount );

    void setInitialSphereRadius( const float aInitialSphereRadius );
    inline float initialSphereRadius() const
    {
        return m_initialSphereRadius;
    }

    static bool isInitialSphereRadiusValid( const float aInitialSphereRadius );

    void setChildSphereRadiusFactor( const float aChildSphereRadiusFactor );
    inline float childSphereRadiusFactor() const
    {
        return m_childSphereRadiusFactor;
    }

    static bool isChildSphereRadiusFactorValid( const float aChildSphereRadiusFactor );

    void setChildSphereOffsetFactor( const float aChildSphereOffsetFactor );
    inline float childSphereOffsetFactor() const
    {
        return m_childSphereOffsetFactor;
    }

    static bool isChildSphereOffsetFactorValid( const float aChildSphereOffsetFactor );

    void setSpheresAlternatingColors( const QList< QColor >& aSpheresAlternatingColors );
    inline const QList< QColor >& spheresAlternatingColors() const
    {
        return m_spheresAlternatingColors;
    }

    static bool isSpheresAlternatingColorsCountValid( const int aSpheresAlternatingColorsCountValid );
    static bool areSpheresAlternatingColorsValid( const QList< QColor >& aSpheresAlternatingColors );

    void setShininess( const float aShininess );
    inline float shininess() const
    {
        return m_shininess;
    }

    static bool isShininessValid( const float aShininess );

    // in contrast to operator= this setter raises
    // notifications about eventual traits' changes
    void set( const SphereflakeSettings& other );
    void set( const quint64&         aLevelsCount,
              const float            aInitialSphereRadius,
              const float            aChildSphereRadiusFactor,
              const float            aChildSphereOffsetFactor,
              const QList< QColor >& aSpheresAlternatingColors,
              const float            aShininess );

    bool operator==( const SphereflakeSettings& other ) const;
    bool operator!=( const SphereflakeSettings& other ) const;

    quint64 allTraits() const /* override */;
    quint64 differingTraits( const SphereflakeSettings& other ) const;

    bool isValid() const;

    static const SphereflakeSettings& defaultSphereflakeSettings();

private:
    quint64         m_levelsCount;
    float           m_initialSphereRadius;
    float           m_childSphereRadiusFactor;
    float           m_childSphereOffsetFactor;
    QList< QColor > m_spheresAlternatingColors;
    float           m_shininess;
};

}
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const Sphereflake::Data::SphereflakeSettings& settings );
#endif

#endif // SPHEREFLAKE_DATA_SPHEREFLAKESETTINGS_H
