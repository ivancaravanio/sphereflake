#ifndef SPHEREFLAKE_SPHEREFLAKESETTINGSEDITOR_H
#define SPHEREFLAKE_SPHEREFLAKESETTINGSEDITOR_H

#include "Data/SphereflakeSettings.h"
#include "Utilities/Retranslatable.h"

#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QDoubleSpinBox)
QT_FORWARD_DECLARE_CLASS(QLabel)
QT_FORWARD_DECLARE_CLASS(QSpinBox)

namespace Sphereflake {

class ColorsPicker;

using namespace Sphereflake::Data;
using namespace Sphereflake::Utilities;

class SphereflakeSettingsEditor : public QWidget, public Retranslatable
{
    Q_OBJECT

public:
    explicit SphereflakeSettingsEditor( QWidget* const parent = nullptr, Qt::WindowFlags f = 0 );

    void setSphereflakeSettings( const SphereflakeSettings& aSphereflakeSettings );
    SphereflakeSettings sphereflakeSettings() const;

    void retranslate() /* override */;

private:
    void retranslateSelfOnly();

private:
    QLabel*         m_levelsCountLabel;
    QSpinBox*       m_levelsCountEditor;
    QLabel*         m_initialSphereRadiusLabel;
    QDoubleSpinBox* m_initialSphereRadiusEditor;
    QLabel*         m_childSphereRadiusFactorLabel;
    QDoubleSpinBox* m_childSphereRadiusFactorEditor;
    QLabel*         m_childSphereOffsetFactorLabel;
    QDoubleSpinBox* m_childSphereOffsetFactorEditor;
    ColorsPicker*   m_colorsPicker;
    QLabel*         m_shininessLabel;
    QDoubleSpinBox* m_shininessEditor;
};

}

#endif // SPHEREFLAKE_SPHEREFLAKESETTINGSEDITOR_H
