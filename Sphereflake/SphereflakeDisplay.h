#ifndef SPHEREFLAKE_SPHEREFLAKEDISPLAY_H
#define SPHEREFLAKE_SPHEREFLAKEDISPLAY_H

// glew.h should be included prior to including gl.h
#include <GL/glew.h>

#include "Data/Settings.h"
#include "Data/Triangle2D.h"
#include "Data/Vector3D.h"
#include "Utilities/OpenGl/AbstractOpenGlClient.h"
#include "Utilities/OpenGl/OpenGlUtilities.h"
#include "Utilities/OpenGl/ShaderProgram.h"
#include "Utilities/Value.h"

#include <QByteArray>
#include <QColor>
#include <QGenericMatrix>
#include <QGLWidget>
#include <QHash>
#include <QList>
#include <QMap>
#include <QMatrix4x4>
#include <QRect>
#include <QString>
#include <QVector>

QT_FORWARD_DECLARE_CLASS(QKeyEvent)
QT_FORWARD_DECLARE_CLASS(QMouseEvent)

#ifndef QT_NO_WHEELEVENT
    QT_FORWARD_DECLARE_CLASS(QWheelEvent)
#endif

namespace Sphereflake {

using namespace Sphereflake::Data;
using namespace Sphereflake::Utilities::OpenGl;

class SphereflakeDisplay : public QGLWidget, public AbstractOpenGlClient
{
    Q_OBJECT

public:
    explicit SphereflakeDisplay( const QGLFormat& aOpenGlFormat, QWidget* const parent = nullptr );
    ~SphereflakeDisplay() /* override */;

    inline const Settings& settings() const
    {
        return m_settings;
    }

    void init() /* override */;

public slots:
    void setSettings( const Settings& aSettings );

signals:
    void isFocusedChanged( const bool isFocused );
    void cameraChanged();

protected:
    void initializeGL() /* override */;
    void resizeGL( int w, int h ) /* override */;
    void paintGL() /* override */;

    void mousePressEvent( QMouseEvent* event ) /* override */;
    void mouseMoveEvent( QMouseEvent* event ) /* override */;
    void mouseReleaseEvent( QMouseEvent* event ) /* override */;

    void keyPressEvent( QKeyEvent* event ) /* override */;

    void focusInEvent( QFocusEvent* event ) /* override */;
    void focusOutEvent( QFocusEvent* event ) /* override */;

#ifndef QT_NO_WHEELEVENT
    void wheelEvent( QWheelEvent* event ) /* override */;
#endif

    static void printGLInfo();

    using AbstractOpenGlClient::viewportAlternator;

private:
    enum GenericVertexAttributeIndex
    {
        GenericVertexAttributeIndexInvalid = -1,
        GenericVertexAttributeIndexVertexPos,
        GenericVertexAttributesCount
    };

    enum FragmentOutputVariableIndex
    {
        FragmentOutputVariableIndexInvalid = -1,
        FragmentOutputVariableIndexColor,
        FragmentOutputVariablesCount
    };

    enum UniformVariableIndex
    {
        UniformVariableIndexInvalid = -1,

        UniformVariableIndexModelMatrix,
        UniformVariableIndexNormalMatrix,
        UniformVariableIndexViewMatrix,
        UniformVariableIndexProjectionMatrix,

        UniformVariableIndexSphereCenterPt,
        UniformVariableIndexSphereRadius,

        UniformVariableIndexAmbientColor,

        UniformVariableIndexMaterialColor,
        UniformVariableIndexMaterialShininessLevel,

        UniformVariableIndexLightPosition,
        UniformVariableIndexLightColor,
        UniformVariableIndexLightIntensityLevel,
        UniformVariableIndexLightAttenuationConstant,
        UniformVariableIndexLightAttenuationLinear,
        UniformVariableIndexLightAttenuationQuadratic,

        UniformVariableIndexCameraPos,
        UniformVariableIndexShouldShadeSurface,
        UniformVariablesCount
    };

    enum MatrixType
    {
        MatrixTypeModel,
        MatrixTypeNormal,
        MatrixTypeView,
        MatrixTypeProjection
    };

    struct SphereDrawData
    {
    public:
        SphereDrawData( const quint64& aLevel,
                        const QMatrix4x4& aModelMatrix,
                        const QMatrix3x3& aNormalMatrix );

    public:
        quint64    level;
        QMatrix4x4 modelMatrix;
        QMatrix3x3 normalMatrix;
    };

private:
    static const QList< GLuint > s_allOrderedGenericVertexAttributeIndices;
    static const float s_initialSphereDefaultRadius;

private:
    static QByteArray uniformVariableName( const int index );
    static bool isUniformVariableIndexValid( const int index );
    GLint uniformVariableLocation( const int index ) const;
    void cacheUniformVariableLocations();
    GLint uniformVariableCachedLocation( const int index ) const;

    void enableGenericVertexAttributeArrays( const bool enable );

    void disposeOpenGlResources();

    QRect viewport() const;
    ViewportAlternator viewportAlternator( const bool autoRollback = true ) const;
    void resetViewport();

    using AbstractOpenGlClient::vaoBinder;
    OpenGlObjectBinder vaoBinder( const bool autoUnbind = true ) const;

    using AbstractOpenGlClient::vboBinder;
    OpenGlObjectBinder vboBinder( const bool autoUnbind = true ) const;

    static QVector< Vector3Df > vertices( const Triangle2D& triangle );
    static QVector< Vector3Df > vertices( const QList< Triangle2D >& triangles );
    static QVector< Vector3Df > vertices( const QList< Triangle2D* >& triangles );
    static QVector< Vector3Df > vertices( const QList< Polygon2D >& polygons );

    static bool isMatrixTypeValid( const MatrixType matrixType );

    // valid only for: model, view, projection
    // normal matrix is automatically updated upon altering model matrix
    void setMatrix( const MatrixType aMatrixType,
                    const QMatrix4x4& aMatrix,
                    const QMatrix3x3* const aNormalMatrix = nullptr,
                    const bool aShouldUpdateDisplay = true,
                    const bool aNotifyCameraChangedIfNeeded = true );
    void applyMatrix( const MatrixType matrixType,
                      const QMatrix3x3* const normalMatrix = nullptr,
                      const bool shouldUpdateDisplay = true );
    void applyMatrices( const bool shouldUpdateDisplay = true );
    const QMatrix4x4& matrix( const MatrixType matrixType ) const;
    QMatrix4x4* mutableMatrix( const MatrixType matrixType );
    static int matrixUniformVariableIndex( const MatrixType matrixType );
    GLint matrixUniformVariableLocation( const MatrixType matrixType ) const;

    static QMatrix3x3 normalMatrix( const QMatrix4x4& matrix );
    QMatrix3x3 normalMatrixCalculated() const;
    QMatrix4x4 projectionMatrixCalculated() const;
    void resetProjectionMatrix( const bool shouldUpdateDisplay = true );

    static QVector< Vector3Df > octahedronTriangles();
    void loadGeometry();

    void setShouldShadeSurface( const bool aShouldShadeSurface );

    void applySettings( const Settings& previousSettings = Settings(),
                        const bool shouldUpdateDisplayIfNeeded = true );

    void drawSpheres();
    void resetSpheresDrawData();
    void cacheSphere( const QMatrix4x4& sphereMatrix,
                      const quint64&    levelIndex,
                      const bool        shouldCacheChildSpheres );
    void cacheChildSpheres( const QMatrix4x4& motherSphereMatrix,
                            const quint64&    motherSphereLevelIndex,
                            const bool        shouldCacheChildSpheres );
    void cacheChildSphere( const QMatrix4x4&     motherSphereMatrix,
                           const quint64&        levelIndex,
                           const float           zRotationAngle,
                           const Value< float >& yRotationAngle,
                           const bool            shouldCacheChildSpheres );
    bool isLevelIndexValid( const quint64& levelIndex ) const;

    void notifyFocusChanged();

private:
    Settings m_settings;
    ShaderProgram m_shaderProgram;
    GLuint m_vao;
    GLuint m_vbo;

    QHash< int, GLint > m_uniformVariableCachedLocations;

    QMatrix4x4 m_modelMatrix;
    QMatrix4x4 m_viewMatrix;
    QMatrix4x4 m_projectionMatrix;

    int m_verticesCount;

    QList< SphereDrawData > m_spheresDrawData;

    bool m_isGlInitialized;
};

}

#endif // SPHEREFLAKE_SPHEREFLAKEDISPLAY_H
