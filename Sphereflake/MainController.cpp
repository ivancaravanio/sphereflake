#include "MainController.h"

#include "MainWindow.h"
#include "Utilities/GeneralUtilities.h"
#include "Utilities/WidgetUtilities.h"

#include <QApplication>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>

using namespace Sphereflake;

MainController::MainController( QObject* parent )
    : QObject( parent )
{
}

MainController::~MainController()
{
}

void MainController::init()
{
    MainWindow* const mainWindow = new MainWindow;
    m_mainWindow.reset( mainWindow );

    mainWindow->setGeometry( WidgetUtilities::scaledConcentricCurrentDesktopGeometry( 3.0 / 4.0, true, mainWindow ) );
    mainWindow->show();
}
