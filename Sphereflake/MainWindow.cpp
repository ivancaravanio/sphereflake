#include "MainWindow.h"

// glew.h should be included prior to including gl.h
#include <GL/glew.h>

#include "SphereflakeControl.h"
#include "Utilities/GeneralUtilities.h"

#include <QApplication>
#include <QHBoxLayout>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QTextStream>

using namespace Sphereflake;

MainWindow::MainWindow( QWidget* parent )
    : QMainWindow( parent )
    , m_helpMenu( nullptr )
    , m_aboutAction( nullptr )
    , m_mainControl( nullptr )
{
    QMenuBar* const menuBar = this->menuBar();

    m_helpMenu = new QMenu;
    m_aboutAction = m_helpMenu->addAction( QString(), this, SLOT(about()) );
    menuBar->addMenu( m_helpMenu );

    m_mainControl = new SphereflakeControl;
    this->setCentralWidget( m_mainControl );

    this->retranslateSelfOnly();
}

void MainWindow::retranslate()
{
    this->retranslateSelfOnly();
}

void MainWindow::about()
{
    QMessageBox* const aboutMessageBox = new QMessageBox( this );
    connect( aboutMessageBox, SIGNAL(finished(int)), aboutMessageBox, SLOT(deleteLater()) );
    aboutMessageBox->setText( MainWindow::aboutText() );
    aboutMessageBox->setIconPixmap( QPixmap( "://Icons/Icon_64.png" ) );
    aboutMessageBox->show();
}

void MainWindow::retranslateSelfOnly()
{
    m_helpMenu->setTitle( tr( "&Help" ) );
    m_aboutAction->setText( tr( "&About..." ) );
}

QString MainWindow::aboutText()
{
    // TODO: find a way to detect processor architecture for Linux
#if defined(Q_OS_WIN32) || defined(Q_OS_MAC32)
    static const QString bitness = "32-bit";
#elif defined(Q_OS_WIN64) || defined(Q_OS_MAC64)
    static const QString bitness = "64-bit";
#endif

    QString aboutText;
    QTextStream ts( & aboutText );
    ts << QString( "%1 %2 %3" ).arg( qAppName() ).arg( QApplication::applicationVersion() ).arg( bitness ) << endl
       << tr( "Copyright %1 %2" ).arg( QChar( 0x00A9 ) ).arg( QLatin1String( COMPANY ) ) << endl
       << QLatin1String( COMPANY_DOMAIN ) << endl
       << tr( "Used libraries:" ) << endl
       << tr( "\tQt %1" ).arg( QT_VERSION_STR ) << endl
       << tr( "\tOpenGL Extension Wrangler Library (GLEW) %1" )
          .arg( QLatin1String( reinterpret_cast< const char* >( glewGetString( GLEW_VERSION ) ) ) );

    return aboutText;
}
