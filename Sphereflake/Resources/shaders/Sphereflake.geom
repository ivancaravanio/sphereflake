#version 400 core

/* 0: in
 * 1: out
 * 2: uniform
 */

// 0: in
layout (location = 0) in vec3 vVertexPos;
layout (location = 1) in vec3 vSphereCenterPt;
layout (location = 2) in float vSphereRadius;

// 1: out
out vec3 tcVertexPos;
out vec3 tcSphereCenterPt;
out float tcSphereRadius;

// 2: uniform

void main()
{

}
