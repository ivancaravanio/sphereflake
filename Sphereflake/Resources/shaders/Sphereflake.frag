#version 400 core

in vec3 teSurfacePos;
in vec3 teSurfaceNormal;
in vec3 teCameraPos;

layout (location = 0) out vec4 fColor;

uniform vec3 ambientColor;

struct Material
{
    vec4 color;
    float shininessLevel;
};

struct Attenuation
{
    float constant;
    float linear;
    float quadratic;
};

struct Light
{
    vec3 position;
    vec3 color;
    float intensityLevel;
    Attenuation attenuation;
};

uniform Material material;
uniform Light light;

uniform bool shouldShadeSurface;

const float eps = 1E-6f;
const vec4 eyeLuminanceFactor = vec4( 0.299f, 0.587f, 0.114f, 1.0f );
const vec4 srgbLuminanceFactor = vec4( 0.2125f, 0.7154f, 0.0721f, 1.0f );

// #define ATTENUATION_ON

vec4 overlay( vec4 baseColor, vec4 blendColor )
{
    float luminance = dot( baseColor, srgbLuminanceFactor );
    vec4 lowerLumOverlay = 2.0f * blendColor * baseColor;
    if ( luminance < 0.45f )
    {
        return lowerLumOverlay;
    }

    const vec4 whiteColor = vec4( 1.0 );
    vec4 higherLumOverlay = whiteColor - 2.0f * ( whiteColor - blendColor ) * ( whiteColor - baseColor );
    return luminance > 0.55f
           ? higherLumOverlay
           : mix( lowerLumOverlay, higherLumOverlay, ( luminance - 0.45f ) * 10.0f );
}

vec4 blinnPhongIlluminationModel(
    vec3  aAmbientColor,

    vec3  aSurfacePos,
    vec3  aSurfaceNormal,
    vec4  aSurfaceColor,
    float aSurfaceShininessLevel,

    vec3  aEyePos,

    vec3  aLightPos,
    vec3  aLightColor,
    float aLightIntensityLevel,
    float aLightConstantAttenuation,
    float aLightLinearAttenuation,
    float aLightQuadraticAttenuation )
{
    vec3 lightDir = aLightPos - aSurfacePos;
    float lightSurfaceDistance = length( lightDir );

#ifdef ATTENUATION_ON
    float attenuation = 1.0f / (   aLightConstantAttenuation
                                 + aLightLinearAttenuation    * lightSurfaceDistance
                                 + aLightQuadraticAttenuation * lightSurfaceDistance * lightSurfaceDistance );
#else
    float attenuation = 1.0f;
#endif

    vec3  finalLightColor             = aLightIntensityLevel * attenuation * aLightColor;
    vec3  lightDirUnit                = normalize( lightDir );
    vec3  normalUnit                  = normalize( aSurfaceNormal );
    float diffuseColorIntensityLevel  = max( dot( lightDirUnit, normalUnit ), 0.0f );
    float specularColorIntensityLevel = diffuseColorIntensityLevel > eps
                                        ? pow( max( dot( normalize( lightDirUnit + normalize( aEyePos - aSurfacePos ) ),
                                                         normalUnit ),
                                                    0.0f ),
                                               aSurfaceShininessLevel )
                                        : 0.0f;

    vec3 finalColor = clamp( ( ambientColor
                               + diffuseColorIntensityLevel * aSurfaceColor.rgb
                               // + diffuseColorIntensityLevel * ( aSurfaceColor.rgb + finalLightColor ) / 2.0f
                               + specularColorIntensityLevel * finalLightColor ),
                             vec3( 0.0f ),
                             vec3( 1.0f ) );

    vec3 intensity = vec3( dot( finalColor, srgbLuminanceFactor.rgb ) );
    finalColor = mix( intensity, finalColor, 1.0f );

    return vec4( finalColor,
                 aSurfaceColor.a );
}

void main()
{
    // https://en.wikipedia.org/wiki/Blinn%E2%80%93Phong_shading_model
    // OpenGL SuperBible 6th edition -> Rendering Techniques -> Lighting Models -> Blinn-Phong Lighting
    if ( shouldShadeSurface )
    {
        fColor = blinnPhongIlluminationModel( ambientColor,
                                              teSurfacePos,
                                              teSurfaceNormal,
                                              material.color,
                                              material.shininessLevel,
                                              teCameraPos,
                                              light.position,
                                              light.color,
                                              light.intensityLevel,
                                              light.attenuation.constant,
                                              light.attenuation.linear,
                                              light.attenuation.quadratic );
    }
    else
    {
        fColor = material.color;
    }
}
