#ifndef SPHEREFLAKE_MAINWINDOW_H
#define SPHEREFLAKE_MAINWINDOW_H

#include "Utilities/Retranslatable.h"

#include <QMainWindow>
#include <QStringList>

QT_FORWARD_DECLARE_CLASS(QAction)
QT_FORWARD_DECLARE_CLASS(QMenu)

namespace Sphereflake {

class SphereflakeControl;

using namespace Sphereflake::Utilities;

class MainWindow : public QMainWindow, public Retranslatable
{
    Q_OBJECT

public:
    explicit MainWindow( QWidget* const parent = nullptr );

    void retranslate() /* override */;

private slots:
    void about();

private:
    void retranslateSelfOnly();
    static QString aboutText();

private:
    // help menu
    QMenu*   m_helpMenu;
    QAction* m_aboutAction;

    SphereflakeControl* m_mainControl;
};

}

#endif // SPHEREFLAKE_MAINWINDOW_H
