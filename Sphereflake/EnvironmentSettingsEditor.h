#ifndef SPHEREFLAKE_ENVIRONMENTSETTINGSEDITOR_H
#define SPHEREFLAKE_ENVIRONMENTSETTINGSEDITOR_H

#include "Data/EnvironmentSettings.h"
#include "Utilities/Retranslatable.h"

#include <QList>
#include <QPair>
#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QGroupBox)
QT_FORWARD_DECLARE_CLASS(QLabel)

namespace Sphereflake {

class CameraEditor;
class ColorPicker;
class LightEditor;

using namespace Sphereflake::Data;
using namespace Sphereflake::Utilities;

class EnvironmentSettingsEditor : public QWidget, public Retranslatable
{
    Q_OBJECT

public:
    explicit EnvironmentSettingsEditor( QWidget* const parent = nullptr, Qt::WindowFlags f = 0 );

    void setEnvironmentSettings( const EnvironmentSettings& aEnvironmentSettings );
    EnvironmentSettings environmentSettings() const;

    void retranslate() /* override */;

private:
    void retranslateSelfOnly();

private:
    QGroupBox*    m_lightEditorGroupBox;
    LightEditor*  m_lightEditor;
    QGroupBox*    m_cameraEditorGroupBox;
    CameraEditor* m_cameraEditor;
    QLabel*       m_ambientColorLabel;
    ColorPicker*  m_ambientColorEditor;
};

}

#endif // SPHEREFLAKE_ENVIRONMENTSETTINGSEDITOR_H
