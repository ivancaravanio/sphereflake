#include "ColorPicker.h"

#include "Utilities/ImageUtilities.h"
#include "Utilities/MathUtilities.h"

#include <QColorDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QRegExpValidator>
#include <QSpinBox>

using namespace Sphereflake;

using namespace Sphereflake::Utilities;

ColorPicker::ColorPicker( QWidget* parent )
    : QWidget( parent )
    , m_color( Qt::black )
    , m_colorHexEditor( nullptr )
    , m_colorDisplayPickerTrigger( nullptr )
    , m_colorPickerDialog( nullptr )
{
    this->init();
}

ColorPicker::ColorPicker( const QColor& aColor, QWidget* parent )
    : QWidget( parent )
    , m_color( Qt::black )
    , m_colorHexEditor( nullptr )
    , m_colorDisplayPickerTrigger( nullptr )
    , m_colorPickerDialog( nullptr )
{
    this->init();
    this->setColor( aColor );
}

void ColorPicker::setColor( const QColor& aColor )
{
    if ( ! aColor.isValid()
         || aColor == m_color )
    {
        return;
    }

    m_color = aColor;
    this->applyColor();

    emit valueChanged();
}

void ColorPicker::init()
{
    QHBoxLayout* const mainLayout = new QHBoxLayout( this );
    mainLayout->setContentsMargins( QMargins() );

    const int colorComponentMin            = ImageUtilities::byteColorComponentMin();
    const int colorComponentMax            = ImageUtilities::byteColorComponentMax();
    const int colorComponentMaxDigitsCount = MathUtilities::digitsCount( colorComponentMax );

    for ( int i = 0; i < ImageUtilities::ColorComponentsCount; ++ i )
    {
        QLabel* const colorComponentLabel = new QLabel;
        m_colorComponentsLabels[ i ] = colorComponentLabel;

        QSpinBox* const colorComponentEditor = new QSpinBox;
        colorComponentEditor->setRange( colorComponentMin,
                                        colorComponentMax );
        connect( colorComponentEditor,
                 SIGNAL(valueChanged(int)),
                 this,
                 SLOT(onColorUpdatedFromComponentsEditor()) );

        mainLayout->addWidget( colorComponentLabel, 0 );
        mainLayout->addWidget( colorComponentEditor, colorComponentMaxDigitsCount );

        m_colorComponentsEditors[ i ] = colorComponentEditor;
    }

    const int colorComponentsDoubledCount = ImageUtilities::ColorComponentsCount * 2;

    m_colorHexEditor = new QLineEdit;
    m_colorHexEditor->setValidator( new QRegExpValidator( QRegExp( QString( "^[0-9A-F]{%1}$" ).arg( colorComponentsDoubledCount ) ), this ) );
    connect( m_colorHexEditor, SIGNAL(textEdited(QString)), this, SLOT(onColorUpdatedFromHexEditor()) );
    mainLayout->addWidget( m_colorHexEditor, colorComponentsDoubledCount );

    m_colorDisplayPickerTrigger = new QPushButton;
    m_colorDisplayPickerTrigger->setFlat( true );
    connect( m_colorDisplayPickerTrigger, SIGNAL(clicked()), this, SLOT(pickColor()) );
    mainLayout->addWidget( m_colorDisplayPickerTrigger, 0 );

    this->applyColor();
    this->retranslateSelfOnly();
}

QString ColorPicker::colorToHexString( const QColor& aColor )
{
    return QString( "%1%2%3%4" )
           .arg( aColor.red(), 2, 16, QLatin1Char( '0' ) )
           .arg( aColor.green(), 2, 16, QLatin1Char( '0' ) )
           .arg( aColor.blue(), 2, 16, QLatin1Char( '0' ) )
           .arg( aColor.alpha(), 2, 16, QLatin1Char( '0' ) )
           .toUpper();
}

QString ColorPicker::colorToStyleSheetString( const QColor& aColor )
{
    return QString( "rgba(%1, %2, %3, %4%)" )
           .arg( aColor.red() )
           .arg( aColor.green() )
           .arg( aColor.blue() )
           .arg( qRound( qreal( aColor.alpha() ) / ImageUtilities::byteColorComponentMax() * 100.0 ) );
}

void ColorPicker::setColorToComponentsEditor( const QColor& aColor )
{
    for ( int i = 0; i < ImageUtilities::ColorComponentsCount; ++ i )
    {
        QSpinBox* const colorComponentEditor = m_colorComponentsEditors[ i ];
        colorComponentEditor->blockSignals( true );

        int colorComponentValue = 0;
        switch ( i )
        {
            case ImageUtilities::ColorComponentIndexRed:
                colorComponentValue = aColor.red();
                break;
            case ImageUtilities::ColorComponentIndexGreen:
                colorComponentValue = aColor.green();
                break;
            case ImageUtilities::ColorComponentIndexBlue:
                colorComponentValue = aColor.blue();
                break;
            case ImageUtilities::ColorComponentIndexAlpha:
                colorComponentValue = aColor.alpha();
                break;
            default:
                break;
        }

        colorComponentEditor->setValue( colorComponentValue );
        colorComponentEditor->blockSignals( false );
    }
}

QColor ColorPicker::colorFromComponentsEditor() const
{
    return QColor( m_colorComponentsEditors[ ImageUtilities::ColorComponentIndexRed ]->value(),
                   m_colorComponentsEditors[ ImageUtilities::ColorComponentIndexGreen ]->value(),
                   m_colorComponentsEditors[ ImageUtilities::ColorComponentIndexBlue ]->value(),
                   m_colorComponentsEditors[ ImageUtilities::ColorComponentIndexAlpha ]->value() );
}

void ColorPicker::setColorToHexEditor( const QColor& aColor )
{
    m_colorHexEditor->setText( ColorPicker::colorToHexString( aColor ) );
}

QColor ColorPicker::colorFromHexEditor() const
{
    bool converted = false;
    const QRgb color = m_colorHexEditor->text().toUInt( & converted, 16 );
    if ( ! converted )
    {
        return QColor();
    }

    return QColor( qRed( color ),
                   qGreen( color ),
                   qBlue( color ),
                   qAlpha( color ) );
}

void ColorPicker::setColorToPicker( const QColor& aColor )
{
    if ( m_colorPickerDialog != 0 )
    {
        m_colorPickerDialog->setCurrentColor( aColor );
    }
}

QColor ColorPicker::colorFromPicker() const
{
    return m_colorPickerDialog == 0
           ? QColor()
           : m_colorPickerDialog->currentColor();
}

void ColorPicker::applyColor()
{
    this->setColorToComponentsEditor( m_color );
    this->setColorToHexEditor( m_color );
    this->setColorToPicker( m_color );
    this->updateColorDisplay();
}

QColor ColorPicker::color() const
{
    return m_color;
}

void ColorPicker::retranslate()
{
    this->retranslateSelfOnly();
}

void ColorPicker::retranslateSelfOnly()
{
    for ( int i = 0; i < ImageUtilities::ColorComponentsCount; ++ i )
    {
        QString colorComponentDescription;
        switch ( i )
        {
            case ImageUtilities::ColorComponentIndexRed:
                colorComponentDescription = tr( "R:" );
                break;
            case ImageUtilities::ColorComponentIndexGreen:
                colorComponentDescription = tr( "G:" );
                break;
            case ImageUtilities::ColorComponentIndexBlue:
                colorComponentDescription = tr( "B:" );
                break;
            case ImageUtilities::ColorComponentIndexAlpha:
                colorComponentDescription = tr( "A:" );
                break;
            default:
                break;
        }

        m_colorComponentsLabels[ i ]->setText( colorComponentDescription );
    }
}

void ColorPicker::updateColorDisplay()
{
    static const QString styleSheetTemplate( "QPushButton         { background-color: %1; border: 2px solid gray; margin: 2px 2px 2px 2px } "
                                             "QPushButton:hover   { border: 2px solid black; } "
                                             "QPushButton:pressed { margin: 4px 0px 0px 4px; } "
                                             "outline: none" );
    m_colorDisplayPickerTrigger->setStyleSheet( styleSheetTemplate
                                                .arg( ColorPicker::colorToStyleSheetString( m_color ) ) );
}

void ColorPicker::pickColor()
{
    if ( m_colorPickerDialog == nullptr )
    {
        m_colorPickerDialog = new QColorDialog( this );
        connect( m_colorPickerDialog, SIGNAL(colorSelected(QColor)), this, SLOT(onColorUpdatedFromPicker()) );
    }

    if ( m_colorPickerDialog->isVisible() )
    {
        return;
    }

    m_colorPickerDialog->setCurrentColor( m_color );
    m_colorPickerDialog->show();
}

void ColorPicker::onColorUpdatedFromComponentsEditor()
{
    m_color = this->colorFromComponentsEditor();
    this->setColorToHexEditor( m_color );
    this->setColorToPicker( m_color );
    this->updateColorDisplay();

    emit valueChanged();
    emit valueEdited();
}

void ColorPicker::onColorUpdatedFromHexEditor()
{
    m_color = this->colorFromHexEditor();
    this->setColorToComponentsEditor( m_color );
    this->setColorToPicker( m_color );
    this->updateColorDisplay();

    emit valueChanged();
    emit valueEdited();
}

void ColorPicker::onColorUpdatedFromPicker()
{
    m_color = m_colorPickerDialog->currentColor();
    this->setColorToComponentsEditor( m_color );
    this->setColorToHexEditor( m_color );
    this->updateColorDisplay();

    emit valueChanged();
    emit valueEdited();
}
