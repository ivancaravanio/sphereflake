include( ../Build/QmakeHelper/QmakeHelper.pri )

SPHEREFLAKE_VER_MAJ       = 1
SPHEREFLAKE_VER_MIN       = 0
SPHEREFLAKE_VER_PAT       = 0
SPHEREFLAKE_VERSION_PARTS = $$SPHEREFLAKE_VER_MAJ \
                            $$SPHEREFLAKE_VER_MIN \
                            $$SPHEREFLAKE_VER_PAT
SPHEREFLAKE_VERSION       = $$versionString( SPHEREFLAKE_VERSION_PARTS )
