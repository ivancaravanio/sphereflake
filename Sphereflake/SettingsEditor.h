#ifndef SPHEREFLAKE_SETTINGSEDITOR_H
#define SPHEREFLAKE_SETTINGSEDITOR_H

#include "Data/Settings.h"
#include "Utilities/Retranslatable.h"

#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QGroupBox)
QT_FORWARD_DECLARE_CLASS(QPushButton)

namespace Sphereflake {

class EnvironmentSettingsEditor;
class SphereflakeSettingsEditor;

using namespace Sphereflake::Data;
using namespace Sphereflake::Utilities;

class SettingsEditor : public QWidget, public Retranslatable
{
    Q_OBJECT

public:
    explicit SettingsEditor( QWidget* const parent = nullptr, Qt::WindowFlags f = 0 );

    void setSettings( const Settings& aSettings );
    Settings settings() const;

    inline const Settings& previousSettings() const
    {
        return m_previousSettings;
    }

    void retranslate() /* override */;

signals:
    void settingsChanged();

private slots:
    void requestSettingsChange();

private:
    void retranslateSelfOnly();

private:
    Settings                   m_cachedSettings;
    Settings                   m_previousSettings;

    SphereflakeSettingsEditor* m_sphereflakeSettingsEditor;
    QGroupBox*                 m_sphereflakeSettingsEditorGroupBox;
    EnvironmentSettingsEditor* m_environmentSettingsEditor;
    QGroupBox*                 m_environmentSettingsEditorGroupBox;
    QPushButton*               m_applyButton;
};

}

#endif // SPHEREFLAKE_SETTINGSEDITOR_H
