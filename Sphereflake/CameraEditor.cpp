#include "CameraEditor.h"

#include "Vec3Editor.h"

#include <QDoubleSpinBox>
#include <QGridLayout>
#include <QLabel>

using namespace Sphereflake;

CameraEditor::CameraEditor( QWidget* parent, Qt::WindowFlags f )
    : QWidget( parent, f )
    , m_positionLabel( nullptr )
    , m_positionEditor( nullptr )
    , m_directionLabel( nullptr )
    , m_directionEditor( nullptr )
    , m_zoomFactorLabel( nullptr )
    , m_zoomFactorEditor( nullptr )
{
    m_positionLabel = new QLabel;
    m_positionEditor = new Vec3Editor;

    m_directionLabel = new QLabel;
    m_directionEditor = new Vec3Editor;

    m_zoomFactorLabel = new QLabel;
    m_zoomFactorEditor = new QDoubleSpinBox;
    m_zoomFactorEditor->setRange( Camera::s_zoomFactorMin, Camera::s_zoomFactorMax );
    m_zoomFactorEditor->setSingleStep( 0.001 );
    // m_zoomFactorEditor->setDecimals( DBL_MAX_10_EXP + DBL_DIG );
    m_zoomFactorEditor->setDecimals( 3 );

    QGridLayout* const mainLayout = new QGridLayout( this );
    mainLayout->setContentsMargins( QMargins() );
    mainLayout->addWidget( m_positionLabel, 0, 0 );
    mainLayout->addWidget( m_positionEditor, 0, 1 );
    mainLayout->addWidget( m_directionLabel, 1, 0 );
    mainLayout->addWidget( m_directionEditor, 1, 1 );
    mainLayout->addWidget( m_zoomFactorLabel, 2, 0 );
    mainLayout->addWidget( m_zoomFactorEditor, 2, 1 );

    this->setCamera( Camera::defaultCamera() );

    this->retranslateSelfOnly();
}

void CameraEditor::retranslate()
{
    this->retranslateSelfOnly();
}

void CameraEditor::setCamera( const Camera& aCamera )
{
    if ( ! aCamera.isValid() )
    {
        return;
    }

    m_positionEditor->setValue( aCamera.position() );
    m_directionEditor->setValue( aCamera.direction() );
    m_zoomFactorEditor->setValue( aCamera.zoomFactor() );
}

Camera Sphereflake::CameraEditor::camera() const
{
    return Camera(
                m_positionEditor->value(),
                m_directionEditor->value(),
                m_zoomFactorEditor->value() );
}

void CameraEditor::retranslateSelfOnly()
{
    m_positionLabel->setText( tr( "Position:" ) );
    m_directionLabel->setText( tr( "Direction:" ) );
    m_zoomFactorLabel->setText( tr( "Zoom Factor:" ) );
}
