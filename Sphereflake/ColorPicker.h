#ifndef SPHEREFLAKE_COLORPICKER_H
#define SPHEREFLAKE_COLORPICKER_H

#include "Utilities/ImageUtilities.h"
#include "Utilities/Retranslatable.h"

#include <QColor>
#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QColorDialog)
QT_FORWARD_DECLARE_CLASS(QLabel)
QT_FORWARD_DECLARE_CLASS(QLineEdit)
QT_FORWARD_DECLARE_CLASS(QPushButton)
QT_FORWARD_DECLARE_CLASS(QSpinBox)

namespace Sphereflake {

using namespace Sphereflake::Utilities;

class ColorPicker : public QWidget, public Retranslatable
{
    Q_OBJECT

public:
    explicit ColorPicker( QWidget* parent = nullptr );
    explicit ColorPicker( const QColor& aColor, QWidget* parent = nullptr );

    QColor color() const;

    void retranslate() /* override */;

public slots:
    void setColor( const QColor& aColor );

signals:
    void valueChanged();
    void valueEdited();

private:
    void init();
    void retranslateSelfOnly();

    static QString colorToHexString( const QColor& aColor );
    static QString colorToStyleSheetString( const QColor& aColor );

    void setColorToComponentsEditor( const QColor& aColor );
    QColor colorFromComponentsEditor() const;

    void setColorToHexEditor( const QColor& aColor );
    QColor colorFromHexEditor() const;

    void setColorToPicker( const QColor& aColor );
    QColor colorFromPicker() const;

    void applyColor();

    void updateColorDisplay();

private slots:
    void pickColor();
    void onColorUpdatedFromComponentsEditor();
    void onColorUpdatedFromHexEditor();
    void onColorUpdatedFromPicker();

private:
    QColor        m_color;
    QLineEdit*    m_colorHexEditor;

    QLabel*       m_colorComponentsLabels[ ImageUtilities::ColorComponentsCount ];
    QSpinBox*     m_colorComponentsEditors[ ImageUtilities::ColorComponentsCount ];

    QPushButton*  m_colorDisplayPickerTrigger;
    QColorDialog* m_colorPickerDialog;
};

}

#endif // SPHEREFLAKE_COLORPICKER_H
