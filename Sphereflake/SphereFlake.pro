QT += core gui opengl

CONFIG += c++11

macx {
    # Objective-C++ does not recognize C++11 syntax despite the CONFIG += c++11 option
    # https://bugreports.qt.io/browse/QTBUG-39057
    # https://bugreports.qt.io/browse/QTBUG-36575
    # https://codereview.qt-project.org/#/c/122199/

    QMAKE_OBJCXXFLAGS_PRECOMPILE += -std=c++11 -stdlib=libc++
}

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG *= precompile_header

# DEFINES += DRAW_ILLUMINATED_POINTS

PRECOMPILED_HEADER = Sphereflake_precompiled.h

include( ../Build/Global.pri )
include( ../Build/Versions.pri )

TARGET = $$PRODUCT_NAME
TEMPLATE = app

SOURCES += \
    CameraEditor.cpp \
    ColorPicker.cpp \
    ColorsPicker.cpp \
    Data/Camera.cpp \
    Data/EnvironmentSettings.cpp \
    Data/Light.cpp \
    Data/LightTypesModel.cpp \
    Data/LineSegment2D.cpp \
    Data/NumberRange.cpp \
    Data/Polygon2D.cpp \
    Data/Rectangle2D.cpp \
    Data/Settings.cpp \
    Data/ShaderInfo.cpp \
    Data/Size.cpp \
    Data/SphereflakeSettings.cpp \
    Data/Triangle2D.cpp \
    EnvironmentSettingsEditor.cpp \
    LightEditor.cpp \
    main.cpp \
    MainController.cpp \
    MainWindow.cpp \
    SettingsEditor.cpp \
    Sphereflake_namespace.cpp \
    SphereflakeControl.cpp \
    SphereflakeDisplay.cpp \
    SphereflakeSettingsEditor.cpp \
    Utilities/Comparator.cpp \
    Utilities/ChangeListener.cpp \
    Utilities/ChangeNotifier.cpp \
    Utilities/GeneralUtilities.cpp \
    Utilities/ImageUtilities.cpp \
    Utilities/MathUtilities.cpp \
    Utilities/OpenGl/AbstractOpenGlClient.cpp \
    Utilities/OpenGl/ClearColorAlternator.cpp \
    Utilities/OpenGl/FramebufferBinder.cpp \
    Utilities/OpenGl/LineWidthAlternator.cpp \
    Utilities/OpenGl/OpenGlCapabilityEnabler.cpp \
    Utilities/OpenGl/OpenGlErrorLogger.cpp \
    Utilities/OpenGl/OpenGlObjectBinder.cpp \
    Utilities/OpenGl/OpenGlTimeCounter.cpp \
    Utilities/OpenGl/OpenGlUtilities.cpp \
    Utilities/OpenGl/ShaderProgram.cpp \
    Utilities/OpenGl/ShaderProgramActivator.cpp \
    Utilities/OpenGl/ViewportAlternator.cpp \
    Utilities/Retranslatable.cpp \
    Utilities/WidgetUtilities.cpp \
    Vec3Editor.cpp

HEADERS += \
    CameraEditor.h \
    ColorPicker.h \
    ColorsPicker.h \
    Data/Camera.h \
    Data/EnvironmentSettings.h \
    Data/Light.h \
    Data/LightTypesModel.h \
    Data/LineSegment2D.h \
    Data/NumberRange.h \
    Data/Polygon2D.h \
    Data/Rectangle2D.h \
    Data/Settings.h \
    Data/ShaderInfo.h \
    Data/Size.h \
    Data/SphereflakeSettings.h \
    Data/Triangle2D.h \
    Data/Vector2D.h \
    Data/Vector3D.h \
    EnvironmentSettingsEditor.h \
    LightEditor.h \
    MainController.h \
    MainWindow.h \
    SettingsEditor.h \
    Sphereflake_namespace.h \
    SphereflakeControl.h \
    SphereflakeDisplay.h \
    SphereflakeSettingsEditor.h \
    Utilities/Comparator.h \
    Utilities/ChangeListener.h \
    Utilities/ChangeNotifier.h \
    Utilities/GeneralUtilities.h \
    Utilities/ImageUtilities.h \
    Utilities/MathUtilities.h \
    Utilities/Nullable.h \
    Utilities/OpenGl/AbstractOpenGlClient.h \
    Utilities/OpenGl/ClearColorAlternator.h \
    Utilities/OpenGl/FramebufferBinder.h \
    Utilities/OpenGl/LineWidthAlternator.h \
    Utilities/OpenGl/OpenGlCapabilityEnabler.h \
    Utilities/OpenGl/OpenGlErrorLogger.h \
    Utilities/OpenGl/OpenGlObjectBinder.h \
    Utilities/OpenGl/OpenGlTimeCounter.h \
    Utilities/OpenGl/OpenGlUtilities.h \
    Utilities/OpenGl/ShaderProgram.h \
    Utilities/OpenGl/ShaderProgramActivator.h \
    Utilities/OpenGl/ViewportAlternator.h \
    Utilities/Retranslatable.h \
    Utilities/Value.h \
    Utilities/WidgetUtilities.h \
    Vec3Editor.h

RESOURCES += \
    Resources/Resources.qrc

# glew
GLEW_BASE_DIR_PATH = $${PWD}/../ThirdParty/glew
GLEW_HEADERS_DIR_PATH = $${GLEW_BASE_DIR_PATH}/include

INCLUDEPATH += $$GLEW_HEADERS_DIR_PATH

# https://forum.qt.io/topic/5487/building-projects-for-32-and-64-bit-windows-on-64-bit-machine
defineTest(is64BitBuild) {
    equals( QMAKE_TARGET.arch, x64 ) {
        return (true)
    }

    return (false)
}

BITNESS_DIR_NAME =
is64BitBuild() {
    BITNESS_DIR_NAME = x64
} else {
    BITNESS_DIR_NAME = Win32
}

GLEW_BASE_FILE_NAME = glew32

GLEW_STATIC_LIBS_DIR_PATH = $${GLEW_BASE_DIR_PATH}/lib/Release/$${BITNESS_DIR_NAME}
LIBS += -L$$GLEW_STATIC_LIBS_DIR_PATH -l$${GLEW_BASE_FILE_NAME}

VERSION_MAJ = $$SPHEREFLAKE_VER_MAJ
VERSION_MIN = $$SPHEREFLAKE_VER_MIN
VERSION_PAT = $$SPHEREFLAKE_VER_PAT

USED_LIBRARIES =

ICON_COMPLETE_BASE_FILE_PATH = $${PWD}/Resources/Icons/Icon

# SHOULD_DEPLOY = 1

# isDebugBuild() {
#     SHOULD_DEPLOY = 0
# } else {
#     SHOULD_DEPLOY = 1
# }

include( ../Build/Build.pri )

GLEW_DYNAMIC_LIBS_DIR_PATH = $${GLEW_BASE_DIR_PATH}/bin/Release/$${BITNESS_DIR_NAME}
GLEW_DYNAMIC_LIB_FILE_NAME = $$libraryFullBinaryFileName( $$GLEW_BASE_FILE_NAME )

for( DEPLOY_DIR_PATH, DEPLOY_DIR_PATHS ) {
    QMAKE_POST_LINK += $$copyFile( $${GLEW_DYNAMIC_LIBS_DIR_PATH}/$${GLEW_DYNAMIC_LIB_FILE_NAME}, $$DEPLOY_DIR_PATH ) $$CRLF
    QMAKE_CLEAN += $${DEPLOY_DIR_PATH}/$${GLEW_DYNAMIC_LIB_FILE_NAME}
}
