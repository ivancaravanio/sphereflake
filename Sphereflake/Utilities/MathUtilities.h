#ifndef SPHEREFLAKE_UTILITIES_MATHUTILITIES_H
#define SPHEREFLAKE_UTILITIES_MATHUTILITIES_H

#include <QMatrix>
#include <QMatrix4x4>
#include <QPointF>
#include <QtGlobal>
#include <QVector>
#include <QVector3D>

#include <float.h>

// #include <limits>

namespace Sphereflake {
namespace Utilities {

class MathUtilities
{
public:
    enum AxisIndex
    {
        AxisIndexX = 0,
        AxisIndexY = 1,
        AxisIndexZ = 2,
        AxesCount  = 3
    };

public:
    static const int s_axesCount2D;
    static const int s_axesCount3D;
    static const int s_axesCount4D;

    static const double s_invalidLength;

    static const double s_piDegrees;
    static const double s_doublePiDegrees;

    static const QVector3D s_xDir;
    static const QVector3D s_yDir;
    static const QVector3D s_zDir;

public:
    static double degrees( const double& radians );
    static double radians( const double& degrees );
    static double toPositive360DegreesAngle( const double& angleDegrees );
    static double toPositive360DegreesAngleFromRadians( const double& angleRadians );

    static double isFuzzyGreaterOrLessThan( const double& value, const double& referenceValue, const bool greater, const bool equal );
    static double isFuzzyContainedInRegion( const double& minValue, const double& value, const double& maxValue, const bool fully = true );
    static double isFuzzyGreaterThan( const double& value, const double& referenceValue );
    static double isFuzzyGreaterThanOrEqual( const double& value, const double& referenceValue );
    static double isFuzzyLessThan( const double& value, const double& referenceValue );
    static double isFuzzyLessThanOrEqual( const double& value, const double& referenceValue );

    static double sign( const double& num );

    template< int N, int M, typename T >
    static QVector< float > rawMatrix( const QGenericMatrix< N, M, T >& matrix );
    static QVector< float > rawMatrix( const QMatrix4x4& matrix );

    static bool areMatricesEqual( const QMatrix4x4& leftMatrix,
                                  const QMatrix4x4& rightMatrix );
    template< int N, int M, typename T >
    static bool areMatricesEqual( const QGenericMatrix< N, M, T >& leftMatrix,
                                  const QGenericMatrix< N, M, T >& rightMatrix );
    template < typename T >
    static bool areFloatNumberArraysEqual( const T* const leftArray,
                                           const T* const rightArray,
                                           const int size );

    static bool isLengthValid( const double& length, const bool allowZeroLength = true );

    static QMatrix yFlipAboutCenterMatrix( const QPointF& center );

    template < typename T >
    static bool isFuzzyEqual( const T& numLeft, const T& numRight );

    template < typename T >
    static bool isFuzzyZero( const T& num );

    template < typename T >
    static bool isFuzzyFinite( const T& num );

    template < typename T >
    static T epsilon();

    template <>
    static float epsilon< float >();

    template <>
    static double epsilon< double >();

    template < typename T >
    static T cyclicNumber( const T& minNumber, const T& number, const T& maxNumber );

    static int digitsCount( const qint64& value );

    static int randomNumber();
    static int rangeRandomNumber( const int min, const int max );

private:
    static void seedRandomNumberGenerator();

    // do not define:
    // 1. private - compilator error if one tries to instantiate the class from outside
    // 2. only declared but not defined - linker error if one tries to instantiate the class from the inside
    MathUtilities();
};

template< int N, int M, typename T >
bool MathUtilities::areMatricesEqual(
        const QGenericMatrix< N, M, T>& leftMatrix,
        const QGenericMatrix< N, M, T>& rightMatrix )
{
    return MathUtilities::areFloatNumberArraysEqual( leftMatrix.constData(), rightMatrix.constData(), N * M );
}

template < typename T >
bool MathUtilities::areFloatNumberArraysEqual(
        const T* const leftArray,
        const T* const rightArray,
        const int size )
{
    for ( int i = 0; i < size; ++ i )
    {
        if ( ! MathUtilities::isFuzzyEqual( leftArray[ i ], rightArray[ i ] ) )
        {
            return false;
        }
    }

    return true;
}

template< int N, int M, typename T >
QVector< float > MathUtilities::rawMatrix( const QGenericMatrix< N, M, T >& matrix )
{
    const int componentsCount = N * M;
    QVector< float > result;
    result.reserve( componentsCount );

    const T* const matrixRawSource = matrix.constData();
    for ( int i = 0; i < componentsCount; ++ i )
    {
        result.append( matrixRawSource[ i ] );
    }

    return result;
}

template < typename T >
bool MathUtilities::isFuzzyEqual( const T& numLeft, const T& numRight )
{
    // don't; the epsilon is too precise for both single and double precision floating point numbers
    //
    // return abs( numLeft - numRight ) < std::numeric_limits< T >::epsilon();

    // don't:
    //     http://lists.qt-project.org/pipermail/development/2013-July/011920.html
    //     https://bugreports.qt.io/browse/QTBUG-16819
    //
    // return qFuzzyCompare( numLeft, numRight );

    return MathUtilities::isFuzzyZero( numLeft - numRight );
}

template < typename T >
bool MathUtilities::isFuzzyZero( const T& num )
{
    return qAbs( num ) < MathUtilities::epsilon< T >();
}

template < typename T >
bool MathUtilities::isFuzzyFinite( const T& num )
{
    // not available in MSVC10
    // http://www.cplusplus.com/reference/cmath/isfinite/

    return _finite( num ) != 0;
}

template < typename T >
T MathUtilities::epsilon()
{
    return 0;
}

template <>
float MathUtilities::epsilon< float >()
{
    return 0.001f;
}

template <>
double MathUtilities::epsilon< double >()
{
    return 0.000001;
}

template < typename T >
T MathUtilities::cyclicNumber(
        const T& minNumber,
        const T& number,
        const T& maxNumber )
{
    const T rangeSpan = maxNumber - minNumber + 1;
    if ( rangeSpan <= 0 )
    {
        return number;
    }

    const T minLocalNumber       = number - minNumber;
    const T minLocalCyclicNumber = minLocalNumber >= 0
                                   ? minLocalNumber % rangeSpan
                                   : ( rangeSpan - qAbs( minLocalNumber ) % rangeSpan );
    return minLocalCyclicNumber + minNumber;
}

}
}

#endif // SPHEREFLAKE_UTILITIES_MATHUTILITIES_H
