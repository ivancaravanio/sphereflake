#include "ChangeNotifier.h"

#include "ChangeListener.h"

using namespace Sphereflake::Utilities;

const quint64 ChangeNotifier::s_noneTrait = 0ULL;

ChangeNotifier::ChangeNotifier()
    : m_areNotificationsBlocked( false )
{
}

ChangeNotifier::ChangeNotifier( const ChangeNotifier& other )
    // change notification is characteristic uniquely available only for the specific instance
    // : m_listeners( other.m_listeners )
    // , m_areNotificationsBlocked( other.m_areNotificationsBlocked )
{
    Q_UNUSED( other )
}

ChangeNotifier::~ChangeNotifier()
{
}

void ChangeNotifier::addListener( ChangeListener* const listener )
{
    if ( ! this->hasListener( listener ) )
    {
        m_listeners.append( listener );
    }
}

void ChangeNotifier::removeListener( ChangeListener* const listener )
{
    m_listeners.removeAll( listener );
}

void ChangeNotifier::clearListeners()
{
    m_listeners.clear();
}

bool ChangeNotifier::hasListener( ChangeListener* const listener ) const
{
    return m_listeners.contains( listener );
}

int ChangeNotifier::listenersCount() const
{
    return m_listeners.size();
}

void ChangeNotifier::setNotificationsBlocked( const bool aAreNotificationsBlocked )
{
    if ( m_areNotificationsBlocked != aAreNotificationsBlocked )
    {
        m_areNotificationsBlocked = aAreNotificationsBlocked;
    }
}

bool ChangeNotifier::areNotificationsBlocked() const
{
    return m_areNotificationsBlocked;
}

void ChangeNotifier::changed( const quint64& traits )
{
    if ( m_areNotificationsBlocked || traits == s_noneTrait )
    {
        return;
    }

    foreach ( ChangeListener* const listener, m_listeners )
    {
        listener->onChanged( this, traits );
    }
}

quint64 ChangeNotifier::allTraits() const
{
    return s_noneTrait;
}

void ChangeNotifier::notifyAllRolesChanged()
{
    this->changed( this->allTraits() );
}

ChangeNotifier& ChangeNotifier::operator=( const ChangeNotifier& other )
{
    // change notification is characteristic uniquely available only for the specific instance
    // : m_listeners = other.m_listeners;
    // , m_areNotificationsBlocked = other.m_areNotificationsBlocked;

    Q_UNUSED( other )

    return *this;
}
