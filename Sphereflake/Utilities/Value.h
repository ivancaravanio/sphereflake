#ifndef SPHEREFLAKE_UTILITIES_VALUE_H
#define SPHEREFLAKE_UTILITIES_VALUE_H

#include "Comparator.h"

namespace Sphereflake {
namespace Utilities {

template < class T, class Cmp = Comparator< T > >
class Value
{
public:
    Value();
    explicit Value( const T& initialValue, const bool isSet = false );
    Value( const Value< T, Cmp >& value );
    virtual ~Value();

    void setValue( const T& value );

    inline const T& value() const
    {
        return m_value;
    }

    T& rvalue();

    bool isSet() const;
    void unset();
    void unset( const T& value );

    inline operator const T& () const
    {
        return m_value;
    }

    bool operator ==( const Value< T, Cmp >& value ) const;
    bool operator !=( const Value< T, Cmp >& value ) const;

    bool operator ==( const T& value ) const;
    bool operator !=( const T& value ) const;

    Value< T, Cmp >& operator =( const T& value );

private:
    bool m_isSet;
    T    m_value;
};

template < class T, class Cmp > Value< T, Cmp >::Value()
    : m_isSet( false )
{
}

template < class T, class Cmp > Value< T, Cmp >::Value( const T& initialValue, const bool isSet )
    : m_isSet( isSet )
    , m_value( initialValue )
{
}

template < class T, class Cmp > Value< T, Cmp >::Value( const Value< T, Cmp >& value )
    : m_isSet( value.m_isSet )
    , m_value( value.m_value )
{
}

template < class T, class Cmp > Value< T, Cmp >::~Value()
{
}

template < class T, class Cmp > void Value< T, Cmp >::setValue( const T& value )
{
    m_value = value;
    m_isSet = true;
}

template < class T, class Cmp > T& Value< T, Cmp >::rvalue()
{
    m_isSet = true;

    return m_value;
}

template < class T, class Cmp > bool Value< T, Cmp >::isSet() const
{
    return m_isSet;
}

template < class T, class Cmp > void Value< T, Cmp >::unset()
{
    m_isSet = false;
}

template < class T, class Cmp > void Value< T, Cmp >::unset( const T& value )
{
    this->setValue( value );
    this->unset();
}

template < class T, class Cmp > bool Value< T, Cmp >::operator ==( const Value< T, Cmp >& value ) const
{
    if ( m_isSet != value.m_isSet )
    {
        return false;
    }

    return m_isSet
           ? Cmp()( m_value, value.m_value )
           : true;
}

template < class T, class Cmp > bool Value< T, Cmp >::operator !=( const Value< T, Cmp >& value ) const
{
    return !( *this == value );
}

template < class T, class Cmp > bool Value< T, Cmp >::operator ==( const T& value ) const
{
    return Cmp()( m_value, value );
}

template < class T, class Cmp > bool Value< T, Cmp >::operator !=( const T& value ) const
{
    return !( *this == value );
}

template < class T, class Cmp > Value< T, Cmp >& Value< T, Cmp >::operator =( const T& value )
{
    this->setValue( value );

    return *this;
}

}
}

#endif // SPHEREFLAKE_UTILITIES_VALUE_H
