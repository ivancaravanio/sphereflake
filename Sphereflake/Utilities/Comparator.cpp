#include "Comparator.h"

using namespace Sphereflake::Utilities;

bool Comparator< float >::operator()( const float& x, const float& y ) const
{
    return qFuzzyCompare( x, y );
}

bool Comparator< double >::operator()( const double& x, const double& y ) const
{
    return qFuzzyCompare( x, y );
}


bool Comparator< long double >::operator()( const long double& x, const long double& y ) const
{
    return qAbs( x - y ) <= std::numeric_limits< long double >::epsilon();
}
