#ifndef SPHEREFLAKE_UTILITIES_WIDGETUTILITIES_H
#define SPHEREFLAKE_UTILITIES_WIDGETUTILITIES_H

#include <QRect>
#include <QtGlobal>

QT_FORWARD_DECLARE_CLASS(QDesktopWidget)
QT_FORWARD_DECLARE_CLASS(QWidget)

namespace Sphereflake {
namespace Utilities {

class WidgetUtilities
{
public:
    static QWidget* activeOrTopLevelWidget();
    static QRect scaledConcentricCurrentDesktopGeometry(
            const qreal& scale,
            const bool available,
            QWidget* const currentWindow = nullptr );

private:
    // do not define
    WidgetUtilities();
};

}
}

#endif // SPHEREFLAKE_UTILITIES_WIDGETUTILITIES_H
