#ifndef SPHEREFLAKE_UTILITIES_CHANGELISTENER_H
#define SPHEREFLAKE_UTILITIES_CHANGELISTENER_H

#include <QVector>

namespace Sphereflake {
namespace Utilities {

class ChangeNotifier;

class ChangeListener
{
    friend class ChangeNotifier;

public:
    ChangeListener();
    virtual ~ChangeListener();

protected:
    virtual void onChanged( ChangeNotifier* const notifier, const quint64& traits = 0ULL ) = 0;
};

}
}

#endif // SPHEREFLAKE_UTILITIES_CHANGELISTENER_H
