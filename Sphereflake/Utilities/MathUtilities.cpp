#include "MathUtilities.h"

#include <QDateTime>
#include <QMutex>
#include <QMutexLocker>

#include <qmath.h>

#include <math.h>

using namespace Sphereflake::Utilities;

const int    MathUtilities::s_axesCount2D   = 2;
const int    MathUtilities::s_axesCount3D   = 3;
const int    MathUtilities::s_axesCount4D   = 4;
const double MathUtilities::s_invalidLength = -1.0;

const double MathUtilities::s_piDegrees       = 180.0;
const double MathUtilities::s_doublePiDegrees = 2.0 * MathUtilities::s_piDegrees;

const QVector3D MathUtilities::s_xDir( 1.0, 0.0, 0.0 );
const QVector3D MathUtilities::s_yDir( 0.0, 1.0, 0.0 );
const QVector3D MathUtilities::s_zDir( 0.0, 0.0, 1.0 );

double MathUtilities::degrees( const double& radians )
{
    return radians / M_PI * s_piDegrees;
}

double MathUtilities::radians( const double& degrees )
{
    return degrees / s_piDegrees * M_PI;
}

double MathUtilities::toPositive360DegreesAngle( const double& angleDegrees )
{
    double angle360Degrees = angleDegrees;
    const double angleDegreesAbs = qAbs( angleDegrees );
    if ( MathUtilities::isFuzzyGreaterThanOrEqual( angleDegreesAbs, s_doublePiDegrees ) )
    {
        angle360Degrees = static_cast< int >( angleDegreesAbs ) % static_cast< int >( s_doublePiDegrees ) * angleDegrees / angleDegreesAbs;
    }

    return MathUtilities::isFuzzyLessThan( angle360Degrees, 0.0 )
           ? MathUtilities::s_doublePiDegrees + angle360Degrees
           : angle360Degrees;
}

double MathUtilities::toPositive360DegreesAngleFromRadians( const double& angleRadians )
{
    return MathUtilities::toPositive360DegreesAngle( MathUtilities::degrees( angleRadians ) );
}

double MathUtilities::isFuzzyGreaterOrLessThan( const double& value, const double& referenceValue, const bool greater, const bool equal )
{
    return MathUtilities::isFuzzyEqual( value, referenceValue )
           ? equal
           : ( greater
               ? value > referenceValue
               : value < referenceValue );
}

double MathUtilities::isFuzzyContainedInRegion( const double& minValue, const double& value, const double& maxValue, const bool fully )
{
    return    MathUtilities::isFuzzyGreaterOrLessThan( value, minValue, true, fully )
           && MathUtilities::isFuzzyGreaterOrLessThan( value, maxValue, false, fully );
}

double MathUtilities::isFuzzyGreaterThan( const double& value, const double& referenceValue )
{
    return MathUtilities::isFuzzyGreaterOrLessThan( value, referenceValue, true, false );
}

double MathUtilities::isFuzzyGreaterThanOrEqual( const double& value, const double& referenceValue )
{
    return MathUtilities::isFuzzyGreaterOrLessThan( value, referenceValue, true, true );
}

double MathUtilities::isFuzzyLessThan( const double& value, const double& referenceValue )
{
    return MathUtilities::isFuzzyGreaterOrLessThan( value, referenceValue, false, false );
}

double MathUtilities::isFuzzyLessThanOrEqual( const double& value, const double& referenceValue )
{
    return MathUtilities::isFuzzyGreaterOrLessThan( value, referenceValue, false, true );
}

double MathUtilities::sign( const double& num )
{
    return num > 0.0
           ? 1.0
           : -1.0;
}

QVector< float > MathUtilities::rawMatrix( const QMatrix4x4& matrix )
{
    return MathUtilities::rawMatrix( matrix.toGenericMatrix< 4, 4 >() );
}

bool MathUtilities::areMatricesEqual(
        const QMatrix4x4& leftMatrix,
        const QMatrix4x4& rightMatrix )
{
    return MathUtilities::areFloatNumberArraysEqual( leftMatrix.constData(), rightMatrix.constData(), 4 * 4 );
}

bool MathUtilities::isLengthValid( const double& length, const bool allowZeroLength )
{
    return allowZeroLength
           ? MathUtilities::isFuzzyGreaterThanOrEqual( length, 0.0 )
           : MathUtilities::isFuzzyGreaterThan( length, 0.0 );
}

QMatrix MathUtilities::yFlipAboutCenterMatrix( const QPointF& center )
{
    QMatrix m;
    m.translate( center.x(), center.y() );
    m.setMatrix( m.m11(), m.m12(),
                 m.m21(), -m.m22(),
                 m.dx(), m.dy() );
    m.translate( - center.x(), - center.y() );

    return m;
}

int MathUtilities::digitsCount( const qint64& value )
{
    return int( log10( double( qAbs( value ) ) ) ) + 1;
}

void MathUtilities::seedRandomNumberGenerator()
{
#ifdef min
    #undef min
#endif

#ifdef max
    #undef max
#endif

    const QDateTime currentDateTime = QDateTime::currentDateTime();
    const uint seed = static_cast< uint > (
                          MathUtilities::cyclicNumber( static_cast< quint64 >( std::numeric_limits< uint >::min() ),
                                                       static_cast< quint64 >( reinterpret_cast< quintptr >( & currentDateTime ) + currentDateTime.toMSecsSinceEpoch() ),
                                                       static_cast< quint64 >( std::numeric_limits< uint >::max() ) ) );
    qsrand( seed );
}

int MathUtilities::randomNumber()
{
    static QMutex mutex;
    QMutexLocker mutexLocker( & mutex );

    static int generatedNumbersCount = 0;
    if ( ( generatedNumbersCount % 10 ) == 0 )
    {
        MathUtilities::seedRandomNumberGenerator();
    }

    ++ generatedNumbersCount;

    return qrand();
}

int MathUtilities::rangeRandomNumber( const int min, const int max )
{
    if ( min > max
         || min < 0
         || max < 0 )
    {
        return -1;
    }

    return min + MathUtilities::randomNumber() % ( max - min + 1 );
}
