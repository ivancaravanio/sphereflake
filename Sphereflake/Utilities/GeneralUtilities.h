#ifndef SPHEREFLAKE_UTILITIES_GENERALUTILITIES_H
#define SPHEREFLAKE_UTILITIES_GENERALUTILITIES_H

#include <QByteArray>

#ifndef QT_NO_DRAGANDDROP
    QT_FORWARD_DECLARE_CLASS(QMimeData)
#endif

namespace Sphereflake {
namespace Utilities {

class GeneralUtilities
{
public:
    template < typename SourceCollection,
               typename DestinationCollection >
    static DestinationCollection convertedCollection( const SourceCollection& collection );

    static QByteArray fileContents( const QString& fileName );
    static QString toExistingCanonicalFilePath( const QString& filePath );
    static bool areFilePathsEqual( const QString& leftFilePath,
                                   const QString& rightFilePath );

    static bool isCountValid( const int count, const bool allowZero = true );

#ifndef QT_NO_DRAGANDDROP
    static QString filePathFromMimeData( const QMimeData* const mimeData );
#endif

private:
    // do not define:
    GeneralUtilities();
};

template < typename SourceCollection,
           typename DestinationCollection >
static DestinationCollection GeneralUtilities::convertedCollection( const SourceCollection& collection )
{
    DestinationCollection result;

    const int itemsCount = collection.size();
    if ( itemsCount == 0 )
    {
        return result;
    }

    result.reserve( itemsCount );
    for ( int i = 0; i < itemsCount; ++ i )
    {
        result.append( DestinationCollection::value_type( collection[ i ] ) );
    }

    return result;
}

}
}

#endif // SPHEREFLAKE_UTILITIES_GENERALUTILITIES_H
