#ifndef SPHEREFLAKE_UTILITIES_COMPARATOR_H
#define SPHEREFLAKE_UTILITIES_COMPARATOR_H

#include <limits>
#include <QtGlobal>

namespace Sphereflake {
namespace Utilities {

// http://www.cplusplus.com/reference/functional/equal_to/

template < class T > class Comparator
{
public:
    bool operator()( const T& x, const T& y ) const
    {
        return x == y;
    }

    typedef T first_argument_type;
    typedef T second_argument_type;
    typedef bool result_type;
};

template<> class Comparator< float >
{
public:
    bool operator()( const float& x, const float& y ) const;
};

template<> class Comparator< double >
{
public:
    bool operator()( const double& x, const double& y ) const;
};

template<> class Comparator< long double >
{
public:
    bool operator()( const long double& x, const long double& y ) const;
};

}
}

#endif // SPHEREFLAKE_UTILITIES_COMPARATOR_H
