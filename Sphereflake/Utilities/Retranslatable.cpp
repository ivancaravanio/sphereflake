#include "Retranslatable.h"

using namespace Sphereflake::Utilities;

QSet< Retranslatable* > Retranslatable::s_allRetranslatables;

Retranslatable::Retranslatable()
{
    s_allRetranslatables.insert( this );
}

Retranslatable::~Retranslatable()
{
    s_allRetranslatables.remove( this );
}

void Retranslatable::retranslateAll()
{
    foreach ( Retranslatable* const retranslatable, s_allRetranslatables )
    {
        retranslatable->retranslate();
    }
}
