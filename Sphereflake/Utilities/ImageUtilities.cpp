#include "ImageUtilities.h"

#include "MathUtilities.h"

#include <QDateTime>
#include <QHash>
#include <QtGlobal>

#include <limits>

using namespace Sphereflake::Utilities;

int ImageUtilities::byteColorComponentMin()
{
#ifdef min
    #undef min
#endif

    return std::numeric_limits< quint8 >::min();
}

int ImageUtilities::byteColorComponentMax()
{
#ifdef max
    #undef max
#endif
    return std::numeric_limits< quint8 >::max();
}

QColor ImageUtilities::oppositeColor( const QColor& color )
{
    return QColor::fromHsl( qRound( MathUtilities::toPositive360DegreesAngle( color.hue() + 180.0 ) ),
                            color.saturation(),
                            color.lightness(),
                            color.alpha() );
}

int ImageUtilities::randomColorComponent()
{
    return MathUtilities::cyclicNumber(
                ImageUtilities::byteColorComponentMin(),
                MathUtilities::randomNumber(),
                ImageUtilities::byteColorComponentMax() );

}

QColor ImageUtilities::randomColor()
{
    return QColor( ImageUtilities::randomColorComponent(),
                   ImageUtilities::randomColorComponent(),
                   ImageUtilities::randomColorComponent() );
}

QColor ImageUtilities::randomDifferentColor( const QSet< QColor >& colors )
{
    forever
    {
        const QColor randomColor = ImageUtilities::randomColor();
        if ( ! colors.contains( randomColor ) )
        {
            return randomColor;
        }
    }

    // intentional lack of return statement in the end of the function
    // it would be unreachable code
}

QString ImageUtilities::qssColor( const QColor& color )
{
    return QString( "rgba( %1, %2, %3, %4% )" )
           .arg( color.red() )
           .arg( color.green() )
           .arg( color.blue() )
           .arg( qRound( color.alphaF() * 100.0 ) );
}

QT_BEGIN_NAMESPACE
uint qHash( const QColor& color )
{
    return static_cast< int >( color.spec() )
           ^ color.red()
           ^ color.green()
           ^ color.blue()
           ^ color.alpha();
}
QT_END_NAMESPACE
