#ifndef SPHEREFLAKE_UTILITIES_IMAGEUTILITIES_H
#define SPHEREFLAKE_UTILITIES_IMAGEUTILITIES_H

#include <QColor>
#include <QSet>

QT_BEGIN_NAMESPACE
uint qHash( const QColor& color );
QT_END_NAMESPACE

namespace Sphereflake {
namespace Utilities {

class ImageUtilities
{
public:
    enum ColorComponentIndex
    {
        ColorComponentIndexInvalid = -1,
        ColorComponentIndexRed     = 0,
        ColorComponentIndexGreen   = 1,
        ColorComponentIndexBlue    = 2,
        ColorComponentIndexAlpha   = 3,
        ColorComponentsCount       = 4
    };

public:
    static int byteColorComponentMin();
    static int byteColorComponentMax();
    static QColor oppositeColor( const QColor& color );

    static int randomColorComponent();
    static QColor randomColor();
    static QColor randomDifferentColor( const QSet< QColor >& colors );
    static QString qssColor( const QColor& color );

private:
    // do not define:
    // 1. private - compilator error if one tries to instantiate the class from outside
    // 2. only declared but not defined - linker error if one tries to instantiate the class from the inside
    ImageUtilities();
};

}
}

#endif // SPHEREFLAKE_UTILITIES_IMAGEUTILITIES_H
