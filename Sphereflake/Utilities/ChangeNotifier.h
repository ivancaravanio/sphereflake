#ifndef SPHEREFLAKE_UTILITIES_CHANGENOTIFIER_H
#define SPHEREFLAKE_UTILITIES_CHANGENOTIFIER_H

#include <QList>
#include <QtGlobal>

namespace Sphereflake {
namespace Utilities {

class ChangeListener;

class ChangeNotifier
{
public:
    static const quint64 s_noneTrait;

public:
    ChangeNotifier();
    ChangeNotifier( const ChangeNotifier& other );
    virtual ~ChangeNotifier();

    void addListener( ChangeListener* const listener );
    void removeListener( ChangeListener* const listener );
    void clearListeners();
    bool hasListener( ChangeListener* const listener ) const;
    int listenersCount() const;

    virtual void setNotificationsBlocked( const bool aAreNotificationsBlocked );
    bool areNotificationsBlocked() const;

    void changed( const quint64& aTraits );
    virtual quint64 allTraits() const;
    virtual void notifyAllRolesChanged();

    ChangeNotifier& operator=( const ChangeNotifier& other );

private:
    QList< ChangeListener* > m_listeners;
    bool                     m_areNotificationsBlocked;
};

}
}

#endif // SPHEREFLAKE_UTILITIES_CHANGENOTIFIER_H
