#ifndef SPHEREFLAKE_UTILITIES_OPENGL_OPENGLCAPABILITYENABLER_H
#define SPHEREFLAKE_UTILITIES_OPENGL_OPENGLCAPABILITYENABLER_H

// glew.h should be included prior to including gl.h
#include <GL/glew.h>

namespace Sphereflake {
namespace Utilities {
namespace OpenGl {

class OpenGlCapabilityEnabler
{
public:
    OpenGlCapabilityEnabler();
    OpenGlCapabilityEnabler(
            const GLenum aCapability,
            const bool aShouldEnable,
            const bool aAutoRollback = true );
    ~OpenGlCapabilityEnabler();

    bool isValid() const;

    GLenum capability() const;
    bool shouldEnable() const;
    bool isEnabled() const;

    void change();
    void rollback();

private:
    void change( const bool shouldEnable );

private:
    GLenum m_capability;
    bool   m_shouldEnable;
    bool   m_wasChanged;
    bool   m_autoRollback;
};

}
}
}

#endif // SPHEREFLAKE_UTILITIES_OPENGL_OPENGLCAPABILITYENABLER_H
