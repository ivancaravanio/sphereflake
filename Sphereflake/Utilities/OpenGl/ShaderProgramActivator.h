#ifndef SPHEREFLAKE_UTILITIES_OPENGL_SHADERPROGRAMACTIVATOR_H
#define SPHEREFLAKE_UTILITIES_OPENGL_SHADERPROGRAMACTIVATOR_H

// glew.h should be included prior to including gl.h
#include <GL/glew.h>

#include <QGLFormat>
#include <QtGlobal>

namespace Sphereflake {
namespace Utilities {
namespace OpenGl {

class ShaderProgramActivator
{
public:
    ShaderProgramActivator();
    ShaderProgramActivator(
            const QGLFormat& aOpenGlFormat,
            const GLuint aShaderProgramId );
    ~ShaderProgramActivator();

    bool isValid() const;

    GLuint activeShaderProgramId() const;

    void activate();
    void deactivate();

private:
    QGLFormat m_openGlFormat;
    GLuint    m_shaderProgramId;
    GLuint    m_previousShaderProgramId;
};

}
}
}

#endif // SPHEREFLAKE_UTILITIES_OPENGL_SHADERPROGRAMACTIVATOR_H
