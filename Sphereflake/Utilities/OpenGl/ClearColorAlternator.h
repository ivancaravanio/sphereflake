#ifndef SPHEREFLAKE_UTILITIES_OPENGL_CLEARCOLORALTERNATOR_H
#define SPHEREFLAKE_UTILITIES_OPENGL_CLEARCOLORALTERNATOR_H

#include <QColor>

namespace Sphereflake {
namespace Utilities {
namespace OpenGl {

class ClearColorAlternator
{
public:
    ClearColorAlternator();
    ClearColorAlternator(
            const QColor& aRequestedClearColor,
            const bool aAutoRollback = true );
    ~ClearColorAlternator();

    bool isValid() const;

    const QColor& requestedClearColor() const;

    QColor currentClearColor() const;

    void change();
    void rollback();

private:
    void change( const QColor& clearColor );

private:
    QColor m_requestedClearColor;
    QColor m_previousClearColor;
    bool m_autoRollback;
};

}
}
}

#endif // SPHEREFLAKE_UTILITIES_OPENGL_CLEARCOLORALTERNATOR_H
