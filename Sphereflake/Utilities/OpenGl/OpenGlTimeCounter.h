#ifndef SPHEREFLAKE_UTILITIES_OPENGL_OPENGLTIMECOUNTER_H
#define SPHEREFLAKE_UTILITIES_OPENGL_OPENGLTIMECOUNTER_H

// glew.h should be included prior to including gl.h
#include <GL/glew.h>

#include <QtGlobal>
#include <QVector>

namespace Sphereflake {
namespace Utilities {
namespace OpenGl {

class OpenGlTimeCounter
{
public:
    OpenGlTimeCounter();
    ~OpenGlTimeCounter();

    void start();
    void stop();
    bool isRunning() const;

    GLuint64 timeNanosecs() const;
    qreal timeMillisecs() const;

    void dumpTime() const;

private:
    enum TimerQueryIndex
    {
        TimerQueryIndexStart = 0,
        TimerQueryIndexEnd   = 1,
        TimerQueriesCount    = 2
    };

private:
    QVector< GLuint > m_timerQueries;
    GLuint64          m_timeNanosecs;
    bool              m_isRunning;
};

}
}
}

#endif // SPHEREFLAKE_UTILITIES_OPENGL_OPENGLTIMECOUNTER_H
