#ifndef SPHEREFLAKE_UTILITIES_OPENGL_LINEWIDTHALTERNATOR_H
#define SPHEREFLAKE_UTILITIES_OPENGL_LINEWIDTHALTERNATOR_H

namespace Sphereflake {
namespace Utilities {
namespace OpenGl {

class LineWidthAlternator
{
public:
    LineWidthAlternator();
    LineWidthAlternator(
            const float aRequestedLineWidth,
            const bool aAutoRollback = true );
    ~LineWidthAlternator();

    bool isValid() const;

    float requestedLineWidth() const;
    float currentLineWidth() const;

    void change();
    void rollback();

private:
    void change( const float aLineWidth );
    void setRequestedLineWidth( const float aRequestedLineWidth );

private:
    float m_requestedLineWidth;
    float m_previousLineWidth;
    bool  m_autoRollback;
};

}
}
}

#endif // SPHEREFLAKE_UTILITIES_OPENGL_LINEWIDTHALTERNATOR_H
