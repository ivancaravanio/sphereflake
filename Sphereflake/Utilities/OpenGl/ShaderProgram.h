#ifndef SPHEREFLAKE_UTILITIES_OPENGL_SHADERPROGRAM_H
#define SPHEREFLAKE_UTILITIES_OPENGL_SHADERPROGRAM_H

// glew.h should be included prior to including gl.h
#include <GL/glew.h>

#include "../../Data/ShaderInfo.h"
#include "../../Data/Vector3D.h"
#include "../MathUtilities.h"
#include "OpenGlUtilities.h"
#include "ShaderProgramActivator.h"

#include <QColor>
#include <QGenericMatrix>
#include <QGLFormat>
#include <QList>
#include <QMatrix4x4>
#include <QtGlobal>

namespace Sphereflake {
namespace Utilities {
namespace OpenGl {

using namespace Sphereflake::Data;

class ShaderProgram
{
public:
    ShaderProgram();
    ShaderProgram(
            const QGLFormat& aOpenGlFormat,
            const QList< ShaderInfo >& aShaders = QList< ShaderInfo >() );
    ~ShaderProgram();

    void setOpenGLFormat( const QGLFormat& aOpenGlFormat );
    void appendShader( const ShaderInfo& shader );
    void setShaders( const QList< ShaderInfo >& aShaders );

    bool build();
    bool build( const QString& vertexShaderFilePath,
                const QString& fragmentShaderFilePath );

    static GLuint id( const QGLFormat& aOpenGlFormat );
    GLuint id() const;
    bool isValid() const;

    void activate();
    void deactivate();
    bool isActive() const;

    GLuint activeShaderProgramId() const;

    void clear();

    // Shader compilers are generally pretty aggressive about optimizing away compile-time constants in any form,
    // and a good mature GLSL compiler will have no trouble with this.
    // The variable should be an active one = existing and not optimized-out.
    GLint genericVertexAttributeVarLocation( const QByteArray& name ) const;
    GLint fragmentOutputVarLocation( const QByteArray& name ) const;
    GLint uniformVarLocation( const QByteArray& name ) const;

    void setUniformVariable1f( const GLint location, const GLfloat v0 );
    void setUniformVariable2f( const GLint location, const GLfloat v0, const GLfloat v1 );
    void setUniformVariable3f( const GLint location, const GLfloat v0, const GLfloat v1, const GLfloat v2 );
    void setUniformVariable4f( const GLint location, const GLfloat v0, const GLfloat v1, const GLfloat v2, const GLfloat v3 );

    // https://www.opengl.org/wiki/GLSL_:_common_mistakes#How_to_use_glUniform
    void setUniformVariable1fv( const GLint location, const GLsizei count, const GLfloat* const value );
    void setUniformVariable2fv( const GLint location, const GLsizei count, const GLfloat* const value );
    void setUniformVariable3fv( const GLint location, const GLsizei count, const GLfloat* const value );
    void setUniformVariable4fv( const GLint location, const GLsizei count, const GLfloat* const value );

    void setUniformVariable1i( const GLint location, const GLint v0 );
    void setUniformVariable2i( const GLint location, const GLint v0, const GLint v1 );
    void setUniformVariable3i( const GLint location, const GLint v0, const GLint v1, const GLint v2 );
    void setUniformVariable4i( const GLint location, const GLint v0, const GLint v1, const GLint v2, const GLint v3 );

    void setUniformVariable1iv( const GLint location, const GLsizei count, const GLint* const value );
    void setUniformVariable2iv( const GLint location, const GLsizei count, const GLint* const value );
    void setUniformVariable3iv( const GLint location, const GLsizei count, const GLint* const value );
    void setUniformVariable4iv( const GLint location, const GLsizei count, const GLint* const value );

    template< int N, int M, typename T >
    void setUniformMatrix( const GLint location, const QGenericMatrix< N, M, T >& matrix, const GLboolean transpose = GL_FALSE );
    void setUniformMatrix( const GLint location, const QMatrix4x4& matrix, const GLboolean transpose = GL_FALSE );

    /*
     * https://www.opengl.org/sdk/docs/man/html/glUniform.xhtml
     * -----
     * If transpose is GL_FALSE, each matrix is assumed to be supplied in column major order.
     * If transpose is GL_TRUE, each matrix is assumed to be supplied in row major order.
     * The count argument indicates the number of matrices to be passed. A count of 1 should be used if modifying the value of a single matrix, and a count greater than 1 can be used to modify an array of matrices.
     */
    void setUniformMatrix2fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value );
    void setUniformMatrix3fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value );
    void setUniformMatrix4fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value );

    void setUniformMatrix2x3fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value );
    void setUniformMatrix3x2fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value );
    void setUniformMatrix2x4fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value );
    void setUniformMatrix4x2fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value );
    void setUniformMatrix3x4fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value );
    void setUniformMatrix4x3fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value );

    void setUniformColor( const GLint location,
                          const QColor& aColor,
                          const bool applyAlpha = true );
    void setUniformVector( const GLint location,
                           const Vector2Df& aVector );
    void setUniformVector( const GLint location,
                           const Vector3Df& aVector );

    void dumpPostLinkInfo();

private:
    static void cleanShaders(
            const QList< GLuint >& shaderIds,
            const GLuint shaderProgramId = OpenGlUtilities::s_invalidUnsignedGlValue );
    static QString shaderVariableTypeDescription( const GLenum shaderVariableType );

private:
    QGLFormat m_openGlFormat;
    QList< ShaderInfo > m_shaders;

    GLuint m_id;
    ShaderProgramActivator m_activator;
};

template< int N, int M, typename T >
void ShaderProgram::setUniformMatrix(
        const GLint location,
        const QGenericMatrix< N, M, T >& matrix,
        const GLboolean transpose )
{
    const QVector< float > rawMatrix = MathUtilities::rawMatrix( matrix );
    typedef void ( ShaderProgram::*MatrixSetter )( const GLint, const GLsizei, const GLboolean, const GLfloat* const );

    MatrixSetter matrixSetter = nullptr;
    switch ( N )
    {
        case 2:
            switch ( M )
            {
                case 2: matrixSetter = & ShaderProgram::setUniformMatrix2fv; break;
                case 3: matrixSetter = & ShaderProgram::setUniformMatrix2x3fv; break;
                case 4: matrixSetter = & ShaderProgram::setUniformMatrix2x4fv; break;
            }

            break;
        case 3:
            switch ( M )
            {
                case 2: matrixSetter = & ShaderProgram::setUniformMatrix3x2fv; break;
                case 3: matrixSetter = & ShaderProgram::setUniformMatrix3fv; break;
                case 4: matrixSetter = & ShaderProgram::setUniformMatrix3x4fv; break;
            }

            break;
        case 4:
            switch ( M )
            {
                case 2: matrixSetter = & ShaderProgram::setUniformMatrix4x2fv; break;
                case 3: matrixSetter = & ShaderProgram::setUniformMatrix4x3fv; break;
                case 4: matrixSetter = & ShaderProgram::setUniformMatrix4fv; break;
            }

            break;
    }

    if ( matrixSetter != nullptr )
    {
        (this->*matrixSetter)( location, 1, transpose, rawMatrix.constData() );
    }
}

}
}
}

#endif // SPHEREFLAKE_UTILITIES_OPENGL_SHADERPROGRAM_H
