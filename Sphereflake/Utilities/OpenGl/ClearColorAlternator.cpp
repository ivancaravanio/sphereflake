#include "ClearColorAlternator.h"

// glew.h should be included prior to including gl.h
#include <GL/glew.h>

#include "../ImageUtilities.h"
#include "OpenGlErrorLogger.h"

using namespace Sphereflake::Utilities::OpenGl;

ClearColorAlternator::ClearColorAlternator()
    : m_autoRollback( false )
{
}

ClearColorAlternator::ClearColorAlternator(
        const QColor& aRequestedClearColor,
        const bool aAutoRollback )
    : m_requestedClearColor( aRequestedClearColor )
    , m_autoRollback( aAutoRollback )
{
}

ClearColorAlternator::~ClearColorAlternator()
{
    if ( m_autoRollback )
    {
        this->rollback();
    }
}

bool ClearColorAlternator::isValid() const
{
    return m_requestedClearColor.isValid();
}

const QColor& ClearColorAlternator::requestedClearColor() const
{
    return m_requestedClearColor;
}

QColor ClearColorAlternator::currentClearColor() const
{
    GLfloat color[ ImageUtilities::ColorComponentsCount ] = { 0.0f };

    glGetFloatv( GL_COLOR_CLEAR_VALUE, color );
    LOG_OPENGL_ERROR();

    return QColor::fromRgbF( color[ ImageUtilities::ColorComponentIndexRed   ],
                             color[ ImageUtilities::ColorComponentIndexGreen ],
                             color[ ImageUtilities::ColorComponentIndexBlue  ],
                             color[ ImageUtilities::ColorComponentIndexAlpha ] );
}

void ClearColorAlternator::change( const QColor& clearColor )
{
    glClearColor( clearColor.redF(),
                  clearColor.greenF(),
                  clearColor.blueF(),
                  clearColor.alphaF() );
    LOG_OPENGL_ERROR();
}

void ClearColorAlternator::change()
{
    if ( ! this->isValid() )
    {
        return;
    }

    const QColor currentClearColor = this->currentClearColor();
    if ( currentClearColor == m_requestedClearColor )
    {
        return;
    }

    m_previousClearColor = currentClearColor;
    this->change( m_requestedClearColor );
}

void ClearColorAlternator::rollback()
{
    if ( ! m_previousClearColor.isValid() )
    {
        return;
    }

    this->change( m_previousClearColor );

    m_previousClearColor = QColor();
}
