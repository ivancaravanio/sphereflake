#ifndef SPHEREFLAKE_UTILITIES_RETRANSLATABLE_H
#define SPHEREFLAKE_UTILITIES_RETRANSLATABLE_H

#include <QSet>

namespace Sphereflake {
namespace Utilities {

class Retranslatable
{
public:
    Retranslatable();
    virtual ~Retranslatable();

    virtual void retranslate() = 0;
    static void retranslateAll();

private:
    static QSet< Retranslatable* > s_allRetranslatables;
};

}
}

#endif // SPHEREFLAKE_UTILITIES_RETRANSLATABLE_H
