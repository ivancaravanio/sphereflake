#include "MainController.h"

#include <QApplication>
#include <QStringList>

using namespace Sphereflake;

int main( int argc, char* argv[] )
{
    QApplication a( argc, argv );
    QApplication::setApplicationVersion( VERSION_STRING );

    MainController controller;
    controller.init();

    return a.exec();
}
