#ifndef SPHEREFLAKE_VEC3EDITOR_H
#define SPHEREFLAKE_VEC3EDITOR_H

#include "Data/Vector3D.h"
#include "Utilities/MathUtilities.h"
#include "Utilities/Retranslatable.h"

#include <QPair>
#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QDoubleSpinBox)
QT_FORWARD_DECLARE_CLASS(QLabel)

namespace Sphereflake {

using namespace Sphereflake::Data;
using namespace Sphereflake::Utilities;

class Vec3Editor : public QWidget, public Retranslatable
{
    Q_OBJECT

public:
    static const Vector3Dd s_minDefault;
    static const Vector3Dd s_maxDefault;

public:
    explicit Vec3Editor( QWidget* parent = 0 );

    void setMinimum( const Vector3Dd& aMinimum );
    Vector3Dd minimum() const;

    void setMaximum( const Vector3Dd& aMaximum );
    Vector3Dd maximum() const;

    void setRange( const Vector3Dd& aMinimum, const Vector3Dd& aMaximum );

    void setSingleStep( const double& aSingleStep );
    double singleStep() const;

    void setDecimalsCount( const int aDecimalsCount );
    int decimalsCount() const;

    void setValue( const Vector3Dd& aValue );
    Vector3Dd value() const;

    void retranslate() /* override */;

signals:
    void valueChanged();
    void valueEdited();

private:
    void retranslateSelfOnly();

private:
    QPair< QLabel*, QDoubleSpinBox* > m_axisLabelEditors[ MathUtilities::AxesCount ];
};

}

#endif // SPHEREFLAKE_VEC3EDITOR_H
