#include "Vec3Editor.h"

#include "Utilities/MathUtilities.h"

#include <QDoubleSpinBox>
#include <QHBoxLayout>
#include <QLabel>

using namespace Sphereflake;

const Vector3Dd Vec3Editor::s_minDefault( -100.0 );
const Vector3Dd Vec3Editor::s_maxDefault( 100.0 );

Vec3Editor::Vec3Editor( QWidget* parent )
    : QWidget( parent )
{
    QHBoxLayout* const mainLayout = new QHBoxLayout( this );
    mainLayout->setContentsMargins( QMargins() );
    for ( int i = 0; i < MathUtilities::AxesCount; ++ i )
    {
        QLabel* const axisLabel = new QLabel;

        QDoubleSpinBox* const axisEditor = new QDoubleSpinBox;
        axisEditor->setAccelerated( true );
        axisEditor->setDecimals( 3 );
        connect( axisEditor, SIGNAL(valueChanged(double)), this, SIGNAL(valueChanged()) );
        connect( axisEditor, SIGNAL(valueChanged(double)), this, SIGNAL(valueEdited()) );

        QPair< QLabel*, QDoubleSpinBox* >& axisLabelEditor = m_axisLabelEditors[ i ];
        axisLabelEditor.first = axisLabel;
        axisLabelEditor.second = axisEditor;

        mainLayout->addWidget( axisLabel, 0 );
        mainLayout->addWidget( axisEditor, 1 );
    }

    this->setRange( s_minDefault, s_maxDefault );
    this->retranslateSelfOnly();
}

void Vec3Editor::setMinimum( const Vector3Dd& aMinimum )
{
    if ( aMinimum > this->maximum()
         || aMinimum == this->minimum() )
    {
        return;
    }

    bool notifyValueChanged = false;
    for ( int i = 0; i < MathUtilities::AxesCount; ++ i )
    {
        QDoubleSpinBox* const axisEditor = m_axisLabelEditors[ i ].second;
        const double axisEditorPreviousValue = axisEditor->value();
        axisEditor->blockSignals( true );
        axisEditor->setMinimum( aMinimum[ i ] );
        axisEditor->blockSignals( false );

        if ( ! MathUtilities::isFuzzyEqual( axisEditorPreviousValue, axisEditor->value() ) )
        {
            notifyValueChanged = true;
        }
    }

    if ( notifyValueChanged )
    {
        emit valueChanged();
    }
}

Vector3Dd Vec3Editor::minimum() const
{
    return Vector3Dd( m_axisLabelEditors[ MathUtilities::AxisIndexX ].second->minimum(),
                      m_axisLabelEditors[ MathUtilities::AxisIndexY ].second->minimum(),
                      m_axisLabelEditors[ MathUtilities::AxisIndexZ ].second->minimum() );
}

void Vec3Editor::setMaximum( const Vector3Dd& aMaximum )
{
    if ( aMaximum < this->minimum()
         || aMaximum == this->maximum() )
    {
        return;
    }

    bool notifyValueChanged = false;
    for ( int i = 0; i < MathUtilities::AxesCount; ++ i )
    {
        QDoubleSpinBox* const axisEditor = m_axisLabelEditors[ i ].second;
        const double axisEditorPreviousValue = axisEditor->value();
        axisEditor->blockSignals( true );
        axisEditor->setMaximum( aMaximum[ i ] );
        axisEditor->blockSignals( false );

        if ( ! MathUtilities::isFuzzyEqual( axisEditorPreviousValue, axisEditor->value() ) )
        {
            notifyValueChanged = true;
        }
    }

    if ( notifyValueChanged )
    {
        emit valueChanged();
    }
}

Vector3Dd Vec3Editor::maximum() const
{
    return Vector3Dd( m_axisLabelEditors[ MathUtilities::AxisIndexX ].second->maximum(),
                      m_axisLabelEditors[ MathUtilities::AxisIndexY ].second->maximum(),
                      m_axisLabelEditors[ MathUtilities::AxisIndexZ ].second->maximum() );
}

void Vec3Editor::setRange( const Vector3Dd& aMinimum, const Vector3Dd& aMaximum )
{
    if ( aMinimum > aMaximum
         || ( aMinimum == this->minimum()
              && aMaximum == this->maximum() ) )
    {
        return;
    }

    bool notifyValueChanged = false;
    for ( int i = 0; i < MathUtilities::AxesCount; ++ i )
    {
        QDoubleSpinBox* const axisEditor = m_axisLabelEditors[ i ].second;
        const double axisEditorPreviousValue = axisEditor->value();
        axisEditor->blockSignals( true );
        axisEditor->setRange( aMinimum[ i ], aMaximum[ i ] );
        axisEditor->blockSignals( false );

        if ( ! MathUtilities::isFuzzyEqual( axisEditorPreviousValue, axisEditor->value() ) )
        {
            notifyValueChanged = true;
        }
    }

    if ( notifyValueChanged )
    {
        emit valueChanged();
    }
}

void Vec3Editor::setSingleStep( const double& aSingleStep )
{
    for ( int i = 0; i < MathUtilities::AxesCount; ++ i )
    {
        m_axisLabelEditors[ i ].second->setSingleStep( aSingleStep );
    }
}

double Sphereflake::Vec3Editor::singleStep() const
{
    // no matter x, y or z; they should have equal single steps
    return m_axisLabelEditors[ MathUtilities::AxisIndexX ].second->singleStep();
}

void Vec3Editor::setDecimalsCount( const int aDecimalsCount )
{
    bool notifyValueChanged = false;
    for ( int i = 0; i < MathUtilities::AxesCount; ++ i )
    {
        QDoubleSpinBox* const axisEditor = m_axisLabelEditors[ i ].second;
        const double axisEditorPreviousValue = axisEditor->value();
        axisEditor->blockSignals( true );
        axisEditor->setDecimals( aDecimalsCount );
        axisEditor->blockSignals( false );

        if ( ! qFuzzyCompare( axisEditorPreviousValue, axisEditor->value() ) )
        {
            notifyValueChanged = true;
        }
    }

    if ( notifyValueChanged )
    {
        emit valueChanged();
    }
}

int Vec3Editor::decimalsCount() const
{
    return m_axisLabelEditors[ MathUtilities::AxisIndexX ].second->decimals();
}

void Vec3Editor::setValue( const Vector3Dd& aValue )
{
    // NOTE: use this negative check since if the new value is not lower than
    // the minimum acceptable value it could also be not greater or equal to it
    if ( ! ( this->minimum() <= aValue && aValue <= this->maximum() ) )
    {
        return;
    }

    const Vector3Dd previousValue = this->value();
    if ( aValue == previousValue )
    {
        return;
    }

    for ( int i = 0; i < MathUtilities::AxesCount; ++ i )
    {
        QDoubleSpinBox* const axisEditor = m_axisLabelEditors[ i ].second;
        axisEditor->blockSignals( true );
        axisEditor->setValue( aValue[ i ] );
        axisEditor->blockSignals( false );
    }

    if ( this->value() != previousValue )
    {
        emit valueChanged();
    }
}

Vector3Dd Vec3Editor::value() const
{
    return Vector3Dd( m_axisLabelEditors[ MathUtilities::AxisIndexX ].second->value(),
                      m_axisLabelEditors[ MathUtilities::AxisIndexY ].second->value(),
                      m_axisLabelEditors[ MathUtilities::AxisIndexZ ].second->value() );
}

void Vec3Editor::retranslate()
{
    this->retranslateSelfOnly();
}

void Vec3Editor::retranslateSelfOnly()
{
    for ( int i = 0; i < MathUtilities::AxesCount; ++ i )
    {
        QString labelText;
        switch ( i )
        {
            case MathUtilities::AxisIndexX:
                labelText = tr( "x:" );
                break;
            case MathUtilities::AxisIndexY:
                labelText = tr( "y:" );
                break;
            case MathUtilities::AxisIndexZ:
                labelText = tr( "z:" );
                break;
            default:
                break;
        }

        m_axisLabelEditors[ i ].first->setText( labelText );
    }
}
