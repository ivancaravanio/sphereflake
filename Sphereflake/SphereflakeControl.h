#ifndef SPHEREFLAKE_SPHEREFLAKECONTROL_H
#define SPHEREFLAKE_SPHEREFLAKECONTROL_H

#include <QList>
#include <QPair>
#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QFrame)
QT_FORWARD_DECLARE_CLASS(QSplitter)

namespace Sphereflake {

class SphereflakeDisplay;
class SettingsEditor;

class SphereflakeControl : public QWidget
{
    Q_OBJECT

public:
    explicit SphereflakeControl( QWidget* const parent = nullptr );

protected:
    void showEvent( QShowEvent* event ) /* override */;

private slots:
    void applySettingsToDisplay();
    void applySettingsFromDisplay();

    void updateSphereflakeDisplayFocusFrame();

private:
    void retranslateSelfOnly();

private:
    SphereflakeDisplay* m_sphereflakeDisplay;
    QFrame*             m_sphereflakeDisplayFocusFrame;
    SettingsEditor*     m_settingsEditor;
    QSplitter*          m_splitter;
};

}

#endif // SPHEREFLAKE_SPHEREFLAKECONTROL_H
