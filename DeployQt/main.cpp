#include <QtGlobal>

#ifdef Q_OS_WIN
#include "Deployer.h"
#include "Logger.h"

#include <QCoreApplication>
#include <QDir>
#include <QFileInfo>
#include <QString>
#include <QStringList>
#include <QTextStream>
#endif

int main( int argc, char* argv[] )
{
#ifdef Q_OS_WIN
    using namespace DeployQt;

    QCoreApplication app( argc, argv );
    if ( argc <= 1 )
    {
        QString usageText;
        QTextStream usageTextStream( &usageText, QIODevice::WriteOnly );
        usageTextStream << "Deploys Windows Qt-library based modules - applications, dynamic-link libraries." << endl
                        << "Usage: DeployQt module_path [/i dir_path1[;dir_path2][;dir_path1]...]" << endl
                        << "/i\tSpecify a semicolon-separated list of additional" << endl
                        << "\tdynamic-link library search directory paths.";

        Logger::log( usageText );

        app.exit( EXIT_SUCCESS );
        return EXIT_SUCCESS;
    }

    enum ExpectedArgumentType
    {
        ExpectedArgumentTypeTargetModulePath,
        ExpectedArgumentTypeOptionalParameters
    };

    enum ArgumentSwitchType
    {
        ArgumentSwitchTypeNone,
        ArgumentSwitchTypeAddLibDirPaths
    };

    QString targetModulePath;
    QStringList addLibDirPaths;

    ArgumentSwitchType detectedArgumentSwitchType = ArgumentSwitchTypeNone;
    ExpectedArgumentType expectedArgumentType = ExpectedArgumentTypeTargetModulePath;
    for ( int i = 1; i < argc; ++ i )
    {
        const QString arg = argv[ i ];
        if ( expectedArgumentType == ExpectedArgumentTypeTargetModulePath )
        {
            const QFileInfo targetModuleFileInfo( arg );
            if ( ! targetModuleFileInfo.exists() )
            {
                Logger::log( QString( "File \"%1\" doesn't exist." ).arg( arg ) );
                return EXIT_SUCCESS;
            }

            targetModulePath = QDir::toNativeSeparators( targetModuleFileInfo.canonicalFilePath() );

            expectedArgumentType = ExpectedArgumentTypeOptionalParameters;
        }
        else if ( expectedArgumentType == ExpectedArgumentTypeOptionalParameters )
        {
            if ( detectedArgumentSwitchType == ArgumentSwitchTypeNone )
            {
                static const QLatin1String ADDITIONAL_LIBRARY_INCLUDE_DIR_PATHS_SWITCH( "/i" );
                if ( arg == ADDITIONAL_LIBRARY_INCLUDE_DIR_PATHS_SWITCH )
                {
                    detectedArgumentSwitchType = ArgumentSwitchTypeAddLibDirPaths;
                }
            }
            else if ( detectedArgumentSwitchType == ArgumentSwitchTypeAddLibDirPaths )
            {
                const QStringList proposedAddLibDirPaths = arg.split( QChar( QLatin1Char( ';' ) ) );
                foreach ( const QString& proposedAddLibDirPath, proposedAddLibDirPaths )
                {
                    const QDir proposedAddLibDir( proposedAddLibDirPath );
                    if ( proposedAddLibDir.exists() )
                    {
                        addLibDirPaths.append( QDir::toNativeSeparators( proposedAddLibDir.canonicalPath() ) );
                    }
                }
            }
        }
        else
        {
            break;
        }
    }

    Deployer deployer;
    deployer.deploy( targetModulePath, addLibDirPaths );
    QObject::connect( & deployer, SIGNAL(deployed(QString,QStringList)), & app, SLOT(quit()) );

    return app.exec();
#else
    Q_UNUSED( argc )
    Q_UNUSED( argv )

    return EXIT_SUCCESS;
#endif
}
