#ifndef DEPLOYQT_LOGGER_H
#define DEPLOYQT_LOGGER_H

#include <QString>

namespace DeployQt {

class Logger
{
private:
    Logger();

public:
    static void log( const QString& message );
};

}

#endif // DEPLOYQT_LOGGER_H
