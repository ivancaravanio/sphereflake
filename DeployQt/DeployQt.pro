QT += core
QT -= gui

CONFIG += console
CONFIG -= app_bundle

CONFIG += c++11

macx {
    # Objective-C++ does not recognize C++11 syntax despite the CONFIG += c++11 option
    # https://bugreports.qt.io/browse/QTBUG-39057
    # https://bugreports.qt.io/browse/QTBUG-36575
    # https://codereview.qt-project.org/#/c/122199/

    QMAKE_OBJCXXFLAGS_PRECOMPILE += -std=c++11 -stdlib=libc++
}

CONFIG *= precompile_header

PRECOMPILED_HEADER = DeployQtPrecompiled.h

TARGET = DeployQt
TEMPLATE = app

SOURCES += \
    Logger.cpp \
    ModuleInfo.cpp \
    main.cpp \
    Version.cpp

win32 {
    SOURCES += Deployer.cpp
}

HEADERS += \
    DeployQtPrecompiled.h \
    Logger.h \
    ModuleInfo.h \
    Version.h

win32 {
    HEADERS += Deployer.h
}

include(../Build/Global.pri)

VERSION_MAJ = 1
VERSION_MIN = 0
VERSION_PAT = 2

include(../Build/Build.pri)

win32 {
    LIBS += -lVersion
}

win32 {
    DEPENDS_FILE_NAME = depends.exe
    DEFINES *= DEPENDS_FILE_NAME=\\\"$${DEPENDS_FILE_NAME}\\\"

    QMAKE_POST_LINK += $$copyFile( $${PWD}/../ThirdParty/depends/64/$${DEPENDS_FILE_NAME}, $${OUT_PWD}/$${BUILD} ) $$CRLF
    QMAKE_CLEAN     += $$fixedPath( $${OUT_PWD}/$${BUILD}/$${DEPENDS_FILE_NAME} )

    QMAKE_POST_LINK += $$copyFile( $${PWD}/../ThirdParty/depends/64/$${DEPENDS_FILE_NAME}, $${DEPLOYDIR} ) $$CRLF
    QMAKE_CLEAN     += $$fixedPath( $${DEPLOY}/$${BUILD}/$${DEPENDS_FILE_NAME} )
}
