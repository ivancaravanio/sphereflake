#!/usr/bin/env python
import sys

def versionString( major, minor, patch, fieldWidth, separator ) :
    format = '%%0%uu%s%%0%uu%s%%0%uu' % ( fieldWidth, separator, fieldWidth, separator, fieldWidth )
    return format % ( major, minor, patch )

if __name__ == '__main__':
    if len(sys.argv) > 1:
        switch = sys.argv[1]
        res = ""
        try:
            if switch == '-versionString' and len( sys.argv ) == 7:
                major = int( sys.argv[2] )
                minor = int( sys.argv[3] )
                patch = int( sys.argv[4] )
                fieldWidth = int( sys.argv[5] )
                separator = sys.argv[6]
                res = versionString( major, minor, patch, fieldWidth, separator )
            elif switch == '-increment' and len( sys.argv ) == 3:
                res = '%d' % ( int( sys.argv[2] ) + 1 )
            elif switch == '-decrement' and len( sys.argv ) == 3:
                res = '%d' % ( int( sys.argv[2] ) - 1 )
        except ValueError as valueError:
            pass
        except Exception as exception:
            pass

        print( res )
