LOCALE_ISO_NAMES = it_IT \
                   fr_FR \
                   de_DE \
                   nl_NL \
                   es_ES \
                   pt_PT \
                   ar \
                   zh_SF \
                   zh_TD \
                   el_GR \
                   hr_HR \
                   hu_HU \
                   pl_PL \
                   ru_RU \
                   sl_SI \
                   tr \
                   bg_BG

TRANSLATION_LOCALES = $$join( LOCALE_ISO_NAMES, : )
TRANSLATION_FILE_NAME_FORMAT = %1_%2.%3

DEFINES += TRANSLATION_LOCALES=\\\"$${TRANSLATION_LOCALES}\\\" \
           TRANSLATION_FILE_NAME_FORMAT=\\\"$$replace( TRANSLATION_FILE_NAME_FORMAT, %, %%%% )\\\"

TRANSLATIONS = $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 0  ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 1  ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 2  ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 3  ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 4  ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 5  ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 6  ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 7  ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 8  ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 9  ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 10 ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 11 ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 12 ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 13 ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 14 ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 15 ), ts ) \
               $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$member( LOCALE_ISO_NAMES, 16 ), ts )
